        <div class="col-md-9 col-thumbnail">
            <div style="margin: 15px 0;">
                <h2><?=str_replace('|','',$segment)?></h2>
            </div>
            <div class="thumbnail thumbnail-dashboard">
            <div class="row">
                <div class="col-md-12 text-center page-header" style="margin: 10px;">
                    <h2>Share ID anda di Social Media</h2>
                    <a href="javascript:poptastic('https://www.facebook.com/sharer/sharer.php?u=https://kopei-int.com/register?id_sponsor=<?=$user->usertologin?>');" class="fb-social-btn hover-social" style="margin:5px">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="javascript:poptastic('https://twitter.com/share?url=https://kopei-int.com/register?id_sponsor=<?=$user->usertologin?>;text=Ayo%20Gabung%20Di%20kopei-int.com;hashtags=kopei-int.com');" style="margin:5px" class="twitter-social-btn hover-social">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="javascript:poptastic('https://plus.google.com/share?url=https://kopei-int.com/register?id_sponsor=<?=$user->usertologin?>');" class="gplus-social-btn hover-social" style="margin:5px">
                        <i class="fa fa-google-plus"></i>
                    </a>
                    
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 table-responsive">
                    <h1 class="arrow text-center"></h1>
                    <table id="tab" class="table table-striped">
                        <thead>
                        <tr class="">
                            <th>No</th>
                            <th class="text-center">Generasi</th>
							<th>Username</th>
							<th>Nama</th>
						    <th>Kota</th>
							<th>No. Telepon</th>  
                            <th class="text-center">Total<br>LOT</th>  
							<th class="text-center">Jumlah<br>Sponsorisasi</th> 
							<th>Status</th>  
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $z = 0;
						foreach ($generasi as $key=>$v) {
							$z = $z + 1;?>
							<tr>
								<td><?=$z?></td>
								<td class="text-center"><?=$generasi[$key]['level']-1?></td>
								<td><?=$generasi[$key]['usertologin']?></td>
								<td><?=$generasi[$key]['name']?></td>
								<td><?=$generasi[$key]['kota']?></td>
								<td class="text-center"><?=$generasi[$key]['tlp']?></td>
								<td class="text-center"><?=$generasi[$key]['lot']?> LOT</td>
								<td class="text-center"><?=$generasi[$key]['sponsor']?></td>
								<td><?=$generasi[$key]['status']?></td>
							</tr>
						
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
        <!--tag open in other page-->
    </div>
</div>
</section>
<!--tag open in other page-->
<script>
$(document).ready(function () {
    $('#tab').DataTable({
        "order": [[ 0, "asc" ]],
        //"aoColumns": [null,{ "bSortable": false },{ "bSortable": false }]
    });
});
</script>