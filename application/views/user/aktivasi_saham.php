            <div class="col-md-9 col-thumbnail">
                <div style="margin: 15px 0;">
                    <h2><?=str_replace('|','',$segment)?></h2>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                            <h2 class="text-center card-header"><i class="fa fa-user fa-1x"></i> AKUN ANDA </h2>
                            <div class="row" style="font-weight: bold;">
                                <div class="col-md-6 col-xs-6">
                                    Saldo<br>
                                    Total LOT<br />
                                    LOT Aktif <br />
                                    LOT Antri <br />
                                    LOT Non-Aktif <br />
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    : <?=decimalNumber($saldo)?> Point<br />
                                    : <?php if (!empty($inactive_lot)){ echo $inactive_lot; }else{ echo '0';}?> Lot<br />
                                    : <?php if (!empty($active_lot)){ echo $active_lot; }else{ echo '0';}?> Lot<br />
                                    : <?php if (!empty($queue_lot)){ echo $queue_lot; }else{ echo '0';} ?> Lot <br />
                                    : <?=$non_active_lot?> Lot <br />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                            <?php 
                            $now_day = date('Y-m-d');
                            if($now_day >= '2016-07-05' && $now_day <= '2016-07-10'){ ?>
                                <span>Untuk sementara fitur ini dinon-aktifkan dan akan kembali diaktifkan pada tanggal 11 Juli 2016 pukul 00:00:00.
                                </span>
                            <?php } else if($max_activate_saham_user == 0){ ?>
                                <span>Semua Lot sudah diaktivasi<br />
                                Silahkan hubungi Leader untuk membeli LOT<br />
                                <a href="<?=base_url()?>leader" target="_blank"><b><u>Daftar Leader</u></b></a> 
                                </span>
                            <?php } else if(date('Y-m-d H:i:s') > date('Y-m-d 23:49:59') && date('Y-m-d H:i:s') < date('Y-m-d 23:59:59')) { ?>
                                <span>Saat ini anda tidak bisa mengaktifkan LOT. <br /> <br />
                                Mohon Tunggu &PlusMinus;15 Menit untuk mengaktifkan LOT anda. <br /><br />
                                Terima Kasih<br />
                                TTD Admin
                                </span>
                            <?php } else {?>
                            <h2 class="text-center card-header">AKTIVASI LOT ANDA </h2>
                            <form id="saham-form" class="form-horizontal">
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                               <div class="form-group">
                                <label class="col-sm-4 control-label">Jumlah Lot</label>
                                <div class="col-sm-4">
                                  <input type="number" class="form-control jmlsaham" id="jmlsaham" name="jmlsaham" placeholder="">
                                </div>
                              </div>
							  
							  <div class="form-group">
                                    <label class="col-sm-4 control-label">PIN</label>
                                    <div class="col-sm-5">
                                      <input type="password" maxlength="6" class="form-control" id="pin" name="pin" placeholder="">
                                      <!--small>klik tombol Kirim PIN SMS untuk mendapatkan PIN di nomor HP anda</small-->  
                                    </div>
                                    <!--div class="col-sm-3">
                                        <a class="btn btn-default btn-send-code">Kirim PIN SMS</a>
                                    </div--> 
                              </div><br>
                              <div class="form-group">
                                <div class="col-sm-12 text-center">
                                  <button type="submit" class="btn btn-default btn-submit">Aktivasi <!--span class="calculate"></span--></button>
                                </div>
                              </div>
                              <div class="show-notifForm"></div>
                            </form>
                            <?php } ?>
                       </div> 
                    </div>
                </div>
            </div>
<!--tag open in other page-->
        </div>
    </div>
</section>
<script>
$(document).ready( function(){
	var random_code = randomString(6, '0123456789abcdefghijklmnopqrstuvwxyz'); 
	$('<input />').attr('type', 'hidden')
				.attr('name', "confirmPin")
				.attr('value', random_code)
				.appendTo('#saham-form');
			
    $('.btn-send-code').click( function() {
        $(this).html('<i class="fa fa-spinner fa-spin"></i>');
        $(this).attr("disabled", 'disabled');
        var handphone = '<?=$user->handphone?>';
        var transaction = '<?=str_replace('|','',$segment)?>';
        //console.log(random_code);
        $.ajax({
            type: 'POST',  
            dataType: 'json', 
            url: "<?=base_url()?>user/send_random_pin", 
            data: { '<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',handphone:handphone ,code:random_code,transaction:transaction },
            success: function(result){
                $(this).html('PIN dikirim');
            }
        });
    }); 
	
	$('.jmlsaham').on('change keyup', function(){
		var curr = $(this).val();
		$('.calculate').html('');
		if(curr != ""){
			calculate = <?=$point_saham?>*curr;
			$('.calculate').html(formatNumber(calculate)+' Point');
		}
	});

	$('#saham-form').formValidation('destroy').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			jmlsaham: {
				validators: {
					notEmpty: {
						message: 'Harap diisi!'
					},
					lessThan: {
						value: <?=$max_activate_saham_user?>,
						message: 'Maksimal '+<?=$max_activate_saham_user?>
					},
					greaterThan: {
						value: 1,
						message: 'Minimal 1'
					}
				}
			},
			pin: {
                validators: {
                    notEmpty: {
                        message: 'Pin diperlukan'
                    },
                    remote: {
                        message: 'PIN tidak valid',
                        url: '<?=base_url()?>authenticate/pin_compare',
    					data:{'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',id:'pin'},
                        type: 'POST',
                        delay: 1200,
                    },
                    integer: {
                        message: 'Harus 6 Digit Angka'
                    },
                    stringLength: {
                        message: 'Harus 6 Digit Angka',
                        min: function (value, validator, $field) {
                            return 6 - (value.match(/\r/g) || []).length;
                        },
                        max: function (value, validator, $field) {
                            return 6 - (value.match(/\r/g) || []).length;
                        },
                    }
                }
            },
		}
	}).on('success.form.fv', function (e) {
		e.preventDefault();
		var dataString = $("#saham-form").serialize();
		$(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i>');
		$(".btn-submit").attr("disabled", 'disabled');
		$.ajax({
		url: '<?=base_url()?>user/do_aktivasi_saham',
		type: 'POST',
		data: dataString,
		success: function(data) {
		  //var obj = jQuery.parseJSON(data);
	//      if (obj.hasOwnProperty("false")) {
	//        $(".btn-submit").html('Aktivasi');
	//        $(".btn-submit").removeAttr("disabled");
	//        $('#signup-form .show-notifForm' ).empty();
	//        $('#signup-form .show-notifForm' ).append( '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button><strong>Oops! </strong>'+obj.false+'</div>' );
	//      } else {
			
		  //}
		  Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
			{
				msg: "Saham telah berhasil diaktivasi.",
				buttons: {
					ok: {
					'class': 'btn btn-default',
					closeOnClick: true
					},
				},
				callback: function(lobibox, type){
					window.location = '<?=base_url()?>aktivasi-saham';
				}
			});
		}
		});
		return false;
	}).on('err.field.fv', function (e, data) {
		if (data.fv.getSubmitButton()) {
			data.fv.disableSubmitButtons(true);
		}
	}).on('success.field.fv', function (e, data) {
		if (data.fv.getSubmitButton()) {
			data.fv.disableSubmitButtons(false);
		}
	});
});

</script>