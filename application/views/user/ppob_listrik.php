            <div class="col-md-9 col-thumbnail">
                <div style="margin: 15px 0;">
                    <h2><?=str_replace('|','',$segment)?></h2>
                </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                                <h2 class="text-center card-header"><i class="fa fa-user fa-1x"></i> AKUN ANDA </h2>
                                <div class="row" style="font-weight: bold;">
                                    <div class="col-md-6 col-xs-6">
                                        Saldo<br>
                                        Total LOT<br />
                                        LOT Aktif <br />
                                        LOT Antri <br />
                                        LOT Non-Aktif <br />
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                					 : <?=decimalNumber($saldo)?> Point<br />
                					 : <?php if (!empty($inactive_lot)){ echo $inactive_lot; }else{ echo '0';}?> Lot<br />
                					 : <?php if (!empty($active_lot)){ echo $active_lot; }else{ echo '0';}?> Lot<br />
                					 : <?php if (!empty($queue_lot)){ echo $queue_lot; }else{ echo '0';} ?> Lot <br />
                					 : <?=$non_active_lot?> Lot <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                                <?php 
                                $now_day = date('Y-m-d');
                                if($now_day >= '2016-07-05' && $now_day <= '2016-07-10'){ ?>
                                    <span>Untuk sementara fitur ini dinon-aktifkan dan akan kembali diaktifkan pada tanggal 11 Juli 2016 pukul 00:00:00.
                                    </span>
                                <?php } else if($user_point<=0){ ?>
                                    <br><span>Anda tidak dapat membeli pulsa karna minimal saldo anda tidak mencukupi</span>
                                <?php } else { ?>
                                <h2 class="text-center card-header"></i> Form Pembelian Token Listrik</h2>
                                <form id="saham-form" class="form-horizontal" style="margin: 0 0px">
                                <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label"><span class="operator-text">Token Listrik</span></label>
                                    <div class="col-sm-5">
                                        <select class="form-control" id="nominal" name="nominal">
                                        <?php foreach ($master_ppob_listrik as $master) {
                                            echo '<option value="'.($master->price_center+$master->price_markup)/$point_to_rp.'" id_ppob="'.$master->id_ppob.'">Rp '.currencyNumber($master->price).' | '.($master->price_center+$master->price_markup)/$point_to_rp.' Point</option>';
                                        } ?>
                                        </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">No.ID/Meteran Listrik</label>
                                    <div class="col-sm-5">
                                      <input type="number" class="form-control input-meteran" id="pln_number" name="pln_number" placeholder="" value="<?=$user->pln_number?>">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Token akan dikirim ke</label>
                                    <div class="col-sm-5">
                                      <input type="number" class="form-control input-handphone" id="handphone" name="handphone" placeholder="" value="<?=$user->handphone?>">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">PIN</label>
                                    <div class="col-sm-5">
                                      <input type="password" maxlength="6" class="form-control" id="pin" name="pin" placeholder="">
                                      <!--small>klik tombol Kirim PIN SMS untuk mendapatkan PIN di nomor HP anda</small-->  
                                    </div>
                                    <!--div class="col-sm-3">
                                        <a class="btn btn-default btn-send-code">Kirim PIN SMS</a>
                                    </div--> 
                              </div><br>

                                  <div class="form-group">
                                    <div class="col-sm-12 text-center">
                                      <button type="submit" class="btn btn-default btn-submit">Submit <!--span class="calculate"></span--></button>
                                    </div>
                                  </div>
                                  <div class="show-notifForm"></div>
                                </form>
                                <?php } ?>
                           </div> 
                        </div>
                        <div class="clearfix"></div>
						<div class="col-md-12 table-responsive">
                            <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                            <h2 class="text-center">History Pembelian Token Listrik</h2>
                            <table id="tab" class="table table-striped">
                                <thead>
                                <tr class="">
        							<th>Tanggal</th>
                                    <th>Keterangan</th>
        							<th>Point</th> 
        							<th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                $z = 0;
                                foreach($point_data as $data){ 
                                $z = $z + $data->value;
                                ?>
                                <tr>
        							<td><?=dateHours($data->date_transaction)?></td>
                                    <td><?=$data->description?></td>
        							<td><?=decimalNumber($data->value)?></td>
                                    <?php if($data->is_done==0){ ?>
                                        <td>Proses</td>
                                    <?php } else if($data->is_done==1){ ?>
                                        <td>Selesai</td>
                                    <?php } else if($data->is_done==2){ ?>
                                        <td>Dibatalkan</td>
                                    <?php } ?>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
<!--tag open in other page-->
        </div>
    </div>
</section>
<script>

$(document).ready( function(){
    var random_code = randomString(6, '0123456789abcdefghijklmnopqrstuvwxyz'); 
    $('<input />').attr('type', 'hidden')
            .attr('name', "confirmPin")
            .attr('value', random_code)
            .appendTo('#saham-form');
            
    $('.btn-send-code').click( function() {
        $(this).html('<i class="fa fa-spinner fa-spin"></i>');
        $(this).attr("disabled", 'disabled');
        var handphone = '<?=$user->handphone?>';
        var transaction = '<?=str_replace('|','',$segment)?>';
        //console.log(random_code);
        $.ajax({
            type: 'POST',  
            dataType: 'json', 
            url: "<?=base_url()?>user/send_random_pin", 
            data: { '<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',handphone:handphone ,code:random_code,transaction:transaction },
            success: function(result){
                $(this).html('PIN dikirim');
            }
        });
    }); 
      
                
    $('.jmlpoint').on('change keyup', function(){
        var curr = $(this).val();
        $('.calculate').html('');
        if(curr != ""){
            calculate = <?=$point_saham?>*curr;
            $('.calculate').html(formatNumber(calculate)+' Point');
        }
    });
    
    $('#saham-form').formValidation('destroy').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            pln_number: {
                validators: {
                    notEmpty: {
                        message: 'Harap diisi!'
                    },
                }
            },
            handphone: {
                validators: {
                    notEmpty: {
                        message: 'Harap diisi!'
                    },
                }
            },
            nominal: {
                validators: {
                    notEmpty: {
                        message: 'Harap diisi!'
                    },
                    lessThan: {
                        value: <?=$user_point-$proses_point?>,
                        message: 'Saldo point anda tidak mencukupi'
                    },
                }
            },
            pin: {
                validators: {
                    notEmpty: {
                        message: 'Pin diperlukan'
                    },
                    remote: {
                        message: 'PIN tidak valid',
                        url: '<?=base_url()?>authenticate/pin_compare',
    					data:{'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',id:'pin'},
                        type: 'POST',
                        delay: 1200,
                    },
                    integer: {
                        message: 'Harus 6 Digit Angka'
                    },
                    stringLength: {
                        message: 'Harus 6 Digit Angka',
                        min: function (value, validator, $field) {
                            return 6 - (value.match(/\r/g) || []).length;
                        },
                        max: function (value, validator, $field) {
                            return 6 - (value.match(/\r/g) || []).length;
                        },
                    }
                }
            },
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();
        var dataString = $("#saham-form").serialize();
        var pln_number = $('input[name=pln_number]').val();
        var handphone = $('input[name=handphone]').val();
        var id_ppob = $('#nominal option:selected').attr("id_ppob");
        $(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i>');
        $(".btn-submit").attr("disabled", 'disabled');
        $.ajax({
        url: '<?=base_url()?>user/do_ppob_listik',
        type: 'POST',
        data:{ '<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',
                handphone: handphone,
                id_ppob: id_ppob,
                pln_number: pln_number },
        success: function(data) {
          //var obj = jQuery.parseJSON(data);
    //      if (obj.hasOwnProperty("false")) {
    //        $(".btn-submit").html('Aktivasi');
    //        $(".btn-submit").removeAttr("disabled");
    //        $('#signup-form .show-notifForm' ).empty();
    //        $('#signup-form .show-notifForm' ).append( '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button><strong>Oops! </strong>'+obj.false+'</div>' );
    //      } else {
            
          //}
		  Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
			{
				msg: "Pembelian token listrik anda dalam proses.",
				buttons: {
					ok: {
					'class': 'btn btn-default',
					closeOnClick: true
					},
				},
				callback: function(lobibox, type){
					window.location = '<?=base_url()?>ppob-listrik';
				}
			});
        }
        });
        return false;
    }).on('err.field.fv', function (e, data) {
        if (data.fv.getSubmitButton()) {
            data.fv.disableSubmitButtons(true);
        }
    }).on('success.field.fv', function (e, data) {
        if (data.fv.getSubmitButton()) {
            data.fv.disableSubmitButtons(false);
        }
    });
});
</script>