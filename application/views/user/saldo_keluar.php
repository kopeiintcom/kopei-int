        <div class="col-md-9 col-thumbnail">
            <div style="margin: 15px 0;">
                <h2><?=str_replace('|','',$segment)?></h2>
            </div>
            <div class="thumbnail thumbnail-dashboard">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <table id="tab" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Nominal</th>
                            <th>Keterangan</th>
                            <th>Saldo</th>        
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $z = 0;
                        foreach($saldo as $data){ 
                        $z = $z + $data->value;?>
                        <tr>
                            <td><?=dateGeneral($data->date_transaction)?></td>
                            <td><?=decimalNumber($data->value)?></td>
                            <td><?=$data->description?></td>
                            <td><?=decimalNumber($z)?></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
        <!--tag open in other page-->
    </div>
</div>
</section>
<!--tag open in other page-->
<script>
$(document).ready(function () {
    $('#tab').DataTable({
        "order": [[ 0, "asc" ]],
        "aoColumns": [{ "bSortable": false },{ "bSortable": false },{ "bSortable": false },{ "bSortable": false },{ "bSortable": false }]
    });
});
</script>