<div class="col-md-9 col-thumbnail">
    <div style="margin: 15px 0;">
        <h2><?=str_replace('|','',$segment)?></h2>
    </div>
  <div class="thumbnail thumbnail-dashboard">
    <div class="row">
      <div class="clearfix"></div>
        <div class="col-md-offset-1 col-md-9 text-center">
          <form id="signup-form" class="form-horizontal" style="margin: 0 20px;">
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
            <div class="form-group">
              <label class="col-sm-4 control-label">User Name</label>
              <div class="col-sm-8">
                <input disabled type="text" class="form-control" id="username" name="username" placeholder="" value="<?=$user->usertologin?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Nama Lengkap</label>
              <div class="col-sm-8">
                <input disabled type="text" class="form-control" id="name" name="name" placeholder="" value="<?=$user->name?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Alamat</label>
              <div class="col-sm-8">
                <textarea disabled class="form-control" name="alamat" id="alamat" rows="3" cols="20" onkeypress="onTestChange();" ><?=$user->address?></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Kota</label>
              <div class="col-sm-8">
                <input disabled type="text" class="form-control" id="kota" name="kota" placeholder="" value="<?=$user->city?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Handphone</label>
              <div class="col-sm-8">
                <input disabled type="number" class="form-control" id="handphone" name="handphone" placeholder="" value="<?=$user->handphone?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Email</label>
              <div class="col-sm-8">
                <input disabled type="email" class="form-control" id="email" name="email" placeholder="" value="<?=$user->email?>">
              </div>
            </div>
                
            <div class="form-group">
              <label class="col-sm-4 control-label">Nomor Rekening</label>
              <div class="col-sm-8">
                <input disabled type="text" class="form-control" id="norekening" name="norekening" placeholder="" value="<?=$user->bank_account_number?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Nama Bank</label>
              <div class="col-sm-8">
                <input disabled type="text" class="form-control" id="nmbank" name="nmbank" placeholder="" value="<?=$user->bank_name?>">
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-4 control-label">No. ID/Meteran Listrik</label>
              <div class="col-sm-8">
                <input disabled type="text" class="form-control" id="pln_number" name="pln_number" placeholder="" value="<?=$user->pln_number?>">
              </div>
            </div>
            
            <div class="form-group">
            <div class="col-sm-offset-5 col-sm-10">
              <a class="btn btn-danger" role="button" id="edit" style="margin-right: 10px">Edit</a>
              <input id="save" type="submit" class="btn btn-primary disabled" value="Save">
            </div>
          </div>
          </form>
        </div>
    </div>
  </div>
  
  <div class="thumbnail thumbnail-dashboard">
    <div class="row">
      <div class="col-md-12 text-center" >
        <h1>Change Password</h1><br />
      </div>
      <div class="clearfix"></div>
      <div class="col-md-offset-1 col-md-9 text-center">
        <form id="pass-form" class="form-horizontal" style="margin: 0 20px;">
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
            <div class="form-group">
              <label class="col-sm-4 control-label">Old Password</label>
              <div class="col-sm-8">
                <input disabled type="password" class="form-control" id="cpass" name="cpass" placeholder="">
              </div>
          </div>

          <div class="form-group">
                <label class="col-sm-4 control-label">New Password</label>
                <div class="col-sm-8">
                  <input disabled type="password" class="form-control" id="password" name="password" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Confirm Password</label>
                <div class="col-sm-8">
                  <input disabled type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="">
                </div>
            </div>
          <div class="form-group">
            <div class="col-sm-offset-5 col-sm-10">
              
              <a class="btn btn-danger" role="button" id="editpass" style="margin-right: 10px">Edit</a>
              <input id="savepass" type="submit" class="btn btn-primary disabled" value="Save">
            </div>
          </div>
          <div class="show-notifForm"></div>
        </form>
      </div>
    </div>
  </div>
  
  <div class="thumbnail thumbnail-dashboard">
    <div class="row">
      <div class="col-md-12 text-center" >
        <h1>Change PIN</h1><br />
      </div>
      <div class="clearfix"></div>
      <div class="col-md-offset-1 col-md-9 text-center">
        <form id="pin-form" class="form-horizontal" style="margin: 0 20px;">
            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
			  <div class="form-group">
                <label class="col-sm-4 control-label">Old Pin</label>
                <div class="col-sm-8">
                  <input disabled type="password" maxlength="6" class="form-control" id="oldpin" name="oldpin" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">New Pin</label>
                <div class="col-sm-8">
                  <input disabled type="password" maxlength="6" class="form-control" id="pin" name="pin" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Confirm Pin</label>
                <div class="col-sm-8">
                  <input disabled type="password" class="form-control" maxlength="6" id="confirmPin" name="confirmPin" placeholder="">
                </div>
			</div>

          <div class="form-group">
            <div class="col-sm-offset-5 col-sm-10">
              
              <a class="btn btn-danger" role="button" id="editpin" style="margin-right: 10px">Edit</a>
              <input id="savepin" type="submit" class="btn btn-primary disabled" value="Save">
            </div>
          </div>
          <div class="show-notifForm"></div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<!--tag open in other page-->
</div>
          
<script>
var obj = jQuery.parseJSON('<?=$json?>');
//$('#name').val(obj.name);
//$('#alamat').val(obj.address); 
//$('#kota').val(obj.city);
//$('#handphone').val(obj.hp);
//$('#email').val(obj.email);  
//$('#norekening').val(obj.rekening);  
//$('#nmbank').val(obj.bank); 
//$('#username').val(obj.username); 
$('#save').hide();
$('#savepass').hide();
$('#savepin').hide();

$('#signup-form').formValidation('destroy').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        name: {
            validators: {
                notEmpty: {
                    message: 'Nama lengkap diperlukan'
                }
            }
        },
        kota: {
            validators: {
                notEmpty: {
                    message: 'Kota diperlukan'
                }
            }
        },
        handphone: {
            validators: {
                notEmpty: {
                    message: 'Silahkan masukan no Handphone!'
                },
                remote: {
                    message: 'No handphone sudah terdaftar',
                    url: '<?=base_url()?>user/check_handphone',
                    type: 'POST',
                    delay: 1200,
                    data:{'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',},
                }
            },
        },
        email: {
            validators: {
                notEmpty: {
                    message: 'Email diperlukan'
                },
                emailAddress: {
                    message: 'Inputan bukan alamat email yang valid'
                },
                remote: {
                    message: 'Email sudah terdaftar',
                    url: '<?=base_url()?>general/check_email_update',
					data:{'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',email:email, id_user:<?=$user->id_user?>},
                    type: 'POST',
                    delay: 1200,
                }
            }
        },
        norekening: {
            validators: {
                notEmpty: {
                    message: 'Nomor Rekening diperlukan'
                },
				remote: {
                    message: 'No Rekening sudah terdaftar',
                    url: '<?=base_url()?>user/check_rekening',
                    type: 'POST',
                    delay: 1200,
                    data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',},
                }
            }
        },
        nmbank: {
            validators: {
                notEmpty: {
                    message: 'Nama Bank diperlukan'
                },
            }
        },
        pln_number: {
            validators: {
                notEmpty: {
                    message: 'No. ID/Meteran Listrik diperlukan'
                },
            }
        },
        //username: {
//            validators: {
//                notEmpty: {
//                    message: 'Masukan username'
//                }
//            }
//        },
//        password: {
//            validators: {
//                notEmpty: {
//                    message: 'Password diperlukan'
//                },
//                //identical: {
////                    field: 'confirmPassword',
////                    message: 'Password dan konfirmasi password tidak sama'
////                }
//            }
//        },
//        confirmPassword: {
//            validators: {
//                //notEmpty: {
////                    message: 'Konfirmasi password diperlukan'
////                },
//                identical: {
//                    field: 'password',
//                    message: 'Password dan konfirmasi password tidak sama'
//                }
//            }
//        },
//        pin: {
//            validators: {
//                notEmpty: {
//                    message: 'Pin diperlukan'
//                },
//                integer: {
//                    message: 'Harus 6 Digit Angka'
//                },
//                stringLength: {
//                    message: 'Harus 6 Digit Angka',
//                    min: function (value, validator, $field) {
//                        return 6 - (value.match(/\r/g) || []).length;
//                    },
//                    max: function (value, validator, $field) {
//                        return 6 - (value.match(/\r/g) || []).length;
//                    },
//                }
//            }
//        },
//        confirmPin: {
//            validators: {
//                //notEmpty: {
////                    message: 'Konfirmasi pin diperlukan'
////                },
//                identical: {
//                    field: 'pin',
//                    message: 'Pin dan konfirmasi pin tidak sama'
//                }
//            }
//        },
    }
}).on('success.form.fv', function (e) {
    e.preventDefault();
    var dataString = $("#signup-form").serialize()+"&user_id=null";
    $(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i>');
    $(".btn-submit").attr("disabled", 'disabled');
    $.ajax({
    url: '<?=base_url()?>authenticate/update_profile_admin',
    type: 'POST',
    data: dataString,
    success: function(data) {
      
	  Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
		{
			msg: "Data Berhasil diupdate.",
			buttons: {
				ok: {
				'class': 'btn btn-info',
				closeOnClick: true
				},
			},
			callback: function(lobibox, type){
				window.location.href = "<?=base_url()?>profile";
			}
		});
    }
    });
    return false;
}).on('err.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(true);
    }
}).on('success.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(false);
    }
});


$('#pass-form').formValidation('destroy').formValidation({
    
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      cpass: {
            validators: {
                notEmpty: {
                    message: 'Masukan password anda!'
                },
                remote: {
                    message: 'Password tidak sama',
                    url: '<?=base_url()?>authenticate/pass_compare',
                    data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>'},
                    type: 'POST',
                    delay: 1200,
                }
            }
        },
        password: {
            validators: {
                notEmpty: {
                    message: 'Masukan password baru anda!'
                },
                //identical: {
//                    field: 'confirmPassword',
//                    message: 'Password dan konfirmasi password tidak sama'
//                }
            }
        },
        confirmPassword: {
            validators: {
                //notEmpty: {
//                    message: 'Konfirmasi password diperlukan'
//                },
                identical: {
                    field: 'password',
                    message: 'Password dan konfirmasi password tidak sama'
                }
            }
        },
    }
}).on('success.form.fv', function (e) {
    e.preventDefault();
    var dataString = $("#pass-form").serialize();
    $(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i>');
    $(".btn-submit").attr("disabled", 'disabled');
    $.ajax({
    url: '<?=base_url()?>authenticate/update_pass_admin',
    type: 'POST',
    data: dataString,
    success: function(data) {
	  Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
		{
			msg: "Password berhasil diupdate.",
			buttons: {
				ok: {
				'class': 'btn btn-info',
				closeOnClick: true
				},
			},
			callback: function(lobibox, type){
				window.location.href = "<?=base_url()?>profile";
			}
		});
    }
    });
    return false;
}).on('err.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(true);
    }
}).on('success.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(false);
    }
});

$('#pin-form').formValidation('destroy').formValidation({
    
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      oldpin: {
            validators: {
                notEmpty: {
                    message: 'Masukan PIN anda!'
                },
                remote: {
                    message: 'PIN tidak sama',
                    url: '<?=base_url()?>authenticate/pin_compare',
					data:{id:'oldpin','<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',},
                    type: 'POST',
                    delay: 1200,
                }
            }
        },
        pin: {
            validators: {
                notEmpty: {
                    message: 'Masukan PIN baru anda!'
                },
            }
        },
        confirmPin: {
            validators: {
                identical: {
                    field: 'pin',
                    message: 'PIN dan konfirmasi PIN tidak sama'
                }
            }
        },
    }
}).on('success.form.fv', function (e) {
    e.preventDefault();
    var dataString = $("#pin-form").serialize();
    $(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i>');
    $(".btn-submit").attr("disabled", 'disabled');
    $.ajax({
    url: '<?=base_url()?>authenticate/update_pin_admin',
    type: 'POST',
    data: dataString,
    success: function(data) {
      
	  Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
		{
			msg: "PIN berhasil diupdate.",
			buttons: {
				ok: {
				'class': 'btn btn-info',
				closeOnClick: true
				},
			},
			callback: function(lobibox, type){
				window.location.href = "<?=base_url()?>profile";
			}
		});
    }
    });
    return false;
}).on('err.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(true);
    }
}).on('success.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(false);
    }
});

$('#save').click(function(){
  //window.location.replace("<?=base_url()?>admin_transfer_lot");
 
});


$('#edit').click(function(){
  $("#name").prop('disabled', false);
  $("#alamat").prop('disabled', false);
  $("#handphone").prop('disabled', false);
  $("#email").prop('disabled', false);
  $("#norekening").prop('disabled', false);
  $("#nmbank").prop('disabled', false);
  //$("#username").prop('disabled', false);
  $("#kota").prop('disabled', false);
  $(this).hide();
  $('#save').show();
});

$('#editpass').click(function(){
  $("#cpass").prop('disabled', false);
  $("#password").prop('disabled', false);
  $("#confirmPassword").prop('disabled', false);
  $(this).hide();
  $('#savepass').show();
});

$('#editpin').click(function(){
  $("#oldpin").prop('disabled', false);
  $("#pin").prop('disabled', false);
  $("#confirmPin").prop('disabled', false);
  $(this).hide();
  $('#savepin').show();
});
</script>