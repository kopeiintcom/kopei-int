    <div class="hidden-xs" id='cssmenuv'>
        <ul>
           <li class="first"><a href='<?=base_url()?>dashboard'><span>Dashboard</span></a></li>
           <!--li><a href='".base_url()."register-saham'><span>Beli Saham</span></a></li-->
           <li class='has-sub open'><a href='#'><span>Transaksi</span></a>
              <ul style="display: block;">
                 <!--li class='has-sub'><a href='#'><span>Product</span></a>
                    <ul>
                       <li><a href='#'><span>Sub Product</span></a></li>
                       <li class='last'><a href='#'><span>Sub Product</span></a></li>
                    </ul>
                 </li-->
                 <li><a href='<?=base_url()?>aktivasi-saham'><span>Aktivasi Lot</span></a></li>
                 <li><a href='<?=base_url()?>pencairan-point'><span>Pencairan Point</span></a></li>
                 <!--li><a href='#'><span>Pemesanan Saham</span></a></li-->
                 <li><a href='<?=base_url()?>transfer-lot'><span>Transfer LOT</span></a></li>
				 <li><a href='<?=base_url()?>transfer-saldo'><span>Transfer Saldo</span></a></li>
              </ul>
           </li>
           <li class="has-sub open"><a href='#'><span>Monitoring Mutasi</span></a>
                <ul style="display: block;">
                    <li><a href='<?=base_url()?>saldo-pendapatan'><span>Saldo Pendapatan</span></a></li>
                    <li><a href='<?=base_url()?>saldo-keluar'><span>Saldo Keluar</span></a></li>
                    <li><a href='<?=base_url()?>saldo-masuk'><span>Saldo Masuk</span></a></li>
                    <li><a href='<?=base_url()?>stok-pendapatan'><span>Stok Pendapatan</span></a></li>
                    <li><a href='<?=base_url()?>dana-bersama'><span>Dana Bersama</span></a></li>
					<li><a href='<?=base_url()?>mutasi-lot'><span>Mutasi LOT</span></a></li>
                </ul>
           </li>
           <li class="has-sub open"><a href='#'><span>PPOB</span></a>
                <ul style="display: block;">
                    <li><a href='<?=base_url()?>ppob-pulsa'><span>Pulsa</span></a></li>
                    <li><a href='<?=base_url()?>ppob-listrik'><span>Token Listrik</span></a></li>
                </ul>
           </li>
		   <li><a href='<?=base_url()?>sponsorisasi'><span>Sponsorisasi</span></a></li>
           <li class="last"><a href='<?=base_url()?>amal-anda'><span>Amal Anda</span></a></li>
        </ul>
    </div>
    <div class="visible-xs" id='cssmenu'>
        <ul>
           <li><a href='<?=base_url()?>dashboard'><span>Dashboard</span></a></li></li> 
           <li><a href='#'><span>Transaksi</span></a>
               <ul>
                 <li><a href='<?=base_url()?>aktivasi-saham'><span>Aktivasi Lot</span></a></li>
                 <li><a href='<?=base_url()?>pencairan-point'><span>Pencairan Point</span></a></li>
                 <li><a href='<?=base_url()?>transfer-lot'><span>Transfer LOT</span></a></li>
    			 <li><a href='<?=base_url()?>transfer-saldo'><span>Transfer Saldo</span></a></li>
              </ul>
           </li>
           <li><a href='#'><span>Monitoring Mutasi</span></a>
                <ul>
                    <li><a href='<?=base_url()?>saldo-pendapatan'><span>Saldo Pendapatan</span></a></li>
                    <li><a href='<?=base_url()?>saldo-keluar'><span>Saldo Keluar</span></a></li>
                    <li><a href='<?=base_url()?>saldo-masuk'><span>Saldo Masuk</span></a></li>
                    <li><a href='<?=base_url()?>stok-pendapatan'><span>Stok Pendapatan</span></a></li>
                    <li><a href='<?=base_url()?>dana-bersama'><span>Dana Bersama</span></a></li>
                    <li><a href='<?=base_url()?>mutasi-lot'><span>Mutasi LOT</span></a></li>
                </ul>
           </li>
           <li class="has-sub open"><a href='#'><span>PPOB</span></a>
                <ul style="display: block;">
                    <li><a href='<?=base_url()?>ppob-pulsa'><span>Pulsa</span></a></li>
                    <!--li><a href='<?=base_url()?>ppob-listrik'><span>Token Listrik</span></a></li-->
                </ul>
           </li>
           <li><a href='<?=base_url()?>sponsorisasi'><span>Sponsorisasi</span></a></li>
           <li><a href='<?=base_url()?>amal-anda'><span>Amal Anda</span></a></li>
           <?php if(isset($is_receiver_fee)){
                    if($is_receiver_fee) {
            ?>
           <li><a href='<?=base_url()?>pencairan-fee'><span>Pencairan Fee</span></a></li>
           <?php } } ?>
       </ul>
    </div>
</div>
<script>
$(function() {
     var pgurl = window.location.href;
     $("#cssmenuv ul li").each(function(){
          if($(this).attr("href") == pgurl || $(this).attr("href") == '' )
          $(this).addClass("static-page-menu-active");
     })
});
</script>