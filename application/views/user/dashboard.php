        <div class="col-md-9 col-thumbnail">
            <div style="margin: 15px 0;">
                <h2><?=str_replace('|','',$segment)?></h2>
            </div>
            <?php $now_day = date('Y-m-d');
            if($now_day >= '2016-07-02' && $now_day <= '2016-07-10'){ ?>
            <div class="thumbnail thumbnail-dashboard text-center" style="background: pink;font-family: times-new-roman;">
                <div class="row">
                    <div class="col-md-12"><h1>Pengumuman</h1></div>
                    <div class="clearfix"></div>
                    <div class="col-md-12"><br>System Kopei-int.com sementara waktu akan di non-aktifkan untuk segala jenis transaksi dari tanggal 5 Juli 2016 pukul 00:00:00. 
                    Dan akan diaktifkan kembali pada tanggal 11 Juli 2016 pukul 00:00:00.<br /><br />
                    Segenap staff Kopei-int mengucapkan Selamat menyambut Hari Raya Idul Fitri 1 Syawal 1437 Hijriyah, semoga komunitas kita ini berjalan lancar selamanya, Amin.
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: aqua;">
                        <h2 class="text-center card-header" style="font-size:16px">TOTAL PENDAPATAN</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-point"><?=decimalNumber($total_pendapatan)?></span> <span class="font-tiny">Point
                                <span class="pull-right"><i class="fa fa-suitcase fa-4x"></i></span><br />
                                Rp. <?=currencyNumber($total_pendapatan*10000)?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: coral;">
                        <h2 class="text-center card-header" style="font-size:16px">DIBAGIKAN</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-point"><?=decimalNumber($current_profit)?></span> <span class="font-tiny">Point 
                                <span class="pull-right"><i class="fa fa-credit-card fa-4x"></i></span><br />Rp. <?=currencyNumber($current_profit*10000)?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: magenta;">
                        <h2 class="text-center card-header" style="font-size:16px">STOK PENDAPATAN</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-point"><?=decimalNumber($stok_pendapatan)?></span> <span class="font-tiny">Point <span class="pull-right"><i class="fa fa-cubes fa-4x"></i></span><br />Rp. <?=currencyNumber($stok_pendapatan*10000)?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: greenyellow;">
                        <h2 class="text-center card-header" style="font-size:16px">DANA BERSAMA</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-point"><?=decimalNumber($dana_bersama)?></span> <span class="font-tiny">Point <span class="pull-right"><i class="fa fa-archive fa-4x"></i></span><br />Rp. <?=currencyNumber($dana_bersama*10000)?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                        <h2 class="text-center card-header"><i class="fa fa-user fa-1x"></i> AKUN ANDA </h2>
                        <div class="row" style="font-weight: bold;">
                            <div class="col-md-6 col-xs-6">
                                Saldo<br>
                                Total LOT<br />
                                LOT Aktif <br />
                                LOT Antri <br />
                                LOT Non-Aktif <br />
                            </div>
                            <div class="col-md-6 col-xs-6">
                                : <?=decimalNumber($saldo)?> Point<br />
                                : <?php if (!empty($inactive_lot)){ echo $inactive_lot; }else{ echo '0';}?> Lot<br />
                                : <?php if (!empty($active_lot)){ echo $active_lot; }else{ echo '0';}?> Lot <br />
                                : <?php if (!empty($queue_lot)){ echo $queue_lot; }else{ echo '0';} ?> Lot <br />
                                : <?=$non_active_lot ?> Lot <br />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                        <h2 class="text-center card-header"><i class="fa fa-group fa-0.5x"></i> RINCIAN PROFIT </h2>
                        <div class="row" style="font-weight: bold;">
                            <div class="col-md-6 col-xs-6" >
                                Profit Sekarang<br>
                                Profit Kemarin<br />
                                LOT Sekarang <br />
                                LOT Kemarin <br />
                                User Sekarang <br />
                            </div>
                            <div class="col-md-6 col-xs-6">
                                : <?=decimalNumber($current_profit)?> Point<br />
                                : <?=decimalNumber($yesterday_profit)?> Point<br />
                                : <?=$current_slot?> Lot<br />
                                : <?php if(!empty($yesterday_slot)){ echo $yesterday_slot; }else{ echo '0';} ?>  Lot<br />
                                : <?=$current_user?> User<br />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                        <h2 class="text-center card-header"><i class="fa fa-bar-chart fa-1x"></i> STATISTIK AKUN </h2>
                        <div class="row" style="font-weight: bold;">
                            <div class="col-md-6 col-xs-6">
                                Total Penerima<br />
                                Total Full Profit<br />
                                Total Antrian<br>
                                Total Member<br />
                                Free Member<br />
                            </div>
                            <div class="col-md-6 col-xs-6">
                                : <?=$total_penerima?> Lot<br />
                                : <?=$total_full_profit?> Lot<br />
                                : <?=$total_antrian?> Lot<br />
                                : <?=$total_member?> User<br />
                                : <?=$total_free_member?> User<br />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                        <h2 class="text-center card-header"><i class="fa fa-share-alt fa-1x"></i> PENAMBAHAN POSISI</h2>
                        <div class="row" style="font-size:12px ;">
                            <div class="col-md-12 col-xs-12">
                                <?php $str_now = strtotime(date("Y-m-d"));
                                foreach($penambahan_posisi as $row){
                                    echo '<div class="card-detail">';
                                    echo $row->name;
                                    $post_time_submit = $row->date_actived;
                                    $date_post_time = date_create($post_time_submit);
                                    $post_time = strtotime(substr($post_time_submit,0,10));
                                    $post_days = ($str_now - $post_time)/(60*60*24);
                                    if($str_now == $post_time) {
                                        echo ', Hari ini '.date_format($date_post_time, "H:i"); 
                                    }
                                    else if($post_days == 1) {
                                        echo ', Kemarin '.date_format($date_post_time, "H:i"); 
                                    }
                                    else if($post_days == 2) {
                                        echo ', 2 hari lalu '.date_format($date_post_time, "H:i");
                                    }
                                    else {
                                        echo ', '.date_format($date_post_time, "d-m-Y H:i");
                                    }
                                echo '<br>From '.$row->city.'<br></div>' ; 
                                }
                                 ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                        <h2 class="text-center card-header"><i class="fa fa-user-plus fa-0.5x"></i> MEMBER BARU </h2>
                        <div class="row" style="font-size:12px ;">
                            <div class="col-md-12 col-xs-12">
                                <?php $str_now = strtotime(date("Y-m-d"));
                                foreach($new_member as $row){
                                    echo '<div class="card-detail">';
                                    echo $row->name;
                                    $post_time_submit = $row->date_created;
                                    $date_post_time = date_create($post_time_submit);
                                    $post_time = strtotime(substr($post_time_submit,0,10));
                                    $post_days = ($str_now - $post_time)/(60*60*24);
                                    if($str_now == $post_time) {
                                        echo ', Hari ini '.date_format($date_post_time, "H:i"); 
                                    }
                                    else if($post_days == 1) {
                                        echo ', Kemarin '.date_format($date_post_time, "H:i"); 
                                    }
                                    else if($post_days == 2) {
                                        echo ', 2 hari lalu '.date_format($date_post_time, "H:i");
                                    }
                                    else {
                                        echo ', '.date_format($date_post_time, "d-m-Y H:i");
                                    }
                                echo '<br>From '.$row->city.'<br></div>' ; 
                                }
                                 ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                        <h2 class="text-center card-header"><i class="fa fa-user-secret fa-1x"></i> TOP SPONSOR </h2>
                        <div class="row" style="font-size:12px ;">
                            <div class="col-md-12 col-xs-12">
                                <?php $str_now = strtotime(date("Y-m-d"));
                                $z=1;
                                foreach($top_sponsor as $row){
                                    echo '<div class="card-detail">';
                                    echo $z.'. '.$row->name;
                                    echo '<span class="pull-right">'.$row->total.' member</span></div>' ; 
                                    $z++;
                                }
                                 ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--tag open in other page-->
    </div>
</div>

</section>
<!--tag open in other page-->