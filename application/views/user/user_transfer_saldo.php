            <div class="col-md-9 col-thumbnail">
                <div style="margin: 15px 0;">
                    <h2><?=str_replace('|','',$segment)?></h2>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                            <h2 class="text-center card-header"><i class="fa fa-user fa-1x"></i> AKUN ANDA </h2>
                            <div class="row" style="font-weight: bold;">
                                <div class="col-md-6 col-xs-6">
                                    Saldo<br>
                                    Total LOT<br />
                                    LOT Aktif <br />
                                    LOT Antri <br />
                                    LOT Non-Aktif <br />
                                </div>
                                <div class="col-md-6 col-xs-6">
            				     : <?=decimalNumber($saldo)?> Point<br />
            				     : <?php if (!empty($inactive_lot)){ echo $inactive_lot; }else{ echo '0';}?> Lot<br />
            				     : <?php if (!empty($active_lot)){ echo $active_lot; }else{ echo '0';}?> Lot<br />
            				     : <?php if (!empty($queue_lot)){ echo $queue_lot; }else{ echo '0';} ?> Lot <br />
            				     : <?=$non_active_lot?> Lot <br />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                            <?php 
                            $now_day = date('Y-m-d');
                            if($now_day >= '2016-07-06' && $now_day <= '2016-07-10'){ ?>
                                <span>Untuk sementara fitur ini dinon-aktifkan dan akan kembali diaktifkan pada tanggal 11 Juli 2016 pukul 00:00:00.
                                </span>
                            <?php } else if($saldo <= 0){ ?>
                                <span>Anda Sudah Tidak Dapat Melakukan Transfer Saldo lagi</span>
                            <?php } else { ?>
                            <h2 class="text-center card-header"> FORM TRANSFER SALDO </h2>
                            <form id="saham-form" class="form-horizontal">
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
								<div class="form-group">
                            <label class="col-sm-4 control-label">Username</label>
                            <div class="col-sm-8">
                              <input style="width:80%" type="text" class="form-control username" id="username" name="username"  value="" placeholder="">
                            </div>
                          </div>
                           <div class="form-group">
                            <label class="col-sm-4 control-label">Nama</label>
                            <div class="col-sm-8">
                              <input disabled style="width:80%" type="text" class="form-control saldo" id="nama" name="nama"  placeholder="">
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Kota</label>
                            <div class="col-sm-8">
                              <input  disabled style="width:80%" type="text"  class="form-control active-lot" id="kota" name="kota"  value="" placeholder="">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">No. Handphone</label>
                            <div class="col-sm-8">
                              <input  disabled style="width:80%" type="text" class="form-control inactive-lot" id="hp" name="hp"  value="" placeholder="">
                            </div>
                          </div>
                               <div class="form-group">
                                <label class="col-sm-4 control-label">Jumlah Saldo</label>
                                <div class="col-sm-4">
                                  <input type="number" class="form-control jmlsaldo" step="0.01" id="jmlsaldo" name="jmlsaldo" placeholder="">
                                </div>
                              </div>
							  
							  <!--div class="form-group">
                                    <label class="col-sm-4 control-label">PIN SMS</label>
                                    <div class="col-sm-5">
                                      <input type="password" maxlength="6" class="form-control" id="pin" name="pin" placeholder="">
                                      <small>klik tombol Kirim PIN SMS untuk mendapatkan PIN di nomor HP anda</small>  
                                    </div>
                                    <div class="col-sm-3">
                                        <a class="btn btn-default btn-send-code">Kirim PIN SMS</a>
                                    </div>
                                    
                              </div><br-->
							  <div class="form-group">
									<label class="col-sm-4 control-label">PIN</label>
									<div class="col-sm-5">
										<input type="password" maxlength="6" class="form-control" id="oldpin" name="oldpin" placeholder="">
									</div>
								</div>
								 
                              <div class="form-group">
                                <div class="col-sm-12 text-center">
                                  <button type="submit" class="btn btn-default btn-submit">Submit <!--span class="calculate"></span--></button>
                                </div>
                              </div>
                              <div class="show-notifForm"></div>
                            </form>
                            <?php } ?>
                       </div> 
                    </div>
                </div>
            </div>
<!--tag open in other page-->
        </div>
    </div>
</section>
<script>
$(document).ready( function(){
	var random_code = randomString(6, '0123456789abcdefghijklmnopqrstuvwxyz'); 
	$('<input />').attr('type', 'hidden')
				.attr('name', "confirmPin")
				.attr('value', random_code)
				.appendTo('#saham-form');
			
    $('.btn-send-code').click( function() {
        $(this).html('<i class="fa fa-spinner fa-spin"></i>');
        $(this).attr("disabled", 'disabled');
        var handphone = '<?=$user->handphone?>';
        var transaction = '<?=str_replace('|','',$segment)?>';
        //console.log(random_code);
        $.ajax({
            type: 'POST',  
            dataType: 'json', 
            url: "<?=base_url()?>user/send_random_pin", 
            data: { '<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',handphone:handphone ,code:random_code,transaction:transaction },
            success: function(result){
                $(this).html('PIN dikirim');
            }
        });
    }); 
	
	$('#saham-form').formValidation('destroy').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			jmlsaldo: {
				validators: {
					notEmpty: {
						message: 'Harap diisi!'
					},
					lessThan: {
						value: <?=$saldo -(($saldo*2)/100)?>,
						message: 'Maksimal '+<?=$saldo -(($saldo*2)/100)?>+' point'
					},
					greaterThan: {
						value: <?=$minimal?>,
						message: 'Minimal <?=$minimal?> point'
					}
				}
			},
			oldpin: {
                validators: {
                    notEmpty: {
                        message: 'Pin diperlukan'
                    },
                    remote: {
                        message: 'PIN tidak valid',
                        url: '<?=base_url()?>authenticate/pin_compare',
    					data:{'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',id:'oldpin'},
                        type: 'POST',
                        delay: 1200,
                    },
                    integer: {
                        message: 'Harus 6 Digit Angka'
                    },
                    stringLength: {
                        message: 'Harus 6 Digit Angka',
                        min: function (value, validator, $field) {
                            return 6 - (value.match(/\r/g) || []).length;
                        },
                        max: function (value, validator, $field) {
                            return 6 - (value.match(/\r/g) || []).length;
                        },
                    }
                }
            },
			//pin: {
//				validators: {
//					notEmpty: {
//						message: 'Masukan PIN anda!'
//					},
//					identical: {
//							field: 'confirmPin',
//							message: 'Pin tidak valid'
//					},
//				}
//			},
			username: {
				validators: {
					notEmpty: {
						message: 'Silahkan masukan username!'
					},
					remote: {
						message: 'Username tidak terdaftar',
						url: '<?=base_url()?>user/check_username',
						type: 'POST',
						delay: 1200,
                        data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',},
					}
				},
			},
		}
	}).on('success.form.fv', function (e) {
		e.preventDefault();
		var dataString = $("#saham-form").serialize();
		$(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i>');
		$(".btn-submit").attr("disabled", 'disabled');
		$.ajax({
		url: '<?=base_url()?>user/do_transfer_saldo',
		type: 'POST',
		data: dataString,
		success: function(data) {
			   var nama = $('#username').val();
			   var saldo = $('#jmlsaldo').val();
			   Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
				{
					msg: "Anda berhasil melakukan transfer Saldo sebanyak "+saldo+" point ke "+nama,
					buttons: {
						ok: {
						'class': 'btn btn-default',
						closeOnClick: true
						},
					},
					callback: function(lobibox, type){
						window.location = '<?=base_url()?>transfer-saldo';
					}
				});
		}
		});
		return false;
	}).on('err.field.fv', function (e, data) {
		if (data.fv.getSubmitButton()) {
			data.fv.disableSubmitButtons(true);
		}
	}).on('success.field.fv', function (e, data) {
		if (data.fv.getSubmitButton()) {
			data.fv.disableSubmitButtons(false);
		}
	});

	$('#username').on('keyup', function(){
	   if($(this).val().length == 10){
		username = $(this).val();
    		$.ajax({
    		url: '<?=base_url()?>user/check_username',
    		type: 'POST',
    		data: { '<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',username : username },
    		success: function(result) {
    		  var obj = jQuery.parseJSON(result);
    			if(obj.valid){
    			  $('#nama').val(obj.nama);
    			  $('#kota').val(obj.kota);
    			  $('#hp').val(obj.hp);
    			}       
    		  }
    		});
        }
	}); 
});

</script>