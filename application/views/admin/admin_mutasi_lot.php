        <div class="col-md-9 col-thumbnail">
            <div style="margin: 15px 0;">
                <h2><?=str_replace('|','',$segment)?></h2>
            </div>
            <div class="thumbnail thumbnail-dashboard">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <table id="tab" class="table table-striped">
                        <thead>
                        <tr>
                            <th class="hide"></th>
                            <th>Tanggal</th>
                            <th>LOT</th>
                            <th>Deskripsi</th>       
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $z = 0;
                        foreach($history_lot as $data){ 
                        $z = $z + $data->value*$data->isdk;?>
                        <tr>
                            <td class="hide"><?=$data->id_transaction?></td>
                            <td><?=dateGeneral($data->date_transaction)?></td>
                            <td><?=decimalNumber($data->value)?></td>
                            <td><?=$data->description?></td>
                            
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
        <!--tag open in other page-->
    </div>
</div>
</section>
<!--tag open in other page-->
<script>
$(document).ready(function () {
    $('#tab').DataTable({
        "order": [[ 0, "desc" ]],
    });
});
</script>