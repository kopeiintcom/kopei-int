  <div class="col-md-9 col-thumbnail">
      <div class="thumbnail thumbnail-dashboard">
        <div class="row">
              <div class="col-md-12 text-center" >
                  <h1>Admin Profile</h1><br />  
              </div>
              <div class="clearfix"></div>
              <div class="col-md-offset-1 col-md-9 text-center">
              <form id="signup-form" class="form-horizontal" style="margin: 0 20px;">
                <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                <div class="form-group">
                  <label class="col-sm-4 control-label">Nama Lengkap</label>
                  <div class="col-sm-8">
                    <input disabled type="text" class="form-control" id="name" name="name" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Alamat</label>
                  <div class="col-sm-8">
                    <textarea disabled class="form-control" name="alamat" id="alamat" rows="3"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Kota</label>
                  <div class="col-sm-8">
                    <input disabled type="text" class="form-control" id="kota" name="kota" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Handphone</label>
                  <div class="col-sm-8">
                    <input disabled type="number" class="form-control" id="handphone" name="handphone" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Email</label>
                  <div class="col-sm-8">
                    <input disabled type="email" class="form-control" id="email" name="email" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Nomor Rekening</label>
                  <div class="col-sm-8">
                    <input disabled type="text" class="form-control" id="norekening" name="norekening" placeholder="">
                  </div>
                </div>
                <!--div class="form-group">
                  <label class="col-sm-4 control-label">Nama Pemegang Rekening</label>
                  <div class="col-sm-8">
                    <input type="email" class="form-control" id="nmrekening" name="nmrekening" placeholder="">
                  </div>
                </div-->
                <div class="form-group">
                  <label class="col-sm-4 control-label">Nama Bank</label>
                  <div class="col-sm-8">
                    <input disabled type="text" class="form-control" id="nmbank" name="nmbank" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">User Name</label>
                  <div class="col-sm-8">
                    <input disabled type="text" class="form-control" id="username" name="username" placeholder="">
                  </div>
                </div>
                <!--div class="form-group">
                  <label class="col-sm-4 control-label">Password</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="password" name="password" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Confirm Password</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Pin</label>
                  <div class="col-sm-8">
                    <input type="password" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="6" class="form-control" id="pin" name="pin" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Confirm Pin</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" maxlength="6" id="confirmPin" name="confirmPin" placeholder="">
                  </div>
                </div-->
                <div class="form-group">
                  <div class="col-sm-offset-6 col-sm-10">
			           <a href="<?=base_url()?>admin_transfer_lot" class="btn btn-primary" role="button">Edit</a>
                  </div>
                </div>
                <div class="show-notifForm"></div>
              </form>
                          </div>
                      </div>
                  </div>
              </div>
              </div>
  <!--tag open in other page-->
          </div>
          
<script>
var obj = jQuery.parseJSON('<?=$json?>');
$('#name').val(obj.name);
$('#alamat').val(obj.address); 
$('#kota').val(obj.city);
$('#handphone').val(obj.hp);
$('#email').val(obj.email);  
$('#norekening').val(obj.rekening);  
$('#nmbank').val(obj.bank); 
$('#username').val(obj.username); 
</script>