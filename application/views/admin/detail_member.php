            <div class="col-md-9 col-thumbnail">
                <div style="margin: 15px 0;">
                    <h2><?=str_replace('|','',$segment)?></h2>
                </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?php if($user_data->is_active == 0){ ?>
                            <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                                <h2 class="text-center card-header">Akun Belum Aktif</h2>
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
									  <div class="col-md-12 col-xs-12 text-center">
										<button title="Aktifkan Member" class="btn btn-sm btn-info" 
										onclick="doActivate('<?=$user_data->id_user?>')"><span class="fa fa-rocket fa-1x"></span> Aktifkan Member</button>
									   </div>
									</div>
                                </div>
							</div>
                            <?php } ?>
                            <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                                <h2 class="text-center card-header"><i class="fa fa-user fa-1x"></i> INFO AKUN </h2>
                                <div class="row" style="font-weight: bold;">
                                    <div class="col-md-6 col-xs-6">
                                        Saldo<br>
                                        Total LOT<br />
                                        LOT Aktif <br />
                                        LOT Antri <br />
                                        LOT Non-Aktif <br />
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                					 : <?=decimalNumber($saldo)?> Point<br />
                					 : <?php if (!empty($inactive_lot)){ echo $inactive_lot; }else{ echo '0';}?> Lot<br />
                					 : <?php if (!empty($active_lot)){ echo $active_lot; }else{ echo '0';}?> Lot<br />
                					 : <?php if (!empty($queue_lot)){ echo $queue_lot; }else{ echo '0';} ?> Lot <br />
                					 : <?=$non_active_lot?> Lot <br />
                                    </div>
                                </div>
                            </div>
                            <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                                <h2 class="text-center card-header"><i class="fa fa-refresh"></i> RESET FORM </h2>
                                <div class="row">
                                    <div class="col-md-10 col-xs-12" style="padding-bottom: 10px;">
									  <label class="col-md-6 col-xs-6">Password</label>
									  <div class="col-md-6 col-xs-6">
										<button title="Reset Password" class="btn btn-sm btn-success" 
										onclick="doReset('<?=$user_data->id_user?>')"><span class="fa fa-refresh"></span> Reset</button>
									  </div>
									</div>
									<div class="col-md-10 col-xs-12">
									  <label class="col-md-6 col-xs-6">PIN</label>
									  <div class="col-md-6 col-xs-6">
										<button title="Reset PIN" class="btn btn-sm btn-success" 
										onclick="doResetPin('<?=$user_data->id_user?>')"><span class="fa fa-refresh"></span> Reset</button>
									  </div>
									</div>
                                </div>
							</div>
                            <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                                <h2 class="text-center card-header">Status : <?php if($user_data->role==0){echo 'Member';}else{echo 'Leader';}  ?></h2>
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
									  <?php if($user_data->role==0){ ?>
										<div class="col-md-12 col-xs-12 text-center">									
											<button title="Jadikan Leader" class="btn btn-sm btn-success"  
											onclick="doProses('<?=$user_data->id_user?>', '<?=$user_data->role?>')"><span class="fa fa-user-secret fa-1x"></span> Jadikan Leader</button>
										</div>
									  <?php } else { ?>
										<div class="col-md-12 col-xs-12 text-center">
											<button title="Jadikan Member" class="btn btn-sm btn-success" 
											onclick="doProses('<?=$user_data->id_user?>', '<?=$user_data->role?>')"><span class="fa fa-user fa-1x"></span> Jadikan Member</button>
										</div>
									  <?php } ?>
									</div>
                                </div>
							</div>
                            <?php if($saldo == 0 && $inactive_lot == 0 && $active_lot == 0 && $queue_lot == 0){ ?>
                            <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                                <h2 class="text-center card-header">Delete Member</h2>
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
									  <div class="col-md-12 col-xs-12 text-center">
										<button title="Delete Member" class="btn btn-sm btn-danger" 
										onclick="doDelete('<?=$user_data->id_user?>')"><span class="fa fa-trash fa-1x"></span> Delete Member</button>
									   </div>
									</div>
                                </div>
							</div>
                            <?php } ?>
						</div>
						<div class="col-md-8">
                            <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                            <h2 class="text-center card-header"></i>Profile Member</h2>
                                <form id="signup-form" class="form-horizontal" style="margin: 0 0px">
                                <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                   <div class="form-group">
									  <label class="col-sm-4 control-label">User Name</label>
									  <div class="col-sm-8">
										<input disabled type="text" class="form-control" id="username" name="username" placeholder="">
									  </div>
									</div>
                                   <div class="form-group">
									  <label class="col-sm-4 control-label">Nama Lengkap</label>
									  <div class="col-sm-8">
										<input disabled type="text" class="form-control" id="name" name="name" placeholder="">
									  </div>
									</div>

									<div class="form-group">
									  <label class="col-sm-4 control-label">Alamat</label>
									  <div class="col-sm-8">
										<textarea disabled class="form-control" name="alamat" id="alamat" rows="3" onkeypress="onTestChange();"></textarea>
									  </div>
									</div>

									<div class="form-group">
									  <label class="col-sm-4 control-label">Kota</label>
									  <div class="col-sm-8">
										<input disabled type="text" class="form-control" id="kota" name="kota" placeholder="">
									  </div>
									</div>

									<div class="form-group">
									  <label class="col-sm-4 control-label">Handphone</label>
									  <div class="col-sm-8">
										<input disabled type="number" class="form-control" id="handphone" name="handphone" placeholder="">
									  </div>
									</div>

									<div class="form-group">
									  <label class="col-sm-4 control-label">Email</label>
									  <div class="col-sm-8">
										<input disabled type="email" class="form-control" id="email" name="email" placeholder="">
									  </div>
									</div>
										
									<div class="form-group">
									  <label class="col-sm-4 control-label">Nomor Rekening</label>
									  <div class="col-sm-8">
										<input disabled type="text" class="form-control" id="norekening" name="norekening" placeholder="">
									  </div>
									</div>

									<div class="form-group">
									  <label class="col-sm-4 control-label">Nama Bank</label>
									  <div class="col-sm-8">
										<input disabled type="text" class="form-control" id="nmbank" name="nmbank" placeholder="">
									  </div>
									</div>
									<div class="form-group">
									<div class="col-sm-offset-10 col-sm-10">
									  
									  <a class="btn btn-danger" role="button" id="edit" style="margin-right: 10px">Edit</a>
									  <input id="save" type="submit" class="btn btn-primary disabled" value="Save">
									</div>
								  </div>
                                </form>
                           </div> 
                           <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                                <h2 class="text-center card-header"></i>Info Sponsor</h2>    
                                <form id="sponsor-form" class="form-horizontal" style="margin: 0 0px">
                                <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                   <div class="form-group">
									  <label class="col-sm-4 control-label">User Name</label>
									  <div class="col-sm-8">
										<input disabled type="text" class="form-control" id="sponsor_username" name="sponsor_username" placeholder="">
									  </div>
									</div>
                                    <div class="form-group">
									  <label class="col-sm-4 control-label">Name</label>
									  <div class="col-sm-8">
										<input disabled type="text" class="form-control" id="sponsor_name" name="sponsor_name" placeholder="">
									  </div>
									</div>
									<div class="form-group">
									<div class="col-sm-offset-10 col-sm-10">
									  
									  <a class="btn btn-danger" role="button" id="edit_sponsor" style="margin-right: 10px">Edit</a>
									  <input id="save_sponsor" type="submit" class="btn btn-primary disabled" value="Save">
									</div>
								  </div>
                                   	<!--div class="form-group">
									<div class="col-sm-offset-10 col-sm-10">
									  <a class="btn btn-danger" role="button" id="edit" style="margin-right: 10px">Edit</a>
									  <input id="save" type="submit" class="btn btn-primary disabled" value="Save">
									</div>
								  </div-->
                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="thumbnail thumbnail-dashboard" style="background-color: white;">
                                <div class="row">
                                    <h2 class="text-center card-header"></i>Track Transaksi</h2>    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label text-right">Jenis Transaksi</label>
                                            <div class="col-sm-4 ">
                                              <select class="form-control" onchange="getval(this);">
                                                <option value="1">Mutasi LOT</option>
                                                <option value="2">Mutasi Point</option>
                                              </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div><br /><br />
    								<div class="col-sm-12" >
                                        <div class="wrap-lot">
                                            <table id="tab" class="table table-striped">
            									<thead>
            									<tr class="">
            										<th>Tanggal</th>
            										<th>Nominal</th>
            										<th>D/K</th>
            										<th>Keterangan</th>
            									</tr>
            									</thead>
            										<tbody>
            											<?php 
            												foreach($mutasi_lot as $data){ ?>
            												<tr>
            													<td><?=dateGeneral($data->date_transaction)?></td>
            													<td><?=decimalNumber($data->value)?></td>
            													<td><?php if($data->isdk==1){echo 'D';}else{echo 'K';}?></td>
            													<td><?=$data->description?></td>
            												</tr>
            											<?php } ?>
            										</tbody>
            								</table>
                                        </div>
        								<div class="wrap-point">
            								<table id="tab_point" class="table table-striped">
            									<thead>
            									<tr class="">
            										<th>Tanggal</th>
            										<th>Nominal</th>
            										<th>D/K</th>
            										<th>Keterangan</th>
            									</tr>
            									</thead>
        										<tbody>
        											<?php 
        												foreach($pendapatan as $data){ ?>
        												<tr>
        													<td><?=dateGeneral($data->date_transaction)?></td>
        													<td><?=decimalNumber($data->value)?></td>
        													<td><?php if($data->isdk==1){echo 'D';}else{echo 'K';}?></td>
        													<td><?=$data->description?></td>
        												</tr>
        											<?php } ?>
        										</tbody>
            								</table>
                                        </div>
    								</div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
<!--tag open in other page-->
        </div>
    </div>
</section>
<script>
$('#tab_point').DataTable( {
    "order": [[ 0, "asc" ]]
} );

$('#tab').DataTable( {
    "order": [[ 0, "asc" ]]
} );


function getval(sel) {
	var data = {};
          if(sel.value==1) {
			$('.wrap-lot').show();
			$('.wrap-point').hide();
          } else {
			$('.wrap-lot').hide();
			$('.wrap-point').show();
            
		  }
    }
	
	
	var myJSONString = JSON.stringify(<?=$json?>);
	var myEscapedJSONString = myJSONString.replace(/\\n/g, "\\n")
                                      .replace(/\\'/g, "\\'")
                                      .replace(/\\"/g, '\\"')
                                      .replace(/\\&/g, "\\&")
                                      .replace(/\\r/g, "\\r")
                                      .replace(/\\t/g, "\\t")
                                      .replace(/\\b/g, "\\b")
                                      .replace(/\\f/g, "\\f");
									  
	var obj = jQuery.parseJSON(myEscapedJSONString);
	$('#name').val(obj.name);
	$('#alamat').val(obj.address); 
	$('#kota').val(obj.city);
	$('#handphone').val(obj.hp);
	$('#email').val(obj.email);  
	$('#norekening').val(obj.rekening);  
	$('#nmbank').val(obj.bank); 
	$('#username').val(obj.username);
    $('#sponsor_username').val(obj.sponsor_username); 
    $('#sponsor_name').val(obj.sponsor_name);  
	$('#save').hide();
	$('#save_sponsor').hide();
	$('#savepass').hide();
	$('#savepin').hide();
	$('.wrap-point').hide();
	$id_user = obj.id_user;

	function onTestChange() {
		var key = window.event.keyCode;

		// If the user has pressed enter
		if (key == 13) {
			event.preventDefault();
			var s = $(this).val();
			$(this).val(s+"\n");
		}
	}
	
	$('#edit').click(function(){
	  $("#name").prop('disabled', false);
	  $("#alamat").prop('disabled', false);
	  $("#handphone").prop('disabled', false);
	  $("#email").prop('disabled', false);
	  $("#norekening").prop('disabled', false);
	  $("#nmbank").prop('disabled', false);
	  //$("#username").prop('disabled', false);
	  $("#kota").prop('disabled', false);
	  $(this).hide();
	  $('#save').show();
	});
	
	$('#edit_sponsor').click(function(){
	  $("#sponsor_username").prop('disabled', false);
	  $(this).hide();
	  $('#save_sponsor').show();
	});
	
	$('#sponsor-form').formValidation('destroy').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        sponsor_username: {
            validators: {
                notEmpty: {
                    message: 'Username diperlukan'
                },
                remote: {
                    message: 'Username tidak terdaftar',
                    url: '<?=base_url()?>admin/check_username',
                    type: 'POST',
                    delay: 1200,
                    data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',},
                }
            }
        },
    }
	}).on('success.form.fv', function (e) {
		e.preventDefault();
		var dataString = $("#sponsor-form").serialize()+"&user_id="+<?=$user_data->id_user?>;
		$(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i>');
		$(".btn-submit").attr("disabled", 'disabled');
		$.ajax({
		url: '<?=base_url()?>admin/update_profile_sponsor',
		type: 'POST',
		data: dataString,
		success: function(data) {
		  //window.location.href = "<?=base_url()?>admin/detail_member/<?=$user_data->id_user?>";
		  Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
			{
				msg: "Data Berhasil diupdate.",
				buttons: {
					ok: {
					'class': 'btn btn-info',
					closeOnClick: true
					},
				},
				callback: function(lobibox, type){
					window.location.href = "<?=base_url()?>admin/detail_member/<?=$user_data->id_user?>";
				}
			});
		}
		});
		return false;
	}).on('err.field.fv', function (e, data) {
		if (data.fv.getSubmitButton()) {
			data.fv.disableSubmitButtons(true);
		}
	}).on('success.field.fv', function (e, data) {
		if (data.fv.getSubmitButton()) {
			data.fv.disableSubmitButtons(false);
		}
	});


	$('#signup-form').formValidation('destroy').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        name: {
            validators: {
                notEmpty: {
                    message: 'Nama lengkap diperlukan'
                }
            }
        },
        kota: {
            validators: {
                notEmpty: {
                    message: 'Kota diperlukan'
                }
            }
        },
        handphone: {
            validators: {
                notEmpty: {
                    message: 'Handphone diperlukan'
                },
            }
        },
        email: {
            validators: {
                notEmpty: {
                    message: 'Email diperlukan'
                },
                emailAddress: {
                    message: 'Inputan bukan alamat email yang valid'
                },
                remote: {
                            message: 'Email sudah terdaftar',
                            url: '<?=base_url()?>general/check_email_update',
							data:{email:email, id_user:<?=$user->id_user?>},
                            type: 'POST',
                            delay: 1200,
                        }
            }
        },
        norekening: {
            validators: {
                notEmpty: {
                    message: 'Nomor Rekening diperlukan'
                },
            }
        },
        nmbank: {
            validators: {
                notEmpty: {
                    message: 'Nama Bank diperlukan'
                },
            }
        },
        username: {
            validators: {
                notEmpty: {
                    message: 'Masukan username'
                }
            }
        },
        password: {
            validators: {
                notEmpty: {
                    message: 'Password diperlukan'
                },
                //identical: {
//                    field: 'confirmPassword',
//                    message: 'Password dan konfirmasi password tidak sama'
//                }
            }
        },
        confirmPassword: {
            validators: {
                //notEmpty: {
//                    message: 'Konfirmasi password diperlukan'
//                },
                identical: {
                    field: 'password',
                    message: 'Password dan konfirmasi password tidak sama'
                }
            }
        },
        pin: {
            validators: {
                notEmpty: {
                    message: 'Pin diperlukan'
                },
                integer: {
                    message: 'Harus 6 Digit Angka'
                },
                stringLength: {
                    message: 'Harus 6 Digit Angka',
                    min: function (value, validator, $field) {
                        return 6 - (value.match(/\r/g) || []).length;
                    },
                    max: function (value, validator, $field) {
                        return 6 - (value.match(/\r/g) || []).length;
                    },
                }
            }
        },
        confirmPin: {
            validators: {
                //notEmpty: {
//                    message: 'Konfirmasi pin diperlukan'
//                },
                identical: {
                    field: 'pin',
                    message: 'Pin dan konfirmasi pin tidak sama'
                }
            }
        },
    }
}).on('success.form.fv', function (e) {
    e.preventDefault();
    var dataString = $("#signup-form").serialize()+"&user_id="+$id_user;
    $(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i>');
    $(".btn-submit").attr("disabled", 'disabled');
    $.ajax({
    url: '<?=base_url()?>authenticate/update_profile_admin',
    type: 'POST',
    data: dataString,
    success: function(data) {
      //window.location.href = "<?=base_url()?>admin/detail_member/<?=$user_data->id_user?>";
	  Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
		{
			msg: "Data Berhasil diupdate.",
			buttons: {
				ok: {
				'class': 'btn btn-info',
				closeOnClick: true
				},
			},
			callback: function(lobibox, type){
				window.location.href = "<?=base_url()?>admin/detail_member/<?=$user_data->id_user?>";
			}
		});
    }
    });
    return false;
}).on('err.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(true);
    }
}).on('success.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(false);
    }
});

function doReset(id)
{
	$.ajax({
         type: "POST",
         url: "<?=base_url()?>admin/reset_password",
         data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>','id':id},
         success: function(msg){
            
			Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
			{
				msg: "Password berhasil direset.",
				buttons: {
					ok: {
					'class': 'btn btn-info',
					closeOnClick: true
					},
				},
				callback: function(lobibox, type){
					window.location = '<?=base_url()?>admin/detail_member/'+id;
				}
			});
         }
    });
}

function doResetPin(id)
{
	$.ajax({
         type: "POST",
         url: "<?=base_url()?>admin/reset_pin",
         data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>','id':id},
         success: function(msg){
            
			Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
			{
				msg: "PIN berhasil direset.",
				buttons: {
					ok: {
					'class': 'btn btn-info',
					closeOnClick: true
					},
				},
				callback: function(lobibox, type){
					window.location = '<?=base_url()?>admin/detail_member/'+id;
				}
			});
         }
    });
}

function doProses(id, role)
{
    $.ajax({
         type: "POST",
         url: "<?=base_url()?>admin/change_status_member",
         data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>','id':id, 'role':role},
         success: function(msg){
            //window.location = '<?=base_url()?>admin/detail_member/'+id;
			Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
			{
				msg: "Status berhasil diupdate.",
				buttons: {
					ok: {
					'class': 'btn btn-info',
					closeOnClick: true
					},
				},
				callback: function(lobibox, type){
					window.location = '<?=base_url()?>admin/detail_member/'+id;
				}
			});
         }
    });
}

function doDelete(id)
{
    $.ajax({
         type: "POST",
         url: "<?=base_url()?>admin/delete_member",
         data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>','id':id},
         success: function(msg){
            //window.location = '<?=base_url()?>admin/detail_member/'+id;
			Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
			{
				msg: "Member berhasil di delete.",
				buttons: {
					ok: {
					'class': 'btn btn-info',
					closeOnClick: true
					},
				},
				callback: function(lobibox, type){
					window.location = '<?=base_url()?>list_member';
				}
			});
         }
    });
}

function doActivate(id)
{
    $.ajax({
         type: "POST",
         url: "<?=base_url()?>admin/activate_member",
         data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>','id':id},
         success: function(msg){
            //window.location = '<?=base_url()?>admin/detail_member/'+id;
			Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
			{
				msg: "Member berhasil di aktifasi.",
				buttons: {
					ok: {
					'class': 'btn btn-info',
					closeOnClick: true
					},
				},
				callback: function(lobibox, type){
					window.location = '<?=base_url()?>admin/detail_member/'+id;
				}
			});
         }
    });
}

$('#sponsor_username').on('keyup', function(){
    username = $(this).val();
    $.ajax({
    url: '<?=base_url()?>admin/check_username',
    type: 'POST',
    data: { '<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',username : username },
    success: function(result) {
      var obj = jQuery.parseJSON(result);
        if(obj.valid){
		  $('#sponsor_name').val(obj.nama);
        }       
      }
    });
}); 
</script>