        <div class="col-md-9 col-thumbnail">
            <div class="thumbnail thumbnail-dashboard">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <h1 class="arrow text-center">Daftar Member</h1>
                    <table id="tab" class="table table-striped">
                        <thead>
                        <tr class="">
                            <th>Username</th>
                            <th>Nama</th>
                            <th>Handphone</th>
                            <th>Kota</th>
                            <th>Active</th>
							<th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        //$z = 0;
                        foreach($data_member as $data){?>
                        <tr>
                            <td><?=$data->usertologin?></td>
                            <td><?=$data->name?></td>
                            <td><?=$data->handphone?></td>
                            <td><?=$data->city?></td>
                            <td class="text-center"><?=($data->is_active != 0 ? '<i class="fa fa-check-circle fa-lg" style="color:#24E424;"></i>' : '<i class="fa fa-times-circle fa-lg" style="color:#F14D4D;"></i>')?></td>
							<td><a title="Detail" class="btn btn-sm btn-default" 
								href="<?=base_url()?>admin/detail_member/<?=$data->id_user?>"><span class="fa fa-file-text"></span></button>
                                <!--button title="Reset Password" class="btn btn-sm btn-default" 
								onclick="doReset('<?=$data->id_user?>')"><span class="fa fa-refresh"></span></button-->
                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
        <!--tag open in other page-->
    </div>
</div>
</section>
<!--tag open in other page-->
<script>
$(document).ready(function () {
    $('#tab').DataTable({
        "order": [[ 0, "desc" ]],
        //"aoColumns": [null,{ "bSortable": false },{ "bSortable": false }]
    });
});

</script>