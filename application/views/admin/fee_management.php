<div class="col-md-9 col-thumbnail">
    <div class="thumbnail thumbnail-dashboard">
      <div class="row">
                <div class="col-md-12 text-center" >
                    <h1>Bagi Hasil Management</h1><br />
                   
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 table-responsive">
                    <table id="tab" class="table table-striped">
                        <thead style="border:1px solid cyan;background: aqua;">
                            <th class="text-center">No.</th>
                            <th>Rincian Bagi Hasil</th>
                            <th class="text-center">Persentasi (%)</th>
                            <th class="text-right">Nominal (Rp)</th>
                        </thead>
                        <tbody>
                        <?php 
                        $tot_percent = 0;
                        $tot_nominal = 0;
                        foreach($master as $data){ 
                            $nom = ($nominal*($data->value/100))*$convert; ?>
                            <tr>
                                <td class="text-center"><?=$data->id_fee_management?></td>
                                <td><?=$data->description?></td>
                                <td class="text-center"><?=decimalNumber($data->value)?></td>
                                <td class="text-right"><?=currencyNumber($nom)?></td>
                            </tr>
                        <?php 
                        $tot_percent = $tot_percent + $data->value;
                        $tot_nominal = $tot_nominal + $nom;
                        } ?>
                            <tr style="border:1px solid lightgreen;background: lightgreen;font-weight: bold;">
                                <td colspan="2" class="text-right">Balance</td>
                                <td class="text-center"><?=decimalNumber($tot_percent)?></td>
                                <td class="text-right"><?=currencyNumber($tot_nominal)?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
          </div>
        </div>
    </div>
</section>