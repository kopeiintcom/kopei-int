    <div class="hidden-xs" id='cssmenuv'>
        <ul>
           <li class="first"><a href='<?=base_url()?>admin'><span>Dashboard</span></a></li>
           <!--li><a href='".base_url()."register-saham'><span>Beli Saham</span></a></li-->
           <li class='has-sub open'><a href='#'><span>Transaksi</span></a>
              <ul style="display: block;">
                 <!--li class='has-sub'><a href='#'><span>Product</span></a>
                    <ul>
                       <li><a href='#'><span>Sub Product</span></a></li>
                       <li class='last'><a href='#'><span>Sub Product</span></a></li>
                    </ul>
                 </li-->
                 <li><a href='<?=base_url()?>update-share-point'><span>Update Share Point</span></a></li>
                <li><a href='<?=base_url()?>admin-transfer-lot'><span>Transfer LOT</span></a></li>
                <li><a href='<?=base_url()?>admin-pencairan-point'><span>Pencairan Point</span></a></li>
              </ul>
           </li>
           <li class="has-sub open"><a href='#'><span>Monitoring Mutasi</span></a>
                <ul style="display: block;">
                    <li><a href='<?=base_url()?>admin-dana-bersama'><span>Dana Bersama</span></a></li>
                    <li><a href='<?=base_url()?>admin-administrasi'><span>Administrasi</span></a></li>
                    <li><a href='<?=base_url()?>admin-amal'><span>Amal</span></a></li>
                    <li><a href='<?=base_url()?>admin-stok-pendapatan'><span>Stok Pendapatan</span></a></li>
					<li><a href='<?=base_url()?>admin-mustasi-lot'><span>Mutasi LOT</span></a></li>
                </ul>
           </li>
           <li class="has-sub open"><a href='#'><span>PPOB</span></a>
                <ul style="display: block;">
                    <li><a href='<?=base_url()?>admin-ppob-pulsa'><span>Pulsa</span></a></li>
                    <li><a href='<?=base_url()?>admin-ppob-listrik'><span>Token Listrik</span></a></li>
                </ul>
           </li>
           <li class><a href='<?=base_url()?>fee-management'><span>Fee Management</span></a></li>
		   <?php if ($user->role==9) {?>
           <li class="has-sub open"><a href='#'><span>Daftar Admin</span></a>
                <ul style="display: block;">
                    <li><a href='<?=base_url()?>register-admin'><span>Registrasi Admin</span></a></li>
                    <li><a href='#'><span>Data Admin</span></a></li>
                </ul>
           </li>
		   <?php } ?>
           <li class="has-sub open"><a href='#'><span>Data Member</span></a>
                <ul style="display: block;">
                    <li><a href='<?=base_url()?>list-member'><span>Daftar Member</span></a></li>
                    <li><a href='<?=base_url()?>list-leader'><span>Daftar Leader</span></a></li>
                </ul>
           </li>
        </ul>
    </div>
    <div class="visible-xs" id='cssmenu'>
        <ul>
           <li><a href='<?=base_url()?>admin'><span>Dashboard</span></a></li>
           <li><a href='#'><span>Transaksi</span></a>
              <ul>
                <li><a href='<?=base_url()?>update-share-point'><span>Update Share Point</span></a></li>
                <li><a href='<?=base_url()?>admin-transfer-lot'><span>Transfer LOT</span></a></li>
                <li><a href='<?=base_url()?>admin-pencairan-point'><span>Pencairan Point</span></a></li>
              </ul>
           </li>
           <li><a href='#'><span>Monitoring Mutasi</span></a>
                <ul>
                    <li><a href='<?=base_url()?>admin-dana-bersama'><span>Dana Bersama</span></a></li>
                    <li><a href='<?=base_url()?>admin-administrasi'><span>Administrasi</span></a></li>
                    <li><a href='<?=base_url()?>admin-amal'><span>Amal</span></a></li>
                    <li><a href='<?=base_url()?>admin-stok-pendapatan'><span>Stok Pendapatan</span></a></li>
                    <li><a href='<?=base_url()?>admin-mustasi-lot'><span>Mutasi LOT</span></a></li>
                </ul>
           </li>
           <li class="has-sub open"><a href='#'><span>PPOB</span></a>
                <ul style="display: block;">
                    <li><a href='<?=base_url()?>admin-ppob-pulsa'><span>Pulsa</span></a></li>
                    <li><a href='<?=base_url()?>admin-ppob-listrik'><span>Token Listrik</span></a></li>
                </ul>
           </li>
           <li class><a href='<?=base_url()?>fee-management'><span>Fee Management</span></a></li>
		   <?php if ($user->role==9) {?>
           <li><a href='#'><span>Daftar Admin</span></a>
                <ul>
                    <li><a href='<?=base_url()?>register-admin'><span>Registrasi Admin</span></a></li>
                    <li><a href='#'><span>Data Admin</span></a></li>
                </ul>
           </li>
		   <?php } ?>
           <li><a href='#'><span>Data Member</span></a>
                <ul>
                    <li><a href='<?=base_url()?>list-member'><span>Daftar Member</span></a></li>
                    <li><a href='<?=base_url()?>list-leader'><span>Daftar Leader</span></a></li>
                </ul>
           </li>
       </ul>
    </div>
</div>
<script>
$(function() {
     var pgurl = window.location.href;
     $("#cssmenuv ul li").each(function(){
          if($(this).attr("href") == pgurl || $(this).attr("href") == '' )
          $(this).addClass("static-page-menu-active");
     })
});
</script>