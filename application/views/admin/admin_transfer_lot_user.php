    <div class="col-md-9 col-thumbnail">
            <div class="thumbnail thumbnail-dashboard">
            <div class="row">
                <div class="col-md-12 text-center" >
                    <h2 class="text-center card-header"></i> Form Transfer LOT</h2>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-offset-2 col-md-10" style="padding: 10px;">
                    <div class="col-md-offset-1 col-md-10 text-center">
                        <form id="saham-form" class="form-horizontal" style="margin: 0 20px">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Username Penerima</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control username" id="username" name="username"  value="" placeholder="">
                            </div>
                          </div>
                           <div class="form-group">
                            <label class="col-sm-4 control-label">Nama Penerima</label>
                            <div class="col-sm-8">
                              <input disabled type="text" class="form-control nama" id="nama" name="nama"  placeholder="">
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Kota</label>
                            <div class="col-sm-8">
                              <input  disabled type="text"  class="form-control kota" id="kota" name="kota"  value="" placeholder="">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">No. Handphone</label>
                            <div class="col-sm-8">
                              <input  disabled type="number" class="form-control hp" id="hp" name="hp"  value="" placeholder="">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">LOT yang akan dikirim</label>
                            <div class="col-sm-8">
                              <input type="number"  class="form-control pointlot" id="pointlot" name="pointlot"  value="0" placeholder="">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-12">
                              <button type="submit" class="btn btn-default btn-submit">Submit <span class="calculate"></span></button>
                            </div>
                          </div>
                          <div class="show-notifForm"></div>
                        </form>
                   </div> 
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</section>
<script>
/*
$('.jmlsaham').on('change keyup', function(){
    var curr = $(this).val();
    $('.calculate').html('');
    if(curr != ""){
        calculate = <?=$max_nominal?>*curr;
        $('.calculate').html('Rp '+formatNumber(calculate));
    }
});*/

$('#saham-form').formValidation('destroy').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
    username: {
            validators: {
                notEmpty: {
                    message: 'Silahkan masukan username!'
                },
                remote: {
                    message: 'Username tidak terdaftar',
                    url: '<?=base_url()?>admin/check_username',
                    type: 'POST',
                    delay: 1200,
                    data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',},
                }
            },
        },
        pointlot: {
            validators: {
                notEmpty: {
                    message: 'Harap diisi!'
                },
                greaterThan: {
                        value: 1,
                        message: 'Minimal 1'
                }
            }
        },
    }
}).on('success.form.fv', function (e) {
    e.preventDefault();
    var dataString = $("#saham-form").serialize();
    $(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i>');
    $(".btn-submit").attr("disabled", 'disabled');
    $.ajax({
    url: '<?=base_url()?>admin/do_trans_saham',
    type: 'POST',
    data: dataString,
    success: function(data) {
           
		   var nama = $('#username').val();
		   var point = $('#pointlot').val();
		   Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
			{
				msg: "Anda berhasil melakukan transfer LOT sebanyak "+point+" LOT ke "+nama,
				buttons: {
					ok: {
					'class': 'btn btn-default',
					closeOnClick: true
					},
				},
				callback: function(lobibox, type){
					window.location = '<?=base_url()?>admin-transfer-lot';
				}
			});
      }
    });
    return false;
}).on('err.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(true);
    }
}).on('success.field.fv', function (e, data) {
    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(false);
    }
});

$('#username').on('keyup', function(){
    username = $(this).val();
    $.ajax({
    url: '<?=base_url()?>admin/check_username',
    type: 'POST',
    data: { '<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',username : username },
    success: function(result) {
      var obj = jQuery.parseJSON(result);
        if(obj.valid){
		  $('#nama').val(obj.nama);
          $('#kota').val(obj.kota);
          $('#hp').val(obj.hp);
        }       
      }
    });
});     
</script>