        <div class="col-md-9 col-thumbnail">
            <?php if($is_new_member){ ?>
            <div class="thumbnail thumbnail-dashboard text-center" style="background: cyan;font-family: times-new-roman;">
                <h2>----Harap Dicatat----</h2>
                <b>Info akun anda<br />
                Akun untuk login : <?=$user->usertologin?></b> <br />
            </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-4">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: whitesmoke;">
                        <h2 class="text-center card-header" style="font-size:16px">DANA BERSAMA</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-point"><?=decimalNumber($dana_bersama)?></span> <span class="font-tiny">Point
                                <span class="pull-right"><i class="fa fa-group fa-4x"></i></span><br />
                                Rp. <?=currencyNumber($dana_bersama*10000)?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: whitesmoke;">
                        <h2 class="text-center card-header" style="font-size:16px">ADMINISTRASI</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-point"><?=decimalNumber($administrasi)?></span> <span class="font-tiny">Point 
                                <span class="pull-right"><i class="fa fa-server fa-4x"></i></span><br />
                                Rp. <?=currencyNumber($administrasi*10000)?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: whitesmoke;">
                        <h2 class="text-center card-header" style="font-size:16px">AMAL</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-point"><?=decimalNumber($amal)?></span> <span class="font-tiny">Point 
                                <span class="pull-right"><i class="fa fa-ambulance fa-4x"></i></span><br />
                                Rp. <?=currencyNumber($amal*10000)?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!--div class="col-md-4">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: whitesmoke;">
                        <h2 class="text-center card-header" style="font-size:16px">TOTAL PENDAPATAN</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-point"></span> <span class="font-tiny">Point <span class="pull-right"><i class="fa fa-suitcase fa-4x"></i></span><br />Rp. </span>
                            </div>
                        </div>
                    </div>
                </div-->
                <div class="clearfix"></div>
                <div class="col-md-6">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: whitesmoke;">
                        <h2 class="text-center card-header" style="font-size:16px">STOK PENDAPATAN</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-point"><?=decimalNumber($stock_pendapatan)?></span> <span class="font-tiny">Point 
                                <span class="pull-right"><i class="fa fa-cubes fa-4x"></i></span><br />
                                Rp. <?=currencyNumber($stock_pendapatan*10000)?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="thumbnail thumbnail-dashboard" style="background-color: whitesmoke;">
                        <h2 class="text-center card-header" style="font-size:16px">POINT YG DIBAGIKAN</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-point"><?=decimalNumber($shared_point)?></span> <span class="font-tiny">Point 
                                <span class="pull-right"><i class="fa fa-sitemap fa-4x"></i></span><br />
                                Rp. <?=currencyNumber($shared_point*10000)?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!--div class="col-md-4">
                    <div class="thumbnail thumbnail-dashboard">
                        Statistik Akun<br />
                        Total Penerima<br />
                        Total Full Profit<br />
                        Total Antrian<br>
                        Total LOT<br />
                        Total Member<br />
                        Free Member<br />
                    </div>
                </div-->
                <div class="clearfix"></div>
                <div class="col-md-12 table-responsive">
                    <div class="thumbnail thumbnail-dashboard">
                        <h1 class="arrow text-center">Request Pencairan Point</h1><br />
                        <table id="tab" class="table table-striped">
                            <thead>
                            <tr class="">
                                <th>Tanggal</th>
                                <th>User</th>
                                <th>Request</th>
                                <th>Nominal</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
						<?php $i = 0;
                            foreach($point_data as $data){ 
                                if($data->is_done == 0) {
                        ?>
                        <tr>
							<td><?=dateHours($data->date_transaction)?></td>
                            <td><?=$data->usertologin?></td>
							<td>Pencairan Point</td>
                            <td><?=decimalNumber($data->value)?></td>
                            <td><a class="btn btn-primary" href="<?=base_url()?>admin/proses_pencairan_point_page" role="button" id="action" style="margin-right: 10px">Action</a></td>    
                        </tr>
                        <?php $i++;}
							}
                        if($i == 0) {
						?>
                        <tr>
                            <td colspan="5" class="text-center"><em>-- Belum ada request --</em></td>
                        </tr>
                        <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
                <!--div class="col-md-12 table-responsive">
                    <div class="thumbnail thumbnail-dashboard">
                        <h1 class="arrow text-center">Request Aktifasi Akun</h1><br />
                        <table id="tab" class="table table-striped">
                            <thead>
                            <tr class="">
                                <th>Tanggal</th>
                                <th>User</th>
                                <th>Content</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
						<?php 
                        if(!empty($activate_data)){
                            foreach($activate_data as $data){ 
                        ?>
                        <tr>
							<td><?=dateHours($data->date_transaction)?></td>
                            <td><?=$data->usertologin?></td>
							<td><?=$data->description?></td>
                            <td><?=$data->email?></td>
                            <td><a class="btn btn-primary" href="<?=base_url()?>admin/proses_pencairan_point_page" role="button" id="action" style="margin-right: 10px">Action</a></td>    
                        </tr>
                        <?php 
							}
                        } else {
						?>
                        <tr>
                            <td colspan="5" class="text-center"><em>-- Belum ada request --</em></td>
                        </tr>
                        <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div-->
            </div>
        </div>
        
        <!--tag open in other page-->
    </div>
</div>
</section>
<script>
function doProses(id, usertologin, value, id_request, handphone)
{
    $.ajax({
         type: "POST",
         url: "<?=base_url()?>admin/do_process",
         data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>','id':id, 'usertologin':usertologin, 'value':value, 'id_request':id_request, 'handphone':handphone},
         success: function(msg){
            
			Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
			{
				msg: "Prose pencairan telah berhasil.",
				buttons: {
					ok: {
					'class': 'btn btn-info',
					closeOnClick: true
					},
				},
				callback: function(lobibox, type){
					window.location = '<?=base_url()?>admin-pencairan-point';
				}
			});
         }
    });
}
</script>