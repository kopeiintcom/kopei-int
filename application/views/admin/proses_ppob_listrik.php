        <div class="col-md-9 col-thumbnail">
            <div class="thumbnail thumbnail-dashboard">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <h1 class="arrow text-center">PPOB Token Listrik User</h1><br />
                    <span>Silahkan kirim format sms yang sudah diterima ke <strong><?=PPOB_SMS_CENTER?></strong></span><br />
                    <table id="tab" class="table table-striped">
                        <thead>
                        <tr class="">
							<th>Tanggal</th>
                            <th>User</th>
							<th>Nama</th>
                            <th>Point</th>
							<th>Keterangan</th>
                            <th>Action</th>       
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $z = 0;
                        
                        foreach($point_data as $data){ 
                        $z = $z + $data->value;
                        ?>
                        <tr>
							<td><?=dateHours($data->date_transaction)?></td>
                            <td><?=$data->usertologin?></td>
							<td><?=$data->name?></td>
                            <td><?=decimalNumber($data->value)?></td>
							<td><?=$data->description?></td>
                            <?php if($data->is_done==0){ ?>
                                <td><button type="button" title="Proses" class="btn btn-success btn-submit" 
								onclick="doProses('<?=$data->id_user?>','<?=$data->id_request?>')"><i class="fa fa-check fa-lg"></i></button>
                                <button type="button" title="Batal" class="btn btn-danger btn-submit" 
								onclick="doCancel('<?=$data->id_request?>')"><i class="fa fa-times fa-lg"></i></button>
                                </td>
                            <?php } else if($data->is_done==1){ ?>
                                <td>Selesai</td>
                            <?php } else if($data->is_done==2){ ?>
                                <td>Dibatalkan</td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
        <!--tag open in other page-->
    </div>
</div>
</section>
<!--tag open in other page-->
<script>
$(document).ready(function () {
    $('#tab').DataTable({
        "order": [[ 0, "desc" ]],
        "aoColumns": [null,{ "bSortable": false },{ "bSortable": false }]
    });
});

$('.btn-submit').click( function() {
    $('.btn-submit').attr('disabled','disabled');
    $(this).html('<i class="fa fa-spinner fa-spin"></i>');
});

function doProses(id_user, id_request)
{
    $.ajax({
         type: "POST",
         url: "<?=base_url()?>admin/do_proses_ppob_listrik",
         data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>','id_user':id_user,'id_request':id_request },
         success: function(msg){
            
			Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
			{
				msg: "Action Success",
				buttons: {
					ok: {
					'class': 'btn btn-default',
					closeOnClick: true
					},
				},
				callback: function(lobibox, type){
					window.location = '<?=base_url()?>admin-ppob-listrik';
				}
			});
         }
    });
}

function doCancel(id_request)
{
    $.ajax({
         type: "POST",
         url: "<?=base_url()?>admin/cancel_pencairan_point",
         data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>','id_request':id_request},
         success: function(msg){
            
			Lobibox.alert("success", //AVAILABLE TYPES: "error", "info", "success", "warning"
			{
				msg: "Pembelian dibatalkan.",
				buttons: {
					ok: {
					'class': 'btn btn-default',
					closeOnClick: true
					},
				},
				callback: function(lobibox, type){
					window.location = '<?=base_url()?>admin-ppob-pulsa';
				}
			});
         }
    });
}
</script>