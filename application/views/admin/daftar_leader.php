        <div class="col-md-9 col-thumbnail">
            <div class="thumbnail thumbnail-dashboard">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <h1 class="arrow text-center">Daftar Leader</h1>
                    <table id="tab" class="table table-striped">
                        <thead>
                        <tr class="">
                            <th>Username</th>
                            <th>Nama</th>
                            <th>Handphone</th>
                            <th>Alamat</th>
							<th>Kota</th>
							<th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $z = 0;
                        foreach($data_member as $data){?>
                        <tr>   
                            <td><?=$data->usertologin?></td>
                            <td><?=$data->name?></td>
                            <td><?=$data->handphone?></td>
                            <td><?=$data->address?></td>
                            <td><?=$data->city?></td>
							<td><a  title="Detail" class="btn btn-sm btn-default" 
								href="<?=base_url()?>admin/detail_member/<?=$data->id_user?>"><span class="fa fa-file-text"></span></button>
                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
        <!--tag open in other page-->
    </div>
</div>
</section>
<!--tag open in other page-->
<script>
$(document).ready(function () {
    $('#tab').DataTable( {
        "order": [[ 0, "asc" ]]
    } );
});

function doProses(id)
{
    $.ajax({
         type: "POST",
         url: "<?=base_url()?>admin/change_status_leader",
         data: {'id':id},
         success: function(msg){
            window.location = '<?=base_url()?>admin/daftar_leader_page';
         }
    });
}

function doReset(id)
{
    $.ajax({
         type: "POST",
         url: "<?=base_url()?>admin/reset_password",
         data: {'id':id},
         success: function(msg){
            window.location = '<?=base_url()?>admin/daftar_leader_page';
         }
    });
}
</script>