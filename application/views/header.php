<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height"/>
    <title><?=$segment?> Kopei-int.com</title>
    <meta name="description" content="website bisnis komunitas">
    <meta name="keywords" content="bisnis komunitas">
    <link rel="image_src" href="">
    <link rel="icon" href="<?=base_url()?>assets/images/favicon.ico">
    <link rel="publisher" href="">
    <meta property="og:title" content="kopei-int.com">
    <meta property="og:type" content="article">
    <meta property="og:url" content="http://kopei-int.com">
    <meta property="og:image" content="http://kopei-int.com/assets/images/logo.png">

    <!-- CSS -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300,800' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url(); ?>assets/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/site.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/flexslider.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/fancybox.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/offcanvas.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/datepicker/bootstrap-datepicker3.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/datepicker/bootstrap-datepicker3.standalone.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/slidertime/bootstrap-slider.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/portfolio.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/styles2.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url(); ?>assets/css/cssmenu.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/cssmenuv.css" rel="stylesheet">
    <!-- Gallery -->
    <link href="<?php echo base_url(); ?>assets/css/gallery.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/formvalidation/dist/css/formValidation.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Select2 CSS -->
    <link href="<?=base_url();?>assets/css/select2.css" rel="stylesheet">
    <!-- End SS -->

    <!-- JS -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.flexslider.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/slimscroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-slider.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/owl.carousel.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/classie.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/sidebarEffects.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/modernizr.js"></script>
    <script src="<?php echo base_url(); ?>assets/formvalidation/dist/js/formValidation.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/formvalidation/dist/js/framework/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.mask.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.cookie.js"></script>
    <!--script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script-->
    <script src="<?php echo base_url(); ?>assets/js/cssmenu.js"></script>
    <!-- Select2 -->
    <script src="<?=base_url();?>assets/js/select2.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript">dataLayer = [];</script>
	<!--Lobibox-->
	<link href="<?php echo base_url(); ?>assets/lobibox/dist/css/lobibox.min.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>assets/lobibox/dist/js/lobibox.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- End JS -->
    <script>   
    // Add the following into your HEAD section
    var timer = 0;
    function set_interval() {
      // the interval 'timer' is set as soon as the page loads
      timer = setInterval("auto_logout()", 600000);
      // the figure '10000' above indicates how many milliseconds the timer be set to.
      // Eg: to set it to 5 mins, calculate 5min = 5x60 = 300 sec = 300,000 millisec.
      // So set it to 300000
    }
    
    function reset_interval() {
      //resets the timer. The timer is reset on each of the below events:
      // 1. mousemove   2. mouseclick   3. key press 4. scroliing
      //first step: clear the existing timer
    
      if (timer != 0) {
        clearInterval(timer);
        timer = 0;
        // second step: implement the timer again
        timer = setInterval("auto_logout()", 600000);
        // completed the reset of the timer
      }
    }
    
    function auto_logout() {
      // this function will redirect the user to the logout script
      window.location = "<?=base_url()?>logout";
    }
    
    
    function onTestChange() {
        var key = window.event.keyCode;
    
        // If the user has pressed enter
        if (key == 13) {
            event.preventDefault();
    		var s = $(this).val();
    		$(this).val(s+"\n");
        }
    }
    
    function randomString(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }
    
    var newwindow;
    function poptastic(url)
    {
    	newwindow=window.open(url,'name','height=500,width=650');
    	if (window.focus) {newwindow.focus()}
    }
    </script>
</head>

<body id="" class="soft-bg" onload="set_interval()" onmousemove="reset_interval()" onclick="reset_interval()"
onkeypress="reset_interval()" onscroll="reset_interval()">

<script>
function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
}

//to normalize dot number
function normalNumber(num) {
    return num.toString().replace(/\./g, '');
}

$('.noEnter').keyup(function(event){
    if(event.keyCode == 10 || event.keyCode == 13) return;
    event.preventDefault();
});

//thousand give dot when outfocus
$('.dotdigit').focusout(function(event) {

    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;

    // format number
    $(this).val(function(index, value) {
      return value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
      ;
    });
});

//thousand remove dot when onfocus
$('.dotdigit').focusin(function(event) {

    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;

    // format number
    $(this).val(function(index, value) {
      return value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, "")
      ;
    });
});
</script>
