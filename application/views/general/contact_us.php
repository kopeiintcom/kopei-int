<section class="soft-bg">
<section style="padding-top: 55px;">
    <div class="container static-contact-map">
        <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=0.909434%2C104.495320&key=AIzaSyDMyz53XVtdBrQ0Q52sFKfjN7cbDYepboI" allowfullscreen></iframe>
    </div> 
</section>
<section class="static-content-border" style="padding-top: 20px;">
    <div class="container static-contact-content">
        <div class="col-md-6 contact-us-wrap">
            <p class="static-contact-title">Contact Us</p>
            <p class="static-content-text-body"> Jl. Hanjoyo Putro BT. 8 Atas<br />
            Komplek Ruko Gesya Blok A-44</p>
            <!--p class="static-content-text-body"><i class="fa fa-phone"></i> 021 123 4567--><br />
            <i class="fa fa-envelope"></i> <a class ="static-contact-email" href="mailto:info@kopei-int.com?Subject=Hello" target="_top">info@kopei-int.com</a></p>
        </div>
        <div class="col-md-6 contact-us-wrap">
            <p class="static-contact-text-body">JIKA ANDA ADA PERTANYAAN SEPUTAR KOPEI-INT SILAHKAN HUBUNGI KONSULTAN KAMI :</p>
            <p class="static-content-text-body"> 
            1. BAPAK HARY <i class="fa fa-phone"></i> 0852-6365-6035<br />
            2. BAPAK SYAMSUL <i class="fa fa-phone"></i> 0813-6480-5484<br />
            3. IBU DYAH <i class="fa fa-phone"></i> 0812-7726-5185<br />
            4. BAPAK SAFARUDDIN <i class="fa fa-phone"></i> 0813-7109-9737<br />
            5. BAPAK MURYONO <i class="fa fa-phone"></i> 0823-9293-1029<br />
            6. BAPAK ASUN <i class="fa fa-phone"></i> 0813-6472-8436<br />
            </p>
            <!--p class="static-content-text-body"><i class="fa fa-phone"></i> 021 123 4567--><br />
            
        </div>
    </div>
</section>

</section>

<script type="text/javascript">
    
    $('#contact-us-form').formValidation('destroy').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Email diperlukan'
                        },
                        emailAddress: {
                            message: 'Inputan bukan alamat email yang valid'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Nama diperlukan'
                        }
                    }
                },
                handphone: {
                    validators: {
                        notEmpty: {
                            message: 'No. Handphone diperlukan'
                        }
                    }
                },
                message: {
                    validators: {
                        notEmpty: {
                            message: 'Pesan diperlukan'
                        }
                    }
                },
            }
        }).on('success.form.fv', function (e) {
            e.preventDefault();
            var dataString = $("#contact-us-form").serialize();
            $(".contact-us-tag").html('<i class="fa fa-spinner fa-spin"></i>');
            $(".contact-us-tag").attr("disabled", 'disabled');
            //alert(dataString);
            $.ajax({
            url: '<?=base_url()?>mulmut/contact_us_email',
            type: 'POST',
            data: dataString,
            success: function(data) {
              var obj = jQuery.parseJSON(data);
              if (obj.hasOwnProperty("false")) {
                $(".contact-us-tag").html('Kirim Pesan');
                $(".contact-us-tag").removeAttr("disabled");
                $('#contact-us-form .show-notifForm' ).empty();
                $('#contact-us-form .show-notifForm' ).append( '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops! </strong>'+obj.false+'</div>' );
              } else {
                $(".loading").html('<div class="loading text-center" style="margin:100px 0"><i class="fa fa-spinner fa-spin fa-4x"></i></div>');
                $(".contact-us-tag").hide();
                $('#contact-us-form .show-notifForm').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="success" aria-hidden="true">×</button><strong></strong>' + obj.message + '</div>');
              }
            }
            });
            return false;
        }).on('err.field.fv', function (e, data) {
            if (data.fv.getSubmitButton()) {
                data.fv.disableSubmitButtons(true);
            }
        }).on('success.field.fv', function (e, data) {
            if (data.fv.getSubmitButton()) {
                data.fv.disableSubmitButtons(false);
            }
        });
</script>