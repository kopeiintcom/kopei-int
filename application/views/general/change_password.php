<section class="user-secure section-padding bg-line-center bg-white">
    <div class="big_font text-center">Pengaturan Password</div>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class='panel panel-info'>
                    <div class='panel-heading'>
                        <h3 class='panel-title'>Ubah Password dan PIN</h3>
                    </div>
                    <div class='panel-body'>
                        <form id="pressChange">
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                            <div class="form-group">
                                <input type="password" name="forgetpass" id="forgetpass" class="form-control" placeholder="Password"
                                       required="">
                            </div>
                            <div class="form-group">
                                <input type="password" name="repass" id="repass" class="form-control"
                                       placeholder="Konfirmasi Password" required="">
                            </div>
                            <div class="form-group">
                                <input type="password" name="forgetpin" id="forgetpin" class="form-control" placeholder="PIN"
                                       required="">
                            </div>
                            <div class="form-group">
                                <input type="password" name="repin" id="repin" class="form-control"
                                       placeholder="Konfirmasi PIN" required="">
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="" id="link" value="<?php echo $link; ?>"/>
                                <button type="submit" id="changePass" class="btn btn-danger btn-block">Submit
                                </button>
                            </div>
                            <div class="form-group">
                                <div id="targetretype"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $('#pressChange').formValidation('destroy').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                forgetpass: {
                    validators: {
                        notEmpty: {
                            message: 'Password diperlukan'
                        }
                    }
                },
                repass: {
                    validators: {
                        notEmpty: {
                            message: 'Konfirmasi password diperlukan'
                        },
                        identical: {
                            field: 'forgetpass',
                            message: 'Password tidak sama'
                        }
                    }
                },
                forgetpin: {
                    validators: {
                        notEmpty: {
                            message: 'PIN diperlukan'
                        }
                    }
                },
                repin: {
                    validators: {
                        notEmpty: {
                            message: 'Konfirmasi PIN diperlukan'
                        },
                        identical: {
                            field: 'forgetpin',
                            message: 'PIN tidak sama'
                        }
                    }
                },
            }
        }).on('success.form.fv', function (e) {
            var pass = $("#forgetpass").val();
            var repass = $("#repass").val();
            var link = $("#link").val();

            $.ajax({
                type: "POST",
                data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',pass: pass, repass: repass, link: link},
                url: "<?php echo base_url('mulmut/set_pass'); ?>",
                error: function (xhr, textStatus, errorThrown) {
                    console.log(xhr);
                }
            }).done(function (data) {
                $("#targetretype").html(data);
            }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(errorThrown);
            });
            return false;
        });
    });
</script>