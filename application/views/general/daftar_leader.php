<section class="light-bg" style="padding-top:53px">
    <div class="container" >
        <div class="row" style="margin:00px 0">
            <div class="col-md-12 table-responsive">
                <br /><br /><h1 class="arrow text-center">Daftar Leader</h1><br />
                <table id="tab" class="table table-striped">
                    <thead>
                    <tr class="">
                        <th>Username</th>
                        <th>Nama Lengkap</th>
                        <th>Kota</th>        
                        <th>No. Handphone</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                    foreach($leader as $data){ ?> 
                    <tr>
                        <td><?=$data->usertologin?></td>
                        <td><?=$data->name?></td>
                        <td><?=$data->city?></td>
                        <td><?=$data->handphone?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>       
<script>
$(document).ready(function () {
    $('#tab').DataTable( {
        "order": [[ 0, "asc" ]]
    } );
});
</script>