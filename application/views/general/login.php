<div class="soft-bg">
    <div class="container" style="padding:50px 0 50px 0;">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="thumbnail">
                    <div class="caption">
                        <div class="wrap-login page-header">
                            <form autocomplete="on">
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                            <div class="text-center page-header"> 
                                <a href="<?=base_url()?>"><img style="display:inline-block;width: 60%;" src="<?=base_url()?>assets/images/logo.png" /></a>
                            </div><br />
                            <div class="form-group">
                                <label>Username</label>
                                <input id="user_form" name="usertologin" type="text" class="form-control" placeholder="User">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input id="pass_form" name="password" type="password" class="form-control" placeholder="Password">
                            </div>
                            <div id="notif-block" class="alert alert-warning" style="display:none" role="alert">
                                <i id="notif-content"></i>
                            </div>
                            <a id="logins" type="button" class="btn btn-success">Log In</a>
                            </form>
                        </div>
                        <div class="wrap-forget text-center">
                            <button id="forget_password" class="btn btn-info">Forget Password</button>
                        </div>
                        <div class="form-forget text-center">
                            <form autocomplete="on">
                                <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                <div class="text-center page-header"> 
                                    <b>FORGET PASSWORD</b>
                                </div><br />
                                <div class="form-group">
                                    <label>Username anda</label>
                                    <input id="username" name="username" type="text" class="form-control" placeholder="Username">
                                </div>
                                <div id="notif-block2" class="alert alert-warning" style="display:none" role="alert">
                                    <i id="notif-content2"></i>
                                </div>
                                <a id="forgets" type="button" class="btn btn-success">Submit</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>

<script>
$('.form-forget').hide();

$('input').keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
    login_click(); 
  }
});

$("#logins").click(function() {
    login_click();
});

function login_click(){
    var usertologin = $("#user_form").val().trim();
    var password = $("#pass_form").val().trim();
    $("#logins").html('<i class="fa fa-spinner fa-spin"></i>');
    $("#logins").attr("disabled", 'disabled');

    $.ajax({
        type: "POST",
        data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',usertologin: usertologin, password: password},
        dataType: "json",
        url: "<?=base_url()?>authenticate/login",
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
        }
    }).done(function (data) {
        if (data.name) {
            $("#login").attr("disabled", true);
            window.location.href = "<?php echo site_url('authenticate/check_role'); ?>";
        }
        else {
            $("#logins").html('Log In');
            $("#logins").removeAttr("disabled");
            $("#notif-block").show(400);
            $("#notif-content").html(data.false);
            $("#notif-block").delay(3000).hide(400);
        }
    });
};

$("#forget_password").click(function() {
    $('.wrap-forget').hide();
    $('.wrap-login').hide();
    $('.form-forget').show();
});

$("#forgets").click(function() {
    forget_click();
});

function forget_click(){
    var username= $("#username").val().trim();
    $("#forgets").html('<i class="fa fa-spinner fa-spin"></i>');
    $("#forgets").attr("disabled", 'disabled');

    $.ajax({
        type: "POST",
        data: {'<?=$this->security->get_csrf_token_name();?>':'<?=$this->security->get_csrf_hash();?>',username: username},
        dataType: "json",
        url: "<?=base_url()?>authenticate/forget_password",
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
        }
    }).done(function (data) {
        if (data.success) {
            $("#forgets").attr("disabled", true);
            $("#forgets").html('Sent');
            $("#notif-block2").show(400);
            $("#notif-content2").html('Link Forget Password dikirimkan ke email anda, silahkan cek email anda');
            //window.location.href = "<?php echo site_url('authenticate/check_role'); ?>";
        }
        else {
            $("#forgets").html('Submit');
            $("#forgets").removeAttr("disabled");
            $("#notif-block2").show(400);
            $("#notif-content2").html(data.false);
            $("#notif-block2").delay(3000).hide(400);
        }
    });
};
</script>