<section class="light-bg" style="padding-top:53px">
    <div class="container" >
        <div class="row" style="margin:20px 0">
            <div class="col-md-12 text-center">
                <div style="margin-top: 15px;width: 60%;" class="img-thumbnail">
                    <img class="img-responsive" src="<?=base_url()?>assets/images/legalitas/1.jpg" />
                    <img class="img-responsive" src="<?=base_url()?>assets/images/legalitas/2.jpg" />
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-offset-2 col-md-4 text-center">
                <div style="margin: 15px 0;">
                    <h1>AKTA PERUSAHAAN</h1><br />
                    <img style="width: 95%;" class="img-responsive img-thumbnail" src="<?=base_url()?>assets/images/legalitas/4.jpg" />
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin: 15px 0;">
                    <h1>IZIN USAHA</h1><br />
                    <img style="width: 100%;" class="img-responsive img-thumbnail" src="<?=base_url()?>assets/images/legalitas/5.jpg" />
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 text-center">
                <div style="margin: 15px 0;">
                    <h1>NPWP</h1><br />
                    <img style="width: 30%;" class="img-responsive img-thumbnail" src="<?=base_url()?>assets/images/legalitas/3.jpg" />
                </div>
            </div>
        </div>
    </div>
</section>
