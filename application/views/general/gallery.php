<section class="light-bg" style="padding-top:60px">
    <div class="container" >
        <div class="row" style="margin:00px 0">
            <div class="col-md-12 text-center"><h1>GALLERY</h1><br /></div>
            <div class="clearfix"></div>
            <div class="col-md-offset-1 col-md-10">
                <div id="mygallery" class="gallery">
                    <div class="images">
                    <?php for($i=1;$i<=18;$i++) { 
                            if ($i == 1) { 
                        ?>
                      <div class="active" style="background-image: url(<?=base_url().PATH_IMAGE_GALLERY.$i?>.JPG)"></div>
                    <?php } else {?>
                      <div style="background-image: url(<?=base_url().PATH_IMAGE_GALLERY.$i?>.JPG)"></div>
                    <?php } } ?>
                      <span class="left"></span>
                      <span class="right"></span>
                    </div>
                    <div class="thumbs">
                      <?php for($i=1;$i<=18;$i++) { 
                            if ($i == 1) { 
                        ?>
                      <div class="active" style="background-image: url(<?=base_url().PATH_IMAGE_GALLERY.$i?>.JPG)"></div>
                    <?php } else {?>
                      <div style="background-image: url(<?=base_url().PATH_IMAGE_GALLERY.$i?>.JPG)"></div>
                    <?php } } ?>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</section>
<script src="<?php echo base_url(); ?>assets/js/hammer.min.js"></script>
<!--script src="dist/js/gallery.min.js"></script-->
<script src="<?php echo base_url(); ?>assets/js/main.min.js"></script>       