<!-- START SLIDER -->
<section class="testimony-photo" style="padding-top:53px">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img class="" width="100%" src="<?php echo base_url().PATH_IMAGE_SLIDER; ?>1.jpg" alt="sa-naturalland">
            </div>
            <div class="item">
                <img class="" width="100%" src="<?php echo base_url().PATH_IMAGE_SLIDER; ?>2.jpg" alt="sa-naturalland">
            </div>
            <div class="item">
                <img class="" width="100%" src="<?php echo base_url().PATH_IMAGE_SLIDER; ?>3.jpg" alt="sa-naturalland">
            </div>
            <div class="item">
                <img class="" width="100%" src="<?php echo base_url().PATH_IMAGE_SLIDER; ?>4.jpg" alt="sa-naturalland">
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<!-- END SLIDER -->
<section class="soft-bg">
    <div class="container" >
        <div class="row" style="margin:40px 25px 40px 25px">
            <div class="col-md-12 text-center" style="">
                <h1>MARKETING PLAN KOPEI-INT</h1><br />
                <span style="font-size: 16px;padding: 20px;">Sistem Marketing Plan yang bertujuan untuk memberikan segudang manfaat bagi seluruh membernya Tanpa terkecuali 
                Didukung dengan perancangan Sistem yang Dinamis dan berkesinambungan jangka Panjang</span>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:10px"><i class="fa fa-dollar fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>Total Penghasilan</h2>
                    <h3>Total Kontrak Per Lot dengan nilai bagi hasil 110 point</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:0px"><i class="fa fa-sitemap fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>Bonus Jaringan</h2>
                    <h3>Bonus Sponsor 2.5 Point dan Bonus Generasi 10 Point dibagi Kedalaman 15 Level</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:0px"><i class="fa fa-archive fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>Penghasilan Harian</h2>
                    <h3>Bagi hasil yang adil sesuai tingkat pertumbuhan Lot dan Keuntungan Bisnis Riil</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:0px"><i class="fa fa-briefcase fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>Stok Penghasilan</h2>
                    <h3>Tabungan penghasilan yang menjadi Stok dan pasti dibagikan</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:0px"><i class="fa fa-leaf fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>Dana Bersama</h2>
                    <h3>Dana Bersama berasal dari setiap Lot yang diposting Member dengan sejumlah 10 Point/Lot</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:0px"><i class="fa fa-ambulance fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>Dana Amal</h2>
                    <h3>Setiap Member membeli Lot maka dia telah menyumbang 0.3 Point/Lot untuk dibagikan kepada yang berhak</h3>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="light-bg">
    <div class="container" >
        <div class="row" style="margin:40px 25px 40px 25px">
            <div class="col-md-12 text-center" style="">
                <h1>BISNIS NYATA KOPEI-INT</h1><br />
                <span style="font-size: 16px;padding: 20px;">Kami menyediakan Pelayanan Bisnis nyata yang menjadi 
                sebuah Fasilitas Khusus bagi Member ataupun Non Member dalam pemenuhan kebutuhan sehari-hari Full 
                24 Jam Non-Stop</span>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:10px"><i class="fa fa-filter fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>PULSA ELEKTRIK</h2>
                    <h3>Kami menyediakan Fasilitas Pengisian Pulsa semua Nomor All Operator Harga Distributor</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:10px"><i class="fa fa-flash fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>PLN</h2>
                    <h3>Kami Menyediakan Fasilitas Pembayaran PLN Pasca Bayar dan Pra Bayar (TOKEN)</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="padding-top: 30px;">
                    <span style="padding:0px"><i class="fa fa-tv fa-4x"></i></span>
                </div>
                <div class="col-md-9">
                    <h2>TV Berlangganan</h2>
                    <h3>Kami menyediakan fasilitas pembayaran TV Berlangganan Anda dengan respon cepat</h3>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
