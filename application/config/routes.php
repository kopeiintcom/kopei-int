<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'General';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//home or static page
$route['about-us'] = 'general/about_us';
$route['contact-us'] = 'general/contact_us';
$route['register'] = 'general/register';
$route['login'] = 'authenticate';
$route['logout'] = 'authenticate/logout';
$route['marketing'] = 'general/marketing';
$route['legalitas'] = 'general/legalitas';
$route['leader'] = 'general/daftar_leader';
$route['gallery'] = 'general/gallery';
$route['email'] = 'general/roundcube_email';
$route['link/(:any)'] = 'authenticate/link/$1';

//user
$route['dashboard'] = 'user/dashboard_page';
$route['register-saham'] = 'user/register_saham';
$route['aktivasi-saham'] = 'user/aktivasi_saham';
$route['amal-anda'] = 'user/amal_page';
$route['transfer-lot'] = 'user/user_transfer_lot';
$route['pencairan-point'] = 'user/pencairan_point';
$route['profile'] = 'user/user_profile';
$route['saldo-pendapatan'] = 'user/saldo_pendapatan';
$route['saldo-keluar'] = 'user/saldo_keluar';
$route['saldo-masuk'] = 'user/saldo_masuk';
$route['stok-pendapatan'] = 'user/stok_pendapatan';
$route['dana-bersama'] = 'user/dana_bersama';
$route['transfer-saldo'] = 'user/transfer_saldo';
$route['mutasi-lot'] = 'user/mustasi_lot_page';
$route['sponsorisasi'] = 'user/sponsorisasi_page';
$route['ppob-pulsa'] = 'user/ppob_pulsa';
$route['ppob-listrik'] = 'user/ppob_listrik';

//admin
$route['admin'] = 'admin/dashboard_page';
$route['register-admin'] = 'admin/register_admin';
$route['update-share-point'] = 'admin/keuntungan_harian';
$route['admin-transfer-lot'] = 'admin/admin_transfer_lot';
$route['admin-profile'] = 'admin/admin_profile';
$route['admin-pencairan-point'] = 'admin/proses_pencairan_point_page';
$route['list-member'] = 'admin/daftar_member_page';
$route['list-leader'] = 'admin/daftar_leader_page';
$route['fee-management'] = 'admin/fee_management';
$route['admin-stok-pendapatan'] = 'admin/stok_pendapatan';
$route['admin-dana-bersama'] = 'admin/dana_bersama';
$route['admin-amal'] = 'admin/amal';
$route['admin-administrasi'] = 'admin/administrasi';
$route['admin-mustasi-lot'] = 'admin/mustasi_lot_page';
$route['admin-ppob-pulsa'] = 'admin/proses_ppob_pulsa';
$route['admin-ppob-listrik'] = 'admin/proses_ppob_listrik';

//cron
$route['simulasi'] = 'cron';


