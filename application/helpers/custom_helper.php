<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('custom_helper'))
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('authenticate_m', 'AM');
    }

    function raja_sms($phone_number,$message)
    {
        $username = RAJASMS_USERNAME;
        $password = RAJASMS_PASSWORD;
        $key = RAJASMS_APIKEY;
        
        //$url = RAJASMS_URL;

        $curlHandle = curl_init();
		$url="http://162.211.84.203/sms/smsreguler.php?username=".urlencode($username)."&password=".urlencode($password)."&key=".$key."&number=".$phone_number."&message=".urlencode($message);
		curl_setopt($curlHandle, CURLOPT_URL,$url);
		curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curlHandle, CURLOPT_TIMEOUT,120);
		$hasil = curl_exec($curlHandle);
		curl_close($curlHandle);	
		return $hasil;
        
    }
    
    function zenziva_sms($phone_number,$message)
    {
        $userkey = ZENZIVA_USERKEY;
        $passkey = ZENZIVA_PASSKEY;

        $url = ZENZIVA_URL;

        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey=' . $userkey . '&passkey=' . $passkey . '&nohp=' . $phone_number . '&pesan=' . urlencode($message));
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        $results = curl_exec($curlHandle);
        curl_close($curlHandle);

        return $results;
    } 

    //function sendgrid_email($from, $fromname, $to, $message, $subject, $bcc='')
    function sendinblue_email()
    {
        $mailin = get_instance();    
        
        $mailin->load->library('mailin');
        
		$mailin->mailin->
    	addTo('taufiqhidayat1611.th@gmail.com', 'Taufiq Hidayat')->
    	setFrom('taufiqhidayat1611.th@gmail.com', 'Taufiq Hidayat')->
    	setReplyTo('taufiqhidayat1611.th@gmail.com','Taufiq Hidayat')->
    	setSubject('Tes sendinblue')->
    	setText('yeyah yeah')->
    	setHtml('<strong>Hello do you hear me</strong>');
        $res = $mailin->mailin->send();
        /**
        Successful send message will be returned in this format:
        {'result' => true, 'message' => 'Email sent'}
        */
          
    }

    function replaceDot($input)
    {
        return str_replace(".","",$input);
    }

    function replaceSpaceToDash($input)
    {
        return str_replace(" ","-",$input);
    }

    function removeAlpha($input)
    {
        return preg_replace("/[^0-9,.]/", "", $input);
    }
    
    function decimalNumber($input)
    {
        return number_format($input,'2',',','.');
    }
    
    function currencyNumber($input)
    {
        return number_format($input,'0',',','.');
    }
    
    function dateGeneral($input)
    {
        return date_format(date_create($input),'d M Y');
    }
    
	function dateHours($input)
    {
        return date_format(date_create($input),'d M Y, H:i');
    }
	
    function is_valid_member_seller()
    {
        $CI = get_instance();
        $sess = $CI->session->userdata('datauser');

        //check login
        if(empty($sess)){
            //destroy the session, like logout
            $CI->session->sess_destroy();
            redirect('/');
        }

        $user_data = $CI->ST->get_by_id_single_row('ki_m_user',$sess['id_user'],'id_user');
        
        if($user_data->role == ADMIN){
            redirect('/login');
        }

    }
    
    function is_valid_admin()
    {
        $CI = get_instance();
        $sess = $CI->session->userdata('datauser');

        //check login
        if(empty($sess)){
            //destroy the session, like logout
            $CI->session->sess_destroy();
            redirect('/');
        }

        $user_data = $CI->ST->get_by_id_single_row('ki_m_user',$sess['id_user'],'id_user');
        
        if($user_data->role == MEMBER || $user_data->role == SELLER){
            redirect('/login');
        }

    }
    
    function redirectToHTTPS()
    {
        if( in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ) ) ) { // debug mode on localhost ('127.0.0.1' IP in IPv4 and IPv6 formats)
        } else {
          if($_SERVER['HTTPS']!="on")
          {
             $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
             header("Location:$redirect");
          }
        }
    }
    
    function update_remember_token()
    {
        $CI = get_instance();
        $sess = $CI->session->userdata('datauser');

        //update database remember_token
        $condition = [
            'id_user' => $sess['id_user']
        ];
        $data_1 = [
            'remember_token' => bin2hex(openssl_random_pseudo_bytes(16)),
        ];
        $CI->ST->update_array('ki_m_user',$data_1,$condition);

        $user = $CI->ST->get_by_id_single_row('ki_m_user',$sess['id_user'],'id_user');

        $session_user = array(
            'id_user' => $user->id_user,
            'name' => $user->name,
            'usertologin' => $user->usertologin,
            'email' => $user->email,
            'logged_in' => TRUE,
            'role' => $user->role,
            'handphone' => $user->handphone,
            'remember_token' => $data_1['remember_token'],
        );
        
        //update session 
        $CI->session->set_userdata('datauser', $session_user);
    }
    
    function check_token()
    {
        $CI = get_instance();
        $sess = $CI->session->userdata('datauser');

        $user = $CI->ST->get_by_id_single_row('ki_m_user',$sess['id_user'],'id_user');

        //check token session with token in database
        if(!isset($sess['remember_token']) || $sess['remember_token'] != $user->remember_token){
            //destroy the session, like logout
            $CI->session->sess_destroy();
            redirect('/');
        }
    }
    
    function send_email_mime($email,$subject,$message)
    {
        $name = KOPEIINT_EMAIL_FROMNAME;
        //$email = 'taufiqhidayat1611.th@gmail.com';
//        $subject = 'testing_mail';
//        $message = 'yeay berhasil';
        
        $to=$email;
        
        $message="From:$name <br /><br /><br /><br />".$message;
        
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        
        // More headers
        $headers .= 'From: <admin@kopei-int.com>' . "\r\n";
        //$headers .= 'Cc: admin@yourdomain.com' . "\r\n";
        @mail($to,$subject,$message,$headers);
        //if(@mail)
//        {
//        echo "Email sent successfully !!";	
//        }
    }

}