<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

    public $message;
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('setting_m', 'ST');
        $this->load->model('user_m', 'UM');
        $this->load->model('authenticate_m', 'AM');
        $this->load->model('transaction_m', 'TM');
        $this->load->model('request_m', 'RM');
        redirectToHTTPS();        
        is_valid_member_seller();
        check_token();
        update_remember_token();
        die('<h1 style="text-align:center;margin-top:150px;">-------WE ARE UNDER MAINTENANCE-------</h1><br /><h3 style="text-align:center;">Please wait until we finish this for you</h3><h3 style="text-align:center;color:lightblue;">kopei-int.com</h3><h4 style="text-align:center;">-Komunitas Pejuang Ekonomi Internasional-</h4>');
    }
    
    public function dashboard_page()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Dashboard |';
        $data['is_new_member'] = $this->UM->is_new_member($id_user);
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['dana_bersama'] = $this->TM->get_dana_bersama();
        $data['stok_pendapatan'] = $this->TM->get_stock_pendapatan();
		$data['current_profit'] = $this->get_total_dibagikan($id_user);
        $data['total_pendapatan'] = $this->get_total_pendapatan_harian($id_user);
		// rincian profit
        $data['yesterday_profit'] = $this->TM->get_profit_yesterday($id_user);
		$data['current_slot'] = $this->TM->get_current_slot();
		$data['yesterday_slot'] = $this->TM->get_yesterday_slot();
		$data['current_user'] = $this->TM->get_current_user();
        // akun
        $data['saldo'] = $this->TM->get_saldo_dk_by_user($id_user);
        $data['total_lot'] = $this->TM->get_total_lot_by_user($id_user);
		$data['active_lot'] = $this->TM->get_lot_active_user($id_user);
		$data['inactive_lot'] = $this->TM->get_lot_inactive_user($id_user);
		$data['queue_lot'] = $this->TM->get_antri_lot_user($id_user);
		$data['non_active_lot'] = $this->get_non_active_lot();
        // statistic
        $data['total_antrian'] = $this->TM->get_total_antrian();
        $data['total_member'] = $this->get_total_member();
        //$data['total_free_member'] = $this->get_free_member();
		$data['total_free_member'] = $this->ST->get_total_all_data('ki_m_user') - $data['total_member'];
        $data['total_full_profit'] = $this->get_full_profit();
        $data['total_penerima'] = $this->TM->get_total_penerima();
        // additional
        $data['penambahan_posisi'] = $this->TM->get_penambahan_posisi();
        $data['new_member'] = $this->TM->get_new_member();
        $data['top_sponsor'] = $this->TM->get_top_sponsor();
        
        $this->load->view('header', $data);
        if($data['user']->is_active == 1){
            $this->load->view('user/header', $data);
            $this->load->view('user/menu', $data);
            $this->load->view('user/dashboard', $data); 
        } else {
            $this->load->view('user/dashboard_activate', $data); 
        }
        $this->load->view('footer');
    }
    
	public function get_full_profit() {
		$master_point_saham = (int) $this->ST->get_setting_by_alias('point_saham');
        $master_end_percent_point = (float) $this->ST->get_setting_by_alias('end_percent_point_saham');
		$max_point = $master_point_saham*$master_end_percent_point;
		$d = $this->ST->get_all_data('ki_m_user');
		$nilai = 0;
		foreach ($d as $key => $value) {
            $lot = $value->active_lot;
			$id_user = $value->id_user;
			$saldoDK = $this->TM->get_saldo_d_bagi_hasil_by_user($id_user);
			if ($lot>0) {
				$nilai = floor($saldoDK/$max_point) + $nilai;
			}
        }
        return $nilai;
	}
	
	public function get_non_active_lot() {
		$master_point_saham = (int) $this->ST->get_setting_by_alias('point_saham');
        $master_end_percent_point = (float) $this->ST->get_setting_by_alias('end_percent_point_saham');
		$max_point = $master_point_saham*$master_end_percent_point;
		
		$sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
		$user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
		$lot = $user_data->active_lot;
		$saldoDK = $this->TM->get_saldo_dk_by_user($id_user);
		$nilai = 0;
		if ($lot>0) {
			$nilai = $saldoDK/($lot*$max_point);
		}
		
		return floor($nilai);
	}
	
    public function register_saham()
    {   
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Beli Saham |';
        $data['harga_saham'] = $this->ST->get_setting_by_alias('harga_saham');
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        
		$sess = $this->session->userdata('datauser');
        $point = $this->ST->get_setting_by_alias('point_saham');
        $jmlsaham = $this->input->post('jmlsaham', true);
        
        $data['saldo'] =  $jmlsaham * $point;
        $condition = [ 'id_user' =>  $sess['id_user'], ];
        
        $this->ST->update_array('ki_m_user',$data,$condition);  $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/register_saham', $data);
        $this->load->view('footer');
    }
    
    public function do_saham()
    {
        
        
    }
    
    public function aktivasi_saham()
    {   
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Aktivasi Saham |';
        $data['point_saham'] = $this->ST->get_setting_by_alias('point_saham');
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        
        // akun
        $data['saldo'] = $this->TM->get_saldo_dk_by_user($id_user);
        $data['total_lot'] = $this->TM->get_total_lot_by_user($id_user);
		$data['active_lot'] = $this->TM->get_lot_active_user($id_user);
		$data['inactive_lot'] = $this->TM->get_lot_inactive_user($id_user);
		$data['queue_lot'] = $this->TM->get_antri_lot_user($id_user);
		$data['non_active_lot'] = $this->get_non_active_lot();
        
        $max_activate_saham = (int) $this->ST->get_setting_by_alias('max_activate_saham');
        $max_activate_saham_user = $max_activate_saham - $data['user']->active_lot;
        if($data['user']->inactive_lot > $max_activate_saham_user){
            $max_activate = $max_activate_saham_user;
        } else {
            $max_activate = $data['user']->inactive_lot;
        }
        $data['max_activate_saham_user'] = $max_activate;
        $data['max_activate_saham'] = $max_activate_saham;
        
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/aktivasi_saham', $data);
        $this->load->view('footer');
    }
    
    public function do_aktivasi_saham()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        
        $jmlsaham = $this->input->post('jmlsaham', true);
        $now = date('Y-m-d H:i:s');
        
        // data from setting
        $point_bagi_hasil = (float) $this->ST->get_setting_by_alias('point_bagi_hasil');
        $point_bonus_generasi = (float) $this->ST->get_setting_by_alias('point_bonus_generasi');
        $point_bonus_sponsor = (float) $this->ST->get_setting_by_alias('point_bonus_sponsor');
        $point_amal = (float) $this->ST->get_setting_by_alias('point_amal');
        $point_administrasi = (float) $this->ST->get_setting_by_alias('point_administrasi');
        $point_dana_bersama = (float) $this->ST->get_setting_by_alias('point_dana_bersama');
        $master_point_saham = (int) $this->ST->get_setting_by_alias('point_saham');
        $master_end_percent_point = (float) $this->ST->get_setting_by_alias('end_percent_point_saham');
        $master_activate = $this->ST->get_all_data('ki_m_activate_saham');
        $master_generation = $this->ST->get_all_data('ki_m_generation');
        
        // dana bersama, administrasi, amal
        $group_kas = $this->ST->get_by_id_multiple_rows('ki_m_transaction_code',1,'id_transaction_group');
        
        if($user_data->inactive_lot > 0 && $user_data->inactive_lot - $jmlsaham >= 0 && $jmlsaham > 0){
            foreach($group_kas as $kas) {
                if($kas->id_transaction_code >= 1 && $kas->id_transaction_code <= 3){
                    $data['id_transaction_group'] = $kas->id_transaction_group;
                    $data['id_transaction_code'] = $kas->id_transaction_code;
                    $data['id_user'] = $user_data->id_user;
                    $data['description'] = $user_data->usertologin.' Aktivasi '.$jmlsaham.' LOT - '.$kas->description;
                    if($kas->id_transaction_code == 1){
                        $data['value'] = $point_dana_bersama * $jmlsaham;
                    } else if($kas->id_transaction_code == 2){
                        $data['value'] = $point_administrasi * $jmlsaham;
                    } else if($kas->id_transaction_code == 3) {
                        $data['value'] = $point_amal * $jmlsaham;
                    }
                    $data['isdk'] = 1;
                    $data['date_transaction'] = date('Y-m-d H:i:s');
                    //insert to transaction
                    $this->ST->insert('ki_t_transaction',$data);
                }
            }
            
            // bonus generasi dan sponsor
            if($user_data->id_sponsor != 0){
                // bonus sponsor
                $sponsor_data = $this->ST->get_by_id_single_row('ki_m_user',$user_data->id_sponsor,'id_user');
                $value_bonus_sponsor = $point_bonus_sponsor * $jmlsaham;
                $code_sponsor = $this->ST->get_by_id_single_row('ki_m_transaction_code',11,'id_transaction_code');
                //insert data
                $data2['id_transaction_group'] = $code_sponsor->id_transaction_group;
                $data2['id_transaction_code'] = $code_sponsor->id_transaction_code;
                $data2['id_user'] = $sponsor_data->id_user;
                $data2['description'] = $code_sponsor->description.' dari '.$user_data->usertologin;
                $data2['value'] = $value_bonus_sponsor;
                $data2['isdk'] = 1;
                $data2['date_transaction'] = $now;
                $this->ST->insert('ki_t_transaction',$data2);
             
                // bonus generasi 
                $generation = array();
                $id_sponsor = $user_data->id_sponsor;
                for($i = 0; $i<15;$i++){
                    $check_sponsor = $this->ST->get_by_id_single_row('ki_m_user',$id_sponsor,'id_sponsor');
                    if($check_sponsor->id_sponsor != 0){
                        $get_data_sponsor = $this->ST->get_by_id_single_row('ki_m_user',$id_sponsor,'id_user');
                        $generation[$i]['generation_lvl'] = $i+1;
                        $generation[$i]['id_sponsor'] =  $get_data_sponsor->id_user;
                        $id_sponsor = $get_data_sponsor->id_sponsor;
                    }else{
                        break;
                    }
                }
                $accum_point_generasi = 0;
                foreach($generation as $gen){
                    $gen_sponsor_data = $this->ST->get_by_id_single_row('ki_m_user',$gen['id_sponsor'],'id_user');
                    $user_sponsor_saldo_d = $this->TM->get_total_lot_by_user($gen['id_sponsor']);
                    foreach($master_generation as $mg){
                        if($mg->generation_lvl == $gen['generation_lvl']){
                            $value_point_generasi = $mg->value * $jmlsaham;
                        }
                    }
                    // check apakah menjadi aktif member
                    if($user_sponsor_saldo_d > 0){
                        $code_generasi = $this->ST->get_by_id_single_row('ki_m_transaction_code',10,'id_transaction_code');
                        $data3['id_transaction_group'] = $code_generasi->id_transaction_group;
                        $data3['id_transaction_code'] = $code_generasi->id_transaction_code;
                        $data3['id_user'] = $gen['id_sponsor'];
                        $data3['description'] = $code_generasi->description.' Ke-'.$gen['generation_lvl'].' dari '.$user_data->usertologin;
                        $data3['value'] = $value_point_generasi;
                        $data3['isdk'] = 1;
                        $data3['date_transaction'] = $now;
                        $this->ST->insert('ki_t_transaction',$data3);
                    } else {
                    // jika free member masuk ke administrasi
                        $code_administrasi = $this->ST->get_by_id_single_row('ki_m_transaction_code',2,'id_transaction_code');
                        $data4['id_transaction_group'] = $code_administrasi->id_transaction_group;
                        $data4['id_transaction_code'] = $code_administrasi->id_transaction_code;
                        $data4['id_user'] = $user_data->id_user;
                        $data4['description'] = 'Bonus generasi dari '.$user_data->usertologin;
                        $data4['value'] = $value_point_generasi;
                        $data4['isdk'] = 1;
                        $data4['date_transaction'] = $now;
                        $this->ST->insert('ki_t_transaction',$data4); 
                    }
                    $accum_point_generasi = $accum_point_generasi + $value_point_generasi;
                }
                
                $gap_accum_budget_generasi = ($point_bonus_generasi*$jmlsaham) - $accum_point_generasi;
                // jika ada sisa bonus generasi, dimasukkan ke administrasi
                if($gap_accum_budget_generasi > 0){
                    $code_administrasi = $this->ST->get_by_id_single_row('ki_m_transaction_code',2,'id_transaction_code');
                    $data5['id_transaction_group'] = $code_administrasi->id_transaction_group;
                    $data5['id_transaction_code'] = $code_administrasi->id_transaction_code;
                    $data5['id_user'] = $user_data->id_user;
                    $data5['description'] = 'Bonus generasi dari '.$user_data->usertologin;
                    $data5['value'] = $gap_accum_budget_generasi;
                    $data5['isdk'] = 1;
                    $data5['date_transaction'] = $now;
                    $this->ST->insert('ki_t_transaction',$data5);
                }
            } else {
                //tidak ada sponsor bonus generasi dan bonus sponsor masuk administrasi
                $code_administrasi = $this->ST->get_by_id_single_row('ki_m_transaction_code',2,'id_transaction_code');
                $data6['id_transaction_group'] = $code_administrasi->id_transaction_group;
                $data6['id_transaction_code'] = $code_administrasi->id_transaction_code;
                $data6['id_user'] = $user_data->id_user;
                $data6['description'] = 'Bonus generasi & sponsor dari '.$user_data->usertologin;
                $data6['value'] = ($point_bonus_sponsor * $jmlsaham)+($point_bonus_generasi * $jmlsaham);
                $data6['isdk'] = 1;
                $data6['date_transaction'] = $now;
                $this->ST->insert('ki_t_transaction',$data6);
            }
            
            $data7['inactive_lot'] = $user_data->inactive_lot - $jmlsaham;
            $condition7 = [ 'id_user' =>  $id_user, ];
            // update lot data in ki_m_user
            $this->ST->update_array('ki_m_user',$data7,$condition7);
            
            $data8['id_user'] = $id_user;
            $data8['total_lot'] = $jmlsaham;
            $data8['quantity'] = $jmlsaham;
            $data8['date_actived'] = date('Y-m-d H:i:s');
            // insert ki_t_activate_saham
            $this->ST->insert('ki_t_activate_saham',$data8);
        }
    }
    
    public function amal_page()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Amal Anda |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['amal_data'] = $this->TM->get_transaction_amal();
        
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/amal', $data);
        $this->load->view('footer');
    }
    
    public function user_transfer_lot() {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];  
      
      // akun
        $data['saldo'] = $this->TM->get_saldo_dk_by_user($id_user);
        $data['total_lot'] = $this->TM->get_total_lot_by_user($id_user);
        $data['active_lot'] = $this->TM->get_lot_active_user($id_user);
        $data['inactive_lot'] = $this->TM->get_lot_inactive_user($id_user);
        $data['queue_lot'] = $this->TM->get_antri_lot_user($id_user);
		$data['non_active_lot'] = $this->get_non_active_lot();
      
        $data['segment'] = 'Transfer LOT |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['inactive_lot'] = $this->TM->get_lot_inactive_user($id_user);
        
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);        
        $this->load->view('user/user_transfer_lot_user', $data);
        $this->load->view('footer');
    }
    
    public function check_username()
    {
	  $sess = $this->session->userdata('datauser');
      $id_user = $sess['id_user'];
	  $currentUser = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
      $username = $this->input->post('username', TRUE);
	  if($username==$currentUser->usertologin) {
		$isExist = false;
	  } else {
		$isExist = $this->UM->is_username_exist($username, false);
	  }
      
      $user_data = $this->ST->get_by_id_single_row('ki_m_user', $username, 'usertologin');

      echo json_encode(array(
          'valid' => $isExist,
          'username' => $username,
          'nama' => $user_data->name,
          'kota' => $user_data->city,
          'hp' => $user_data->handphone,
      ));
    }
    
	public function check_inactivelot() {
		$sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
		$inactive_lot = $this->input->post('pointlot', TRUE);
		$value = $this->TM->get_lot_inactive_user($id_user);
		if ($inactive_lot<=$value) {
			$isValid = true;
		} else {
			$isValid = false;
		}
		echo json_encode(array(
          'valid' => $isValid,
		  'total' => $value
      ));
	}
     
	public function check_handphone()
	{
		  $sess = $this->session->userdata('datauser');
		  $id_user = $sess['id_user'];
		  $currentUser = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
		  $handphone = $this->input->post('handphone', TRUE);
		  
		  if($handphone==$currentUser->handphone) {
			$isExist = true;
		  } else {
			$isExist = $this->UM->is_handphone_available($handphone);
		  }
		  
		  echo json_encode(array(
			  'valid' => $isExist,
		  ));
	}
	
	public function check_email()
	{
		  $sess = $this->session->userdata('datauser');
		  $id_user = $sess['id_user'];
		  $currentUser = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
		  $email = $this->input->post('email', TRUE);
		  
		  if($email==$currentUser->email) {
			$isExist = true;
		  } else {
			$isExist = $this->UM->is_email_available($email);
		  }
		  
		  echo json_encode(array(
			  'valid' => $isExist,
		  ));
	}
	
	public function check_rekening()
	{
		  $sess = $this->session->userdata('datauser');
		  $id_user = $sess['id_user'];
		  $currentUser = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
		  $bank_account_number = $this->input->post('norekening', TRUE);
		  
		  if($bank_account_number==$currentUser->bank_account_number) {
			$isExist = true;
		  } else {
			$isExist = $this->UM->is_rekening_available($bank_account_number);
		  }
		  
		  echo json_encode(array(
			  'valid' => $isExist,
		  ));
	}
	
    public function do_trans_saham()
    {
        $sess = $this->session->userdata('datauser');
        $id_active_user = $sess['id_user'];
        
        $id_user = $this->input->post('username', TRUE);
        
        $active_user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_active_user,'id_user');
        $user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'usertologin');
        $point_amal = (float) $this->ST->get_setting_by_alias('point_amal');
        $point_administrasi = (float) $this->ST->get_setting_by_alias('point_administrasi');
        $point_dana_bersama = (float) $this->ST->get_setting_by_alias('point_dana_bersama');
        
        $jmlsaham = $this->input->post('pointlot', true);
        
        if($active_user_data->inactive_lot > 0 && $active_user_data->inactive_lot - $jmlsaham >= 0 && $jmlsaham > 0 && $id_user != ''){
            $data['inactive_lot'] = $user_data->inactive_lot + $jmlsaham;
            $condition = [ 'usertologin' =>  $id_user, ];
            // update lot data in ki_m_user
            $this->ST->update_array('ki_m_user',$data,$condition);
    		
    		$value['inactive_lot'] = $active_user_data->inactive_lot - $jmlsaham;
    		$condition = [ 'id_user' =>  $id_active_user, ];
            $this->ST->update_array('ki_m_user',$value,$condition);
              
            $point_kas = $this->ST->get_by_id_single_row('ki_m_transaction_code', 7, 'id_transaction_code');
            $data2['id_transaction_code'] = $point_kas->id_transaction_code;
            $data2['id_transaction_group'] = $point_kas->id_transaction_group;
            $data2['id_user'] = $user_data->id_user;
            $data2['description'] = "Anda menerima transfer LOT dari ".$active_user_data->usertologin." (".$active_user_data->name.")";//.$user_data->usertologin;
            $data2['value'] = $jmlsaham;
            $data2['isdk'] = 1;
            $data2['date_transaction'] = date('Y-m-d H:i:s');
            //insert to transaction
            $this->ST->insert('ki_t_transaction',$data2);   
    		
    		$point_kas = $this->ST->get_by_id_single_row('ki_m_transaction_code', 7, 'id_transaction_code');
            $data3['id_transaction_code'] = $point_kas->id_transaction_code;
            $data3['id_transaction_group'] = $point_kas->id_transaction_group;
            $data3['id_user'] = $active_user_data->id_user;
            $data3['description'] = "Anda melakukan transfer LOT ke ".$user_data->usertologin." (".$user_data->name.")";//$active_user_data->usertologin."Anda melakukan transfer LOT ke ".$user_data->usertologin;
            $data3['value'] = $jmlsaham;
            $data3['isdk'] = -1;
            $data3['date_transaction'] = date('Y-m-d H:i:s');
            //insert to transaction
            $this->ST->insert('ki_t_transaction',$data3);  
        }
    }

    public function pencairan_point()
    {   
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];

        $data['segment'] = 'Pencairan Point |';
        $data['point_saham'] = $this->ST->get_setting_by_alias('point_saham');
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $user_point = (int)$this->TM->get_saldo_dk_by_user($id_user);
        $proses_point = (int)$this->RM->get_process_point_by_user($id_user,1);
		$minimal_point = (int)$this->ST->get_setting_by_alias('pencairan_minimum');
        
        // akun
        $data['saldo'] = $this->TM->get_saldo_dk_by_user($id_user);
        $data['total_lot'] = $this->TM->get_total_lot_by_user($id_user);
		$data['active_lot'] = $this->TM->get_lot_active_user($id_user);
		$data['inactive_lot'] = $this->TM->get_lot_inactive_user($id_user);
		$data['queue_lot'] = $this->TM->get_antri_lot_user($id_user);
		$data['non_active_lot'] = $this->get_non_active_lot();
		
		$data['point_data'] = $this->RM->get_user_data_point($id_user);
		$data['harga'] = (int)$this->ST->get_setting_by_alias('point_to_rp');
        $data['user_point'] = $user_point;
		$data['minimal_point'] = $minimal_point;
        $data['proses_point'] = $proses_point;
		
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/pencairan_point', $data);
        $this->load->view('footer');
    }

    public function do_pencairan_point(){
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $point = $this->input->post('jmlpoint', TRUE);
        $user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $user_point = (int)$this->TM->get_saldo_dk_by_user($id_user);
        
        if($user_point - $point >= 0 && $point > 0){
            $data['request_type'] = 1;
            $data['id_user'] = $id_user;
            $data['description'] = $user_data->usertologin.' mencairkan point sebanyak '.$point.' point' ;
            $data['value'] = $point;
            $data['is_done'] = 0;
            $data['date_transaction'] = date('Y-m-d H:i:s');
            $this->ST->insert('ki_t_request',$data); 
            
        }
    }
	
	public function user_profile() {
      $sess = $this->session->userdata('datauser');
      $id_user = $sess['id_user'];  
      $data['segment'] = 'User Profile |';
      $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
      $data['json'] = json_encode(array(
          //'valid' => $isExist,
          'username' => $data['user']->usertologin,
          'name' => $data['user']->name,
          'hp' => $data['user']->handphone,
          'email' => $data['user']->email,
          'address' => $data['user']->address,
          'city' => $data['user']->city,
          'rekening' => $data['user']->bank_account_number,
          'bank' => $data['user']->bank_name,
      ));
      
      $this->load->view('header', $data);
      $this->load->view('user/header', $data);
      $this->load->view('user/menu', $data);        
      $this->load->view('user/update_user_profile', $data);
      $this->load->view('footer');
    }

    public function update_user_profile() {
      $sess = $this->session->userdata('datauser');
      $id_user = $sess['id_user'];
      
      $user_data = $this->ST->get_by_id_single_row('ki_m_user', $id_user, 'id_user');
      $isExist = $this->UM->is_username_exist($user_data->usertologin, false);

      echo json_encode(array(
          'valid' => $isExist,
          'usernaname' => $user_data->usertologin,
      ));
    }
    
    public function saldo_pendapatan()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Saldo Pendapatan |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['saldo_pendapatan'] = $this->TM->get_dk_transaction_by_user($id_user);
        
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/saldo_pendapatan', $data);
        $this->load->view('footer');
    }
    
    public function saldo_keluar()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Saldo Keluar |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['saldo'] = $this->TM->get_kredit_transaction_by_user($id_user);
        
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/saldo_keluar', $data);
        $this->load->view('footer');
    }
    
    public function saldo_masuk()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Saldo Masuk |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['saldo'] = $this->TM->get_debet_transaction_by_user($id_user);
        
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/saldo_masuk', $data);
        $this->load->view('footer');
    }
    
    public function stok_pendapatan()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Stok Pendapatan |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['saldo'] = $this->TM->get_transaction_stock_pendapatan();
        
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/stok_pendapatan', $data);
        $this->load->view('footer');
    }
    
    public function dana_bersama()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Dana Bersama |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['saldo'] = $this->TM->get_transaction_dana_bersama();
		$this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/dana_bersama', $data);
		$this->load->view('footer');
	}
	
	public function transfer_saldo()
    {   
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Transfer Saldo |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['minimal'] = (int)$this->ST->get_setting_by_alias('tf_saldo_minimum');
        
        // akun
        $data['saldo'] = $this->TM->get_saldo_dk_by_user($id_user);
        $data['total_lot'] = $this->TM->get_total_lot_by_user($id_user);
		$data['active_lot'] = $this->TM->get_lot_active_user($id_user);
		$data['inactive_lot'] = $this->TM->get_lot_inactive_user($id_user);
		$data['queue_lot'] = $this->TM->get_antri_lot_user($id_user);
		$data['non_active_lot'] = $this->get_non_active_lot();
        
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/user_transfer_saldo', $data);
        $this->load->view('footer');
    }
	
	public function do_transfer_saldo(){
		$sess = $this->session->userdata('datauser');
        $id_active_user = $sess['id_user'];
        
        $id_user = $this->input->post('username', TRUE);
        
        $active_user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_active_user,'id_user');
        $user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'usertologin');
		$user_point = (int)$this->TM->get_saldo_dk_by_user($id_active_user);
		$jmlsaldo = $this->input->post('jmlsaldo', TRUE);
         
        if($user_point > 0 && $jmlsaldo > 0 && $user_point - $jmlsaldo >= 0){  
            $point_kas = $this->ST->get_by_id_single_row('ki_m_transaction_code', 14, 'id_transaction_code');
            $data2['id_transaction_code'] = $point_kas->id_transaction_code;
            $data2['id_transaction_group'] = $point_kas->id_transaction_group;
            $data2['id_user'] = $user_data->id_user;
            $data2['description'] = "Transfer saldo dari ".$active_user_data->usertologin;
            $data2['value'] = $jmlsaldo;
            $data2['isdk'] = 1;
            $data2['date_transaction'] = date('Y-m-d H:i:s');
            //insert to transaction
            $this->ST->insert('ki_t_transaction',$data2);   
    		
    		$point_kas = $this->ST->get_by_id_single_row('ki_m_transaction_code', 14, 'id_transaction_code');
            $data3['id_transaction_code'] = $point_kas->id_transaction_code;
            $data3['id_transaction_group'] = $point_kas->id_transaction_group;
            $data3['id_user'] = $active_user_data->id_user;
            $data3['description'] = "Transfer saldo ke ".$user_data->usertologin;
            $data3['value'] = $jmlsaldo;
            $data3['isdk'] = -1;
            $data3['date_transaction'] = date('Y-m-d H:i:s');
            //insert to transaction
            $this->ST->insert('ki_t_transaction',$data3);
    		
    		$point_kas = $this->ST->get_by_id_single_row('ki_m_transaction_code', 14, 'id_transaction_code');
            $data5['id_transaction_code'] = $point_kas->id_transaction_code;
            $data5['id_transaction_group'] = $point_kas->id_transaction_group;
            $data5['id_user'] = $active_user_data->id_user;
            $data5['description'] = "Biaya transfer saldo ke ".$user_data->usertologin;
            $data5['value'] = (($jmlsaldo*2)/100);
            $data5['isdk'] = -1;
            $data5['date_transaction'] = date('Y-m-d H:i:s');
            //insert to transaction
            $this->ST->insert('ki_t_transaction',$data5); 
    		
    		$point_kas = $this->ST->get_by_id_single_row('ki_m_transaction_code', 13, 'id_transaction_code');
            $data4['id_transaction_code'] = $point_kas->id_transaction_code;
            $data4['id_transaction_group'] = $point_kas->id_transaction_group;
            $data4['id_user'] = $active_user_data->id_user;
            $data4['description'] = "Biaya transfer saldo dari ".$active_user_data->usertologin;
            $data4['value'] = (($jmlsaldo*2)/100);
            $data4['isdk'] = 1;
            $data4['date_transaction'] = date('Y-m-d H:i:s');
            //insert to transaction
            $this->ST->insert('ki_t_transaction',$data4);
        }
	}
	
	public function get_total_dibagikan($user_id) {
		
        $master_point_bagi_hasil = (float) $this->ST->get_setting_by_alias('point_bagi_hasil');
        $pendapatan = $this->get_total_pendapatan_harian($user_id);
		$penerima = $this->TM->get_total_penerima();
        
        $start_data = $this->UM->get_start_activate_saham(date('Y-m-d'));
        if(!empty($start_data)){
            $add_value = $start_data * $master_point_bagi_hasil;
        } else {
            $add_value = 0;
        }
        
        if($penerima == 0) {
          return $add_value;  
        } else {
		  return $pendapatan/$penerima;
        }
	}
	
	public function get_total_pendapatan_harian($user_id) {
		$sess = $this->session->userdata('datauser');
        $id_active_user = $sess['id_user'];
		$user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_active_user,'id_user');
		
		$master_point_saham = (int) $this->ST->get_setting_by_alias('point_saham');
        $master_end_percent_point = (float) $this->ST->get_setting_by_alias('end_percent_point_saham');
        $master_point_bagi_hasil = (float) $this->ST->get_setting_by_alias('point_bagi_hasil');
        $master_point_kh = (float) $this->ST->get_setting_by_alias('point_keuntungan_harian');
        $master_max_daily_point = (float) $this->ST->get_setting_by_alias('max_daily_point');
        $master_min_sp_point = (float) $this->ST->get_setting_by_alias('min_to_use_stok_pendapatan');
		$posting_data = $this->UM->get_posting_saham();
        $total_lot_active = $this->UM->total_active_lot();
        $get_user_has_active_lot = $this->UM->get_user_has_active_lot();
		
		$active_lot_today = 0;
		$kh=0;
        foreach ($posting_data as $posting) {
            $active_lot_today = $active_lot_today + $posting->value;
        }
        $pk = $master_point_bagi_hasil * $active_lot_today;
		
		if ($active_lot_today > 0){
			$pps = $pk/$total_lot_active;
			if($pps >= $master_max_daily_point){
				$kh = $master_max_daily_point * $user_data->active_lot;
			} else {
				if($pps <= $master_min_sp_point){
					$kh = ($master_point_kh + $pps) * $user_data->active_lot;
				} else {
					$kh = $pps * $user_data->active_lot;
				}
			}
		} else {
			$kh=0;
		}
		
		$penerima = $this->TM->get_total_penerima();
		$start_data = $this->UM->get_start_activate_saham(date('Y-m-d'));
        if(!empty($start_data)){
            $add_value = $start_data * $master_point_bagi_hasil;
        } else {
            $add_value = 0;
        }
        
		//if ($penerima==0) {
			return $add_value;
		//} else {
			//return $kh+$add_value/$penerima;
		//}
	}
	
	public function get_free_member() {
		$master_point_saham = (int) $this->ST->get_setting_by_alias('point_saham');
        $master_end_percent_point = (float) $this->ST->get_setting_by_alias('end_percent_point_saham');
		$max_point = $master_point_saham*$master_end_percent_point;
		
		$d = $this->ST->get_all_data('ki_m_user');
		$total = 0;
		foreach ($d as $key => $value) {
			if ($value->active_lot > 0 && ($value->role==0 || $value->role==1)) {
				$saldo = (int)$this->TM->get_saldo_d_bagi_hasil_by_user($value->id_user);
				if ($saldo==0 || $saldo > $max_point*$value->active_lot) {
					$total = $total+1; 
				}
			} else if($value->inactive_lot = 0) {
				$total = $total+1; 
			}
		}
		
		return $total;	
	}
	
	public function get_status_member_user($id_user){
		$master_point_saham = (int) $this->ST->get_setting_by_alias('point_saham');
        $master_end_percent_point = (float) $this->ST->get_setting_by_alias('end_percent_point_saham');
		$max_point = $master_point_saham*$master_end_percent_point;
		
		$user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
		$free = false;
		if ($user_data->role==0 || $user_data->role==1) {
				$saldo = (int)$this->TM->get_saldo_d_bagi_hasil_by_user($user_data->id_user);
				if ($saldo==0 || $saldo > $max_point) {
					$free= true; 
				} else {
					$free= false; 
				}
		}
		
		return $free;
	}
	
	public function get_total_member() {
		$master_point_saham = (int) $this->ST->get_setting_by_alias('point_saham');
        $master_end_percent_point = (float) $this->ST->get_setting_by_alias('end_percent_point_saham');
		$master_max_point = $master_point_saham*$master_end_percent_point;
		
		$d = $this->ST->get_all_data('ki_m_user');
		
		$total = 0;
		foreach ($d as $value) {
			$lot = $this->UM->is_lot_active_queue($value->id_user);
            $saldo = (int)$this->TM->get_saldo_d_bagi_hasil_by_user($value->id_user);
			/*if ($value->role==0 || $value->role==1) {
				$saldo = (int)$this->TM->get_saldo_d_bagi_hasil_by_user($value->id_user);
				if (!$saldo==0 || $saldo > $max_point) {
					$total = $total+1; 
				}
			}*/
			if($lot){
				$total = $total+1; 
			} 
			
		}
		
		return $total;	
	}
	
	public function mustasi_lot_page() {
		$sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];  
      
		// akun
        $data['saldo'] = $this->TM->get_saldo_dk_by_user($id_user);
        $data['total_lot'] = $this->TM->get_total_lot_by_user($id_user);
        $data['active_lot'] = $this->TM->get_lot_active_user($id_user);
        $data['inactive_lot'] = $this->TM->get_lot_inactive_user($id_user);
        $data['queue_lot'] = $this->TM->get_antri_lot_user($id_user);
		$data['non_active_lot'] = $this->get_non_active_lot();
      
        $data['segment'] = 'Mutasi LOT |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['history_lot'] = $this->TM->get_mutasi_lot($id_user);
        
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);        
        $this->load->view('user/mutasi_lot', $data);
        $this->load->view('footer');
	}
	
	private function recursive_sponsor($id_user, &$i, $arr) {
				
		$c = $this->ST->get_by_id_multiple_rows('ki_m_user',$id_user,'id_sponsor');
		
		if(count($c)>0) {
			$bol = true;
		} else {
			$bol = false;
		} 
		
		foreach($c as $k=>$value) {
				$arr['child'][$k]['parent'] = $value->id_sponsor;
				$arr['child'][$k]['name'] = $value->name;
				$arr['child'][$k]['id'] = $value->id_user;
				//$arr['child'][$k]['level'] = $i;
				
				$marr=array();
				$x=$this->recursive_sponsor($value->id_user, $i, $marr);
				
				$arr['child'][$k]['child'] = $this->recursive_sponsor($value->id_user, $marr);
		}
		return $arr;
	}
	
	public function sponsor_recursive($id_user, &$i, &$arr){
		//$c = $this->ST->get_by_id_multiple_rows('ki_m_user',$id_user,'id_sponsor');
        $c = $this->UM->get_active_user_by_sponsor($id_user);
			
		foreach($c as $k=>$value) {
				$arr[$i]['level'] = $this->display_parent($value->id_user,1);
				$arr[$i]['parent'] = $value->id_sponsor;
				$arr[$i]['name'] = $value->name;
				$arr[$i]['id'] = $value->id_user;
                $arr[$i]['usertologin'] = $value->usertologin;
				$arr[$i]['kota'] = $value->city;
				$arr[$i]['tlp'] = $value->handphone;
				$arr[$i]['lot'] = $this->TM->get_total_lot_by_user($value->id_user);
				//$arr[$i]['sponsor'] = $this->display_children($value->id_user,1);
                $arr[$i]['sponsor'] = $this->get_total_downline($value->id_user,1);
				$status = $this->get_status_member_user($value->id_user);
				if ($status) {
					$arr[$i]['status'] = 'Free';
				} else {
					$arr[$i]['status'] = 'Active';
				}
				$i=$i+1;
				$x= $this->sponsor_recursive($value->id_user, $i, $arr);
		}
		return $arr;
	}
    
    function get_total_downline($id_user){
        $count=0;
        $result = $this->ST->get_by_id_multiple_rows('ki_m_user',$id_user,'id_sponsor');
        if(!empty($result)){
            foreach($result as $row){
                $count = $count+1;
            }
        }
        return $count;
    }
	
	function display_children($parent, $level) { 
		$count = 0; 
		$result = $this->TM->get_child($parent); 
		foreach ( $result as $row ) { 
			$var = str_repeat(' ',$level).$row['id_user']."\n"; 
			$count += 1 +$this->display_children($row['id_user'], $level+1); 
		} 
		return $count;
	} 
	
	
	function display_parent($parent, $level) { 
		$sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
		$count = 0; 
		$result = $this->TM->get_parent($parent); 
		foreach ( $result as $row ) { 
			if($parent<$id_user) {
				break;
			}
			$var = str_repeat(' ',$level).$row['id_sponsor']."\n"; 
			$count += 1 +$this->display_parent($row['id_sponsor'], $level+1); 
		} 
		return $count;
	}
	
	function aasort (&$array, $key) { 
		$sorter=array(); 
		$ret=array(); 
		reset($array); 
		foreach ($array as $ii => $va) { 
			$sorter[$ii]=$va[$key]; 
		} 
		asort($sorter); 
		foreach ($sorter as $ii => $va) { 
			$ret[$ii]=$array[$ii]; 
		} 
		$array=$ret; 
		
		return $array;
	} 
	
	public function sponsorisasi_page() {
		$sess = $this->session->userdata('datauser');
        $id_active_user = $sess['id_user'];

		$generation = array();
		$i=1;
		
		$data['segment'] = 'Sponsorisasi |';
		$data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_active_user,'id_user');
		
		$data['generasi'] = $this->sponsor_recursive($id_active_user,$i,$generation);
		$data_user = $this->ST->get_by_id_single_row('ki_m_user',$id_active_user,'id_sponsor');
		
		$m = $this->aasort($data['generasi'],"level");
		$data['generasi'] = $m;
		
		$this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/user_sponsorisasi', $data);
        $this->load->view('footer');
	}
    
    public function send_random_pin(){
        $phone = $this->input->post('handphone');
        $pin = $this->input->post('code');
        $transaction = $this->input->post('transaction');
        
        $message = 'PIN '.$transaction.'anda adalah '.$pin.'. Silahkan masukkan dikolom PIN SMS';
        raja_sms($phone,$message);
        
        //echo json_encode($phone);
        $message = TRUE;
        echo json_encode($message);
    }
    
    public function pencairan_fee()
    {   
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];

        $data['segment'] = 'Pencairan Fee |';
        $data['point_saham'] = $this->ST->get_setting_by_alias('point_saham');
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $user_point = (int)$this->TM->get_saldo_dk_by_user($id_user);
        $proses_point = (int)$this->RM->get_process_point_by_user($id_user);
		$minimal_point = (int)$this->ST->get_setting_by_alias('pencairan_minimum');
        
        // akun
        $data['saldo'] = $this->TM->get_saldo_dk_by_user($id_user);
        $data['total_lot'] = $this->TM->get_total_lot_by_user($id_user);
		$data['active_lot'] = $this->TM->get_lot_active_user($id_user);
		$data['inactive_lot'] = $this->TM->get_lot_inactive_user($id_user);
		$data['queue_lot'] = $this->TM->get_antri_lot_user($id_user);
		$data['non_active_lot'] = $this->get_non_active_lot();
		
		$data['point_data'] = $this->RM->get_user_data_point($id_user);
		$data['harga'] = (int)$this->ST->get_setting_by_alias('point_to_rp');
        $data['user_point'] = $user_point;
		$data['minimal_point'] = $minimal_point;
        $data['proses_point'] = $proses_point;
		
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/pencairan_point', $data);
        $this->load->view('footer');
    }
    
    public function ppob_pulsa()
    {   
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];

        $data['segment'] = 'PPOB - Pulsa |';
        $data['point_saham'] = $this->ST->get_setting_by_alias('point_saham');
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $user_point = (int)$this->TM->get_saldo_dk_by_user($id_user);
        $proses_point = (int)$this->RM->get_process_point_by_user($id_user,2);
		$data['master_ppob_pulsa'] = $this->ST->get_by_id_multiple_rows('ki_m_ppob',1,'type'); 
        $minimal_point = 0;
        
        // akun
        $data['saldo'] = $this->TM->get_saldo_dk_by_user($id_user);
        $data['total_lot'] = $this->TM->get_total_lot_by_user($id_user);
		$data['active_lot'] = $this->TM->get_lot_active_user($id_user);
		$data['inactive_lot'] = $this->TM->get_lot_inactive_user($id_user);
		$data['queue_lot'] = $this->TM->get_antri_lot_user($id_user);
		$data['non_active_lot'] = $this->get_non_active_lot();
		
		$data['point_data'] = $this->RM->get_user_data_ppob_pulsa($id_user);
		$data['point_to_rp'] = (int)$this->ST->get_setting_by_alias('point_to_rp');
        $data['user_point'] = $user_point;
		$data['minimal_point'] = $minimal_point;
        $data['proses_point'] = $proses_point;
		
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/ppob_pulsa', $data);
        $this->load->view('footer');
    }
    
    public function do_ppob_pulsa(){
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $handphone = $this->input->post('handphone', TRUE);
        $id_ppob = $this->input->post('id_ppob', TRUE);
        $point_to_rp = (int)$this->ST->get_setting_by_alias('point_to_rp');
        $data_pulsa = $this->ST->get_by_id_single_row('ki_m_ppob',$id_ppob,'id_ppob');
        
        if($data_pulsa->type == 1){
            $type_pulsa = 'Reguler';
        } else {
            $type_pulsa = 'Data';
        }
        
        $point = ($data_pulsa->price_center + $data_pulsa->price_markup)/$point_to_rp;
        
        $user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        
        $user_point = (int)$this->TM->get_saldo_dk_by_user($id_user);
        
        if($user_point - $point >= 0 && $point > 0){
            $data['request_type'] = 2;
            $data['id_user'] = $id_user;
            $data['description'] = 'Pulsa '.$type_pulsa.' '.$data_pulsa->name_provider.' Rp '.currencyNumber($data_pulsa->price).' Ke '.$handphone ;
            $data['value'] = $point;
            $data['id_support'] = $id_ppob;
            $data['is_done'] = 0;
            $data['date_transaction'] = date('Y-m-d H:i:s');
            $this->ST->insert('ki_t_request',$data); 
            
            $message = 'Request Pulsa dari '.$user_data->usertologin.' ('.$user_data->name.'). Format : '.$data_pulsa->code.'.'.$handphone.'.'.PPOB_PIN;
            raja_sms(HANDPHONE_KOPEI,$message);
            
        }
    }
    
    public function get_operator_by_handphone(){
        $handphone = $this->input->post('handphone');
        
        $prefix = substr($handphone,0,4);
        
        if($prefix == '0811' || $prefix == '0812' || $prefix == '0813' || $prefix == '0821' || $prefix == '0822' || $prefix == '0823' || $prefix == '0852' || $prefix == '0853' || $prefix == '0851'){
            $operator = 'Telkomsel';  
        } else if($prefix == '0855' || $prefix == '0856' || $prefix == '0857' || $prefix == '0858' || $prefix == '0814' || $prefix == '0815'|| $prefix == '0816') {
            $operator = 'Indosat'; 
        } else if($prefix == '0817' || $prefix == '0818' || $prefix == '0819' || $prefix == '0859' || $prefix == '0877' || $prefix == '0878') {
            $operator = 'XL'; 
        } else if($prefix == '0896' || $prefix == '0897' || $prefix == '0898' || $prefix == '0899'){
            $operator = 'Three';
        }
        
        $data = $this->UM->get_ppob_pulsa($operator);

        echo json_encode($data);
        return '';
    }
    
    public function ppob_listrik()
    {   
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];

        $data['segment'] = 'PPOB - Listrik |';
        $data['point_saham'] = $this->ST->get_setting_by_alias('point_saham');
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $user_point = (int)$this->TM->get_saldo_dk_by_user($id_user);
        $proses_point = (int)$this->RM->get_process_point_by_user($id_user,2);
		$data['master_ppob_listrik'] = $this->UM->get_ppob_listrik(); 
        $minimal_point = 0;
        
        //akun
        $data['saldo'] = $this->TM->get_saldo_dk_by_user($id_user);
        $data['total_lot'] = $this->TM->get_total_lot_by_user($id_user);
		$data['active_lot'] = $this->TM->get_lot_active_user($id_user);
		$data['inactive_lot'] = $this->TM->get_lot_inactive_user($id_user);
		$data['queue_lot'] = $this->TM->get_antri_lot_user($id_user);
		$data['non_active_lot'] = $this->get_non_active_lot();
		
		$data['point_data'] = $this->RM->get_user_data_ppob_listrik($id_user);
		$data['point_to_rp'] = (int)$this->ST->get_setting_by_alias('point_to_rp');
        $data['user_point'] = $user_point;
		$data['minimal_point'] = $minimal_point;
        $data['proses_point'] = $proses_point;
		
        $this->load->view('header', $data);
        $this->load->view('user/header', $data);
        $this->load->view('user/menu', $data);
        $this->load->view('user/ppob_listrik', $data);
        $this->load->view('footer');
    }
    
    public function do_ppob_listik(){
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $handphone = $this->input->post('handphone', TRUE);
        $pln_number = $this->input->post('pln_number', TRUE);
        $id_ppob = $this->input->post('id_ppob', TRUE);
        $point_to_rp = (int)$this->ST->get_setting_by_alias('point_to_rp');
        $data_listrik = $this->ST->get_by_id_single_row('ki_m_ppob',$id_ppob,'id_ppob');
        
        $point = ($data_listrik->price_center + $data_listrik->price_markup)/$point_to_rp;
        
        $user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        
        $user_point = (int)$this->TM->get_saldo_dk_by_user($id_user);
        
        if($user_point - $point >= 0 && $point > 0){
            $data['request_type'] = 3;
            $data['id_user'] = $id_user;
            $data['description'] = 'Token Listrik Rp '.currencyNumber($data_listrik->price).' Ke '.$pln_number.'<br>('.$handphone.')';
            $data['value'] = $point;
            $data['id_support'] = $id_ppob;
            $data['is_done'] = 0;
            $data['date_transaction'] = date('Y-m-d H:i:s');
            $this->ST->insert('ki_t_request',$data); 
            
            $condition['id_user'] = $id_user;
            $value['pln_number'] = $pln_number;
            $this->ST->update_array('ki_m_user',$value,$condition); 
            
            $message = 'Request Token Listrik dari '.$user_data->usertologin.' ('.$user_data->name.'). Format : '.$data_listrik->code.'.'.$pln_number.'.'.$handphone.'.'.PPOB_PIN;
            raja_sms(HANDPHONE_KOPEI,$message);
            
        }
    }
    
}
