<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate extends CI_Controller
{

    public $message;

    function __construct()
    {
        parent::__construct();
        $this->load->model('setting_m', 'ST');
        $this->load->model('authenticate_m', 'AM');
        $this->load->model('user_m', 'UM');
        redirectToHTTPS();
    }

    public function index()
    {
        if ($this->session->has_userdata('datauser')) {
            $sess = $this->session->userdata('datauser');
            if ($sess['logged_in']) {
                $this->check_role();
            }
        }
        $data['segment'] = 'Login |';
        
        $this->load->view('header', $data);
        $this->load->view('general/login');
        
    }
    
    public function signup()
    {
        $sponsor = $this->input->post('user_sponsor', TRUE);
        // get role and id_sponsor
        $seller_code = $this->ST->get_setting_by_alias('code_register_seller');
        if($sponsor == $seller_code){
            $data['role'] = SELLER;
            $data['id_sponsor'] = 0;
        } else {
            $data['role'] = MEMBER;
            $data['id_sponsor'] = $this->ST->get_by_id_single_row('ki_m_user',$sponsor,'usertologin')->id_user;
        }
        $data['name'] = ucwords(strtolower($this->input->post('name', TRUE)));
        $data['address'] = $this->input->post('alamat', TRUE);
        $data['city'] = strtoupper($this->input->post('kota', TRUE));
        $data['handphone'] = $this->input->post('handphone', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['bank_account_number'] = $this->input->post('norekening', TRUE);
        $data['bank_name'] = strtoupper($this->input->post('nmbank', TRUE));
        $data['ppass'] = md5($this->input->post('password', TRUE));
        $data['ppin'] = md5($this->input->post('pin', TRUE));
        
        $data['date_created'] = date('Y-m-d H:i:s');
        $last_id = $this->ST->get_total_all_data('ki_m_user');
        $data['usertologin'] = 'KI'.str_pad($last_id+1,8,"0",STR_PAD_LEFT);
        $data['remember_token'] = bin2hex(openssl_random_pseudo_bytes(16));
        $link_expired = sha1(mt_rand(10000, 99999) . time() . $data['email']);
        $data['link_expired'] = $link_expired;
        $data['time_expired'] = date("Y-m-d H:i:s", strtotime('+2 days'));
        
        $id_user = $this->UM->signup($data);
        
        $subject = 'Aktivasi akun KOPEI-INT (kopei-int.com)';
        $email = $data['email'];
        $email_msg = 'Silahkan klik link berikut untuk mengaktivasi akun anda: <br><br>'
                        .base_url().'link/'.$link_expired.'<br><br><br><br>
                        � 2016 KOPEI-INT<br>
                        Jl. Hanjoyo Putro BT. 8 Atas<br>
                        Komplek Ruko Gesya Blok A-44<br>
                        Tanjung Pinang' ;
        send_email_mime($email,$subject,$email_msg);
        
        $password = $this->input->post('password', TRUE);
        $pin = $this->input->post('pin', TRUE);
        $sms_msg = 'Terima kasih telah bergabung dengan kopei-int.com. Username anda : '.$data['usertologin'].', Password : '.$password.', PIN : '.$pin;
        raja_sms($data['handphone'],$sms_msg);
        
        $datauser = array(
            'id_user' => $id_user,
            'name' => $data['name'],
            'usertologin' => $data['usertologin'],
            'email' => $data['email'],
            'logged_in' => TRUE,
            'role' => $data['role'],
            'handphone' => $data['handphone'],
            'remember_token' => $data['remember_token'],
        );
        
        $message['id_user'] = $id_user;
        
        $this->session->set_userdata('datauser', $datauser);

        echo json_encode($message);
    }
    
    public function signup_admin()
    {
        $data['role'] = ADMIN;
        $data['id_sponsor'] = 0;
        $data['name'] = ucwords(strtolower($this->input->post('name', TRUE)));
        $data['address'] = $this->input->post('alamat', TRUE);
        $data['city'] = strtoupper($this->input->post('kota', TRUE));
        $data['handphone'] = $this->input->post('handphone', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['bank_account_number'] = $this->input->post('norekening', TRUE);
        $data['bank_name'] = strtoupper($this->input->post('nmbank', TRUE));
        $data['ppass'] = md5($this->input->post('password', TRUE));
        $data['ppin'] = md5($this->input->post('pin', TRUE));
        
        $data['date_created'] = date('Y-m-d H:i:s');
        $last_id = $this->ST->get_total_all_data('ki_m_user');
        $data['usertologin'] = strtoupper($this->input->post('username', TRUE));//'ADM'.str_pad($last_id+1,8,"0",STR_PAD_LEFT);
        
        $id_user = $this->UM->signup($data);                
        $message['id_user'] = $id_user;                                                                                          
        echo json_encode($message);
    }

    public function update_profile_admin() {
		$id_user = $this->input->post('user_id', TRUE);;
		if ($id_user=="null") {
			$sess = $this->session->userdata('datauser');
			$id_user = $sess['id_user'];
		}
		
		$user_data =  $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $condition = [ 'id_user' =>  $id_user, ];
        $data['name'] = ucwords(strtolower($this->input->post('name', TRUE)));
        $data['address'] = $this->input->post('alamat', TRUE);
        $data['city'] = strtoupper($this->input->post('kota', TRUE));
        $data['handphone'] = $this->input->post('handphone', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['bank_account_number'] = $this->input->post('norekening', TRUE);
        $data['bank_name'] = strtoupper($this->input->post('nmbank', TRUE));
        $data['pln_number'] = strtoupper($this->input->post('pln_number', TRUE));
		if($user_data->role==2) {
			$data['usertologin'] = strtoupper($this->input->post('username', TRUE));
		}
        
        $this->ST->update_array('ki_m_user',$data,$condition);
        $message['id_user'] = $id_user;                                                                                          
        echo json_encode($message);
    }

	public function update_pass_admin() {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        $condition = [ 'id_user' =>  $sess['id_user'], ];
        $data['ppass'] = md5($this->input->post('password', TRUE));
        
        $this->ST->update_array('ki_m_user',$data,$condition);
        $message['id_user'] = $id_user;                                                                                          
        echo json_encode($message);
    }
	
	public function update_pin_admin() {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        $condition = [ 'id_user' =>  $sess['id_user'], ];

        $data['ppin'] = md5($this->input->post('pin', TRUE));
		
        $this->ST->update_array('ki_m_user',$data,$condition);
        $message['id_user'] = $id_user;                                                                                          
        echo json_encode($message);
    }
	
    /**
     * General method for compare password
     */         
    public function pass_compare() {
      $id = 'cpass';
      $sess = $this->session->userdata('datauser');
      $id_user = $sess['id_user'];
      $user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
      $value = md5($this->input->post($id, TRUE));
      if($value == $user_data->ppass){
          $isValid = TRUE;
      } else {
          $isValid = FALSE;
      }

      echo json_encode(array(
          'valid' => $isValid,
      ));
    }

/**
     * General method for compare pin
     */         
    public function pin_compare() {     
	  $id = $this->input->post('id', TRUE);
      $sess = $this->session->userdata('datauser');
      $id_user = $sess['id_user'];
      $user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
      $value = md5($this->input->post($id, TRUE));
      if($value == $user_data->ppin){
          $isValid = TRUE;
      } else {
          $isValid = FALSE;
      }

      echo json_encode(array(
          'valid' => $isValid,
      ));
    }

    public function login()
    {
        if ($_POST) {
            $e = $this->input->post("usertologin", TRUE);
            $p = $this->input->post("password", TRUE);
            //$p = md5($p);
            //$s = $this->input->post("step", TRUE);

            if ($this->authenticate($e, $p)) {
                $user = $this->AM->get_user($e, $p);
                $message['name'] = 'Hi, ' . $user->name;
                
                $update_data['remember_token'] = bin2hex(openssl_random_pseudo_bytes(16));
                $condition['id_user'] = $user->id_user;
                $this->ST->update_array('ki_m_user',$update_data,$condition);
                
                $datauser = array(
                    'id_user' => $user->id_user,
                    'usertologin' => $user->usertologin,
                    'name' => $user->name,
                    'email' => $user->email,
                    'logged_in' => TRUE, 
                    'role' => $user->role,
                    'handphone' => $user->handphone,
                    'remember_token' => $update_data['remember_token'],
                    );
            
                $this->session->set_userdata('datauser', $datauser);
                $message['name'] = $user->name;
                $message['success'] = TRUE;
            } else {
                $message['success'] = FALSE;
                $message['false'] = 'Username atau password yang Anda masukkan tidak cocok';
            }
        } else {
            $message['success'] = FALSE;
            $message['false'] = "Masukkan username atau password Anda";
        }
        echo json_encode($message);
    }
    
    public function authenticate($e, $p)
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('usertologin', 'usertologin', 'required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'required|xss_clean');

        if ($this->form_validation->run()) {
            if ($this->AM->is_valid($e, $p)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
    
    public function check_role()
    {
        if ($this->AM->is_login()) {
            $sess = $this->session->userdata('datauser');
            $id_user = $sess['id_user'];

            $role = $this->AM->get_role($id_user);

            if ($role == 0 || $role == 1) {
                redirect('/dashboard');
            } else if ($role == 2) {
                redirect('/admin');
            } else {
                redirect('/');
            }

        } else {
            redirect('/');
        }
    }

    public function logout()
    {
        $sess = $this->session->userdata('datauser');
        if(!empty($sess)){
            $this->session->sess_destroy();
            redirect('/');
        } else {
            header('Location: ' . $_SERVER['HTTP_REFERER']);   
        }
    }
    
    public function forget_password()
    {
        if ($_POST) {
            $username = $this->input->post("username", TRUE);
            $data_user = $this->ST->get_by_id_single_row('ki_m_user',$username,'usertologin');
            if(!empty($data_user)) {
                $link_expired = sha1(mt_rand(10000, 99999) . time() . $data_user->email);
                $data['link_expired'] = $link_expired;
                $data['time_expired'] = date("Y-m-d H:i:s", strtotime('+1 days'));
                $condition['id_user'] = $data_user->id_user;
                $this->ST->update_array('ki_m_user',$data,$condition);
                
                $subject = 'Forget Password KOPEI-INT (kopei-int.com)';
                $email = $data['email'];
                $email_msg = 'Silahkan klik link berikut untuk me-reset password dan pin anda: <br><br>'
                                .base_url().'link/'.$link_expired.'<br><br>
                                Link tersebut aktif selama 1 hari.
                                <br><br><br><br>
                                � 2016 KOPEI-INT<br>
                                Jl. Hanjoyo Putro BT. 8 Atas<br>
                                Komplek Ruko Gesya Blok A-44<br>
                                Tanjung Pinang' ;
                send_email_mime($email,$subject,$email_msg);
                $message['success'] = TRUE;
            } else {
                $message['success'] = FALSE;
                $message['false'] = 'Username yang Anda masukkan tidak terdaftar';
            }
        } else {
            $message['success'] = FALSE;
            $message['false'] = "Masukkan Username Anda";
        }
        echo json_encode($message);
    }
    
    public function random_password()
    {
        $range_numeric = range(0,9);
        $range_alpha = range('a','z');
        $range_alphanumeric = implode(array_merge($range_alpha,$range_numeric));
        $random_code = '';
        for ($i = 0; $i < 6; $i++) {
            $random_code .= $range_alphanumeric[rand(0, strlen($range_alphanumeric) - 1)];
        }
        
        return $random_code;
    }
    
    public function link($keylink)
    {
        if (!$this->AM->is_key_available($keylink)) {
            $user = $this->AM->get_info_link($keylink);

            if (strtotime($user->time_expired) > strtotime(date("Y-m-d H:i:s"))) {
                if($user->is_active == 0){
                    // update is_active
                    $data['is_active'] = 1;
                    $condition['id_user'] = $user->id_user;
                    $this->ST->update_array('ki_m_user',$data,$condition);
                    
                    $sms_msg = 'Silahkan kunjungi Daftar Leader di website untuk pembelian Lot/Saham.';
                    raja_sms($user->handphone,$sms_msg);
                    
                    redirect('/login');
                } else {
                    $data['link'] = $keylink;
                    
                    $this->load->view('header', $data);
                    $this->load->view('general/change_pass');
                    $this->load->view('footer');
                }
            } else {
                $this->load->view('header', $data);
                $this->load->view('general/link_expired');
                $this->load->view('footer');
                }
        } else {
            redirect('/');
        }
    }
    
    public function set_pass()
    {
        $link = $this->input->post('link', TRUE);
        $pass = $this->input->post('pass', TRUE);
        $pin = $this->input->post('pin', TRUE);
        $pass = md5($pass);
        $pin = md5($pin);

        if (!$this->AM->is_key_available($link)) {
            $this->AM->set_password($link, $pass, $pin);
            echo 'Password dan PIN berhasil diganti';
        } else {
            redirect('/');
        }
    }
}