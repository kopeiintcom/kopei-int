<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public $message;

    function __construct()
    {
        parent::__construct();
        $this->load->model('setting_m', 'ST');
        $this->load->model('user_m', 'UM');
        $this->load->model('admin_m', 'AM');
        $this->load->model('transaction_m', 'TM');
        $this->load->model('request_m', 'RM');
        redirectToHTTPS();        
        is_valid_admin();
    }

    public function index()
    {
        
    }
    
   
    public function register_admin()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Register Admin |';
        $data['is_new_member'] = $this->UM->is_new_member($id_user);
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/register_admin');
        $this->load->view('footer');
    }
    

    public function dashboard_page()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Dashboard |';
        $data['is_new_member'] = $this->UM->is_new_member($id_user);
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
		
        $data['shared_point'] = $this->ST->get_setting_by_alias('point_keuntungan_harian');
		$data['point_data'] = $this->RM->get_data_request_point(1);
        $data['activate_data'] = $this->RM->get_data_request_activate_account();
		$data['stock_pendapatan'] = $this->TM->get_stock_pendapatan();
		// kas kantor
        $data['administrasi'] = $this->TM->get_administrasi();
        $data['dana_bersama'] = $this->TM->get_dana_bersama();
        $data['amal'] = $this->TM->get_all_amal();
        // statistic akun
		$data['total_penerima'] = $this->TM->get_total_penerima();
		$data['full_profit'] = $this->TM->get_total_full_profit();
		$data['total_antrian'] = $this->TM->get_total_antrian();
		$data['total_lot'] = $this->TM->get_total_lot();
		$data['total_member'] = $this->TM->get_total_member();
		$data['free_member'] = $this->TM->get_total_free_member();
        
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/dashboard', $data);
        $this->load->view('footer');
    }


    public function keuntungan_harian()
    {   
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Share Point |';
        $data['curr_nominal'] = $this->ST->get_setting_by_alias('point_keuntungan_harian');
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);        
        $this->load->view('admin/point_harian', $data);
        $this->load->view('footer');
    }

    public function do_bhh() {
     $inputvalue = $this->input->post('pointharian', true);
     $this->AM->doupdate_user_without_transaction($inputvalue);
    }

    public function update_master_keuntungan_harian() {
     $input = $this->input->post('pointharian', true);
     $data['content'] =  $input;
     $condition = [ 'alias' =>  'point_keuntungan_harian', ];
     $this->ST->update_array('ki_m_setting',$data,$condition);
    }
    
    public function admin_transfer_lot() {
      $sess = $this->session->userdata('datauser');
      $id_user = $sess['id_user'];  
      $data['segment'] = 'Admin Transfer LOT to User |';
      $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
      
      $this->load->view('header', $data);
      $this->load->view('admin/header', $data);
      $this->load->view('admin/menu', $data);        
      $this->load->view('admin/admin_transfer_lot_user', $data);
      $this->load->view('footer');
    }
    
    public function check_username()
    {
      $username = $this->input->post('username', TRUE);
	  
	  if($username==null) {
		$username = $this->input->post('sponsor_username', TRUE);
	  }
	  
      $isExist = $this->UM->is_username_exist($username, false);
      $user_data = $this->ST->get_by_id_single_row('ki_m_user', $username, 'usertologin');

      echo json_encode(array(
          'valid' => $isExist,
          'usernaname' => $username,
		  'nama' => $user_data->name,
          'kota' => $user_data->city,
          'hp' => $user_data->handphone,
      ));
    }
    
    public function do_trans_saham()
    {
        $sess = $this->session->userdata('datauser');
        $id_admin = $sess['id_user'];
        
        $id_user = $this->input->post('username', TRUE);
        
        $admin_data = $this->ST->get_by_id_single_row('ki_m_user',$id_admin,'id_user');
        $user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'usertologin');
        $point_amal = (float) $this->ST->get_setting_by_alias('point_amal');
        $point_administrasi = (float) $this->ST->get_setting_by_alias('point_administrasi');
        $point_dana_bersama = (float) $this->ST->get_setting_by_alias('point_dana_bersama');
        
        $jmlsaham = $this->input->post('pointlot', true);
        
        $data['inactive_lot'] = $user_data->inactive_lot + $jmlsaham;
        $condition = [ 'usertologin' =>  $id_user, ];
        // update lot data in ki_m_user
        $this->ST->update_array('ki_m_user',$data,$condition);
          
        $point_kas = $this->ST->get_by_id_single_row('ki_m_transaction_code', 6,'id_transaction_code');
        $data2['id_transaction_code'] = $point_kas->id_transaction_code;
        $data2['id_transaction_group'] = $point_kas->id_transaction_group;
        $data2['id_user'] = $user_data->id_user;
        $data2['description'] = $admin_data->usertologin." Transfer LOT ke ".$user_data->usertologin;
        $data2['value'] = $jmlsaham;
        $data2['isdk'] = 1;
        $data2['date_transaction'] = date('Y-m-d H:i:s');
        //insert to transaction
        $this->ST->insert('ki_t_transaction',$data2);   
    } 

    public function admin_profile() {
      $sess = $this->session->userdata('datauser');
      $id_user = $sess['id_user'];  
      $data['segment'] = 'Admin Profile |';
      $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
      $data['json'] = json_encode(array(
          //'valid' => $isExist,
          'username' => $data['user']->usertologin,
          'name' => $data['user']->name,
          'hp' => $data['user']->handphone,
          'email' => $data['user']->email,
          'address' => $data['user']->address,
          'city' => $data['user']->city,
          'rekening' => $data['user']->bank_account_number,
          'bank' => $data['user']->bank_name,
      ));
      
      $this->load->view('header', $data);
      $this->load->view('admin/header', $data);
      $this->load->view('admin/menu', $data);        
      $this->load->view('admin/update_admin_profile', $data);
      $this->load->view('footer');
    }

    public function update_admin_profile() {
      $sess = $this->session->userdata('datauser');
      $id_user = $sess['id_user'];
      
      $user_data = $this->ST->get_by_id_single_row('ki_m_user', $id_user, 'id_user');
      $isExist = $this->UM->is_username_exist($user_data->usertologin, false);

      echo json_encode(array(
          'valid' => $isExist,
          'usernaname' => $user_data->usertologin,
      ));
    }

    public function proses_pencairan_point_page()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Pencairan Point |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['point_data'] = $this->RM->get_data_request_point(1);
		$data['harga'] = (int)$this->ST->get_setting_by_alias('point_to_rp');
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/proses_pencairan_point', $data);
        $this->load->view('footer');
    }

    public function do_pencairan_point() {
        $id_user = $this->input->post('id'); 
        $user_name = $this->input->post('usertologin');
        $value = (int)$this->input->post('value');
        $id_request = $this->input->post('id_request');
        $phone = $this->input->post('handphone');
        
        $data['is_done'] = 1;
        $condition = [ 'id_user' =>  $id_user, 'id_request' =>  $id_request];
        // update lot data in ki_m_user
        $this->ST->update_array('ki_t_request',$data,$condition);
      
        $data2['id_transaction_code'] = 12;
        $data2['id_transaction_group'] = 2;
        $data2['id_user'] = $id_user;
        $data2['description'] = "Anda melakukan pencairan sebesar ".$value." point";
        $data2['value'] = $value;
        $data2['isdk'] = -1;
        $data2['date_transaction'] = date('Y-m-d H:i:s');
        //insert to transaction
        $this->ST->insert('ki_t_transaction',$data2);  
        
        $data3['id_transaction_code'] = 13;
        $data3['id_transaction_group'] = 1;
        $data3['id_user'] = $id_user;
        $data3['description'] = "Potongan pencairan saldo dari ".$user_name;
        $data3['value'] = ($value*2)/100;
        $data3['isdk'] = 1;
        $data3['date_transaction'] = date('Y-m-d H:i:s');
        //insert to transaction
        $this->ST->insert('ki_t_transaction',$data3); 

        $point_to_rp = (int)$this->ST->get_setting_by_alias('point_to_rp');
        
        $data4['id_transaction_code'] = 12;
        $data4['nominal'] = ($value*$point_to_rp)-(($value*$point_to_rp*2)/100);
        $data4['id_request'] = $id_request;
        $data4['id_user'] = $id_user;
        $data4['description'] = $user_name." Mencairkan ".$value." point";
        $data4['status'] = 1;
        //insert to transaction
        $this->ST->insert('ki_t_finance',$data4); 
        
        $message = 'Terimakasih telah melakukan pencairan saldo, uang anda sudah kami transer silahkan cek rekening anda.';
        raja_sms($phone,$message);
    }
	
    public function cancel_pencairan_point() {
        $id_request = $this->input->post('id_request');
        
        $data['is_done'] = 2;
        $condition = [ 'id_request' =>  $id_request];
        // update lot data in ki_m_user
        $this->ST->update_array('ki_t_request',$data,$condition);
    }
    
	public function daftar_member_page()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Daftar Member |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['data_member'] = $this->AM->get_all_member();
        
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/daftar_member', $data);
        $this->load->view('footer');
    }
	
	public function change_status_member() {
		$id_user = $this->input->post('id'); 
		$role = $this->input->post('role'); 
		if($role==0) {
			$data['role'] = 1;
		} else {
			$data['role'] = 0;
		}
		
		$condition = [ 'id_user' => $id_user, ];
		$this->ST->update_array('ki_m_user',$data,$condition);
	}
    
    public function reset_password() {
		$id_user = $this->input->post('id'); 
		$data['ppass'] = md5('qwerty');
		$condition = [ 'id_user' => $id_user, ];
		$this->ST->update_array('ki_m_user',$data,$condition);
	}
	
	public function reset_pin() {
		$id_user = $this->input->post('id'); 
		$data['ppin'] = md5('123456');
		$condition = [ 'id_user' => $id_user, ];
		$this->ST->update_array('ki_m_user',$data,$condition);
	}
	
	public function daftar_leader_page()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Daftar Leader |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['data_member'] = $this->ST->get_by_id_multiple_rows('ki_m_user',1,'role');
        
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/daftar_leader', $data);
        $this->load->view('footer');
    }
	
	public function change_status_leader() {
		$id_user = $this->input->post('id'); 
		$data['role'] = 0;
		$condition = [ 'id_user' => $id_user, ];
		$this->ST->update_array('ki_m_user',$data,$condition);
	}
    
    public function delete_member() {
		$id_user = $this->input->post('id'); 
		$data['is_delete'] = 1;
		$condition = [ 'id_user' => $id_user, ];
		$this->ST->update_array('ki_m_user',$data,$condition);
	}
    
    public function activate_member() {
		$id_user = $this->input->post('id'); 
		$data['is_active'] = 1;
		$condition = [ 'id_user' => $id_user, ];
		$this->ST->update_array('ki_m_user',$data,$condition);
	}
    
    public function fee_management()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Bagi Hasil Management |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['nominal'] = $this->TM->get_administrasi();
        $data['convert'] = $this->ST->get_setting_by_alias('point_to_rp');
        $data['master'] = $this->ST->get_all_data('ki_m_fee_management'); 
        
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/fee_management', $data);
        $this->load->view('footer');
    }
    
    public function stok_pendapatan()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Stok Pendapatan |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['saldo'] = $this->TM->get_transaction_stock_pendapatan();
        
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/stok_pendapatan', $data);
        $this->load->view('footer');
    }
    
    public function dana_bersama()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Dana Bersama |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['saldo'] = $this->TM->get_transaction_dana_bersama();
		$this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/dana_bersama', $data);
		$this->load->view('footer');
	}
    
    public function amal()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Amal |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['saldo'] = $this->TM->get_transaction_amal();
        
		$this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/amal', $data);
		$this->load->view('footer');
	}
    
    public function administrasi()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'Administrasi |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['saldo'] = $this->TM->get_transaction_administrasi();
        
		$this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/administrasi', $data);
		$this->load->view('footer');
	}
	
	public function mustasi_lot_page() {
		$sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];  
      
        $data['segment'] = 'Mutasi LOT |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['history_lot'] = $this->TM->get_mutasi_lot_admin();
        
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);        
        $this->load->view('admin/admin_mutasi_lot', $data);
        $this->load->view('footer');
	}
	
	public function send_pencairan_point_notif(){
        $phone = $this->input->post('handphone');
		
        $message = 'Terimakasih telah melakukan pencairan saldo, uang anda akan kami transer silahkan cek rekening anda.';
        raja_sms($phone,$message);
        
        //echo json_encode($verification_code);
        return '';
    }
	
	public function detail_member($id_user) {

		$sess = $this->session->userdata('datauser');
        $id_admin = $sess['id_user'];
		
        $data['segment'] = 'Detail Member |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_admin,'id_user');
        $user_point = (int)$this->TM->get_saldo_dk_by_user($id_user);
        $proses_point = (int)$this->RM->get_process_point_by_user($id_user);
		$minimal_point = (int)$this->ST->get_setting_by_alias('pencairan_minimum');
        
        // akun
        $data['saldo'] = $this->TM->get_saldo_dk_by_user($id_user);
        $data['total_lot'] = $this->TM->get_total_lot_by_user($id_user);
		$data['active_lot'] = $this->TM->get_lot_active_user($id_user);
		$data['inactive_lot'] = $this->TM->get_lot_inactive_user($id_user);
		$data['queue_lot'] = $this->TM->get_antri_lot_user($id_user);
		$data['non_active_lot'] = $this->get_non_active_lot($id_user);
		
		$data['mutasi_lot'] = $this->TM->get_mutasi_lot($id_user);
		$data['pendapatan'] = $this->TM->get_dk_transaction_by_user($id_user);
		$user_data = $this->ST->get_by_id_single_row('ki_m_user', $id_user, 'id_user');
        $sponsor_data = $this->ST->get_by_id_single_row('ki_m_user', $user_data->id_sponsor, 'id_user');
		$data['user_data'] = $user_data;
		
		$data['json'] = json_encode(array(
          'username' => $user_data->usertologin,
          'name' => $user_data->name,
          'hp' => $user_data->handphone,
          'email' => $user_data->email,
          'address' => $user_data->address,
          'city' => $user_data->city,
          'rekening' => $user_data->bank_account_number,
          'bank' => $user_data->bank_name,
		  'id_user' => $user_data->id_user,
          'sponsor_username' => $sponsor_data->usertologin,
          'sponsor_name' => $sponsor_data->name,
		));
		
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/detail_member', $data);
        $this->load->view('footer');
		
	}
	
	public function get_non_active_lot($id_user ) {
		$master_point_saham = (int) $this->ST->get_setting_by_alias('point_saham');
        $master_end_percent_point = (float) $this->ST->get_setting_by_alias('end_percent_point_saham');
		$max_point = $master_point_saham*$master_end_percent_point;
		
		$user_data = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
		$lot = $user_data->active_lot;
		$saldoDK = $this->TM->get_saldo_dk_by_user($id_user);
		$nilai = 0;
		if ($lot>0) {
			$nilai = $saldoDK/($lot*$max_point);
		}
		
		return floor($nilai);
	}
	
	public function update_profile_sponsor() {
		$username = $this->input->post('sponsor_username', TRUE);
		$id_user = $this->input->post('user_id', TRUE);
		
		$user_data =  $this->ST->get_by_id_single_row('ki_m_user',$username,'usertologin');
        $condition = [ 'id_user' =>  $id_user, ];
        $data['id_sponsor'] = $user_data->id_user;
        $this->ST->update_array('ki_m_user',$data,$condition);
        $message['id_user'] = $id_user;                                                                                          
        echo json_encode($message);
    }
    
    public function proses_ppob_pulsa()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'PPOB Pulsa |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['point_data'] = $this->RM->get_data_request_point(2);
		$data['harga'] = (int)$this->ST->get_setting_by_alias('point_to_rp');
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/proses_ppob_pulsa', $data);
        $this->load->view('footer');
    }

    public function do_proses_ppob_pulsa() {
        $id_user = $this->input->post('id_user'); 
        $id_request = $this->input->post('id_request');
        $point_to_rp = (int)$this->ST->get_setting_by_alias('point_to_rp');
        $data_user = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data_request = $this->ST->get_by_id_single_row('ki_t_request',$id_request,'id_request');
        $data_ppob = $this->ST->get_by_id_single_row('ki_m_ppob',$data_request->id_support,'id_ppob');
        
        
        $data['is_done'] = 1;
        $condition = [ 'id_user' =>  $id_user, 'id_request' =>  $id_request];
        // update lot data in ki_m_user
        $this->ST->update_array('ki_t_request',$data,$condition);
        
        $data2['id_transaction_code'] = 16;
        $data2['id_transaction_group'] = 2;
        $data2['id_user'] = $id_user;
        $data2['description'] = "Pembelian pulsa sebesar ".$data_request->value." point";
        $data2['value'] = $data_request->value;
        $data2['isdk'] = -1;
        $data2['date_transaction'] = date('Y-m-d H:i:s');
        //insert to transaction
        $this->ST->insert('ki_t_transaction',$data2);  
        
        $data3['id_transaction_code'] = 13;
        $data3['id_transaction_group'] = 1;
        $data3['id_user'] = $id_user;
        $data3['description'] = "Keuntungan pembelian pulsa dari ".$data_user->usertologin." (".$data_user->name.")";
        $data3['value'] = $data_ppob->price_markup/$point_to_rp;
        $data3['isdk'] = 1;
        $data3['date_transaction'] = date('Y-m-d H:i:s');
        //insert to transaction
        $this->ST->insert('ki_t_transaction',$data3); 
    }
    
    public function proses_ppob_listrik()
    {
        $sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        
        $data['segment'] = 'PPOB Listrik |';
        $data['user'] = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data['point_data'] = $this->RM->get_data_request_point(3);
		$data['harga'] = (int)$this->ST->get_setting_by_alias('point_to_rp');
        $this->load->view('header', $data);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/menu', $data);
        $this->load->view('admin/proses_ppob_listrik', $data);
        $this->load->view('footer');
    }

    public function do_proses_ppob_listrik() {
        $id_user = $this->input->post('id_user'); 
        $id_request = $this->input->post('id_request');
        $point_to_rp = (int)$this->ST->get_setting_by_alias('point_to_rp');
        $data_user = $this->ST->get_by_id_single_row('ki_m_user',$id_user,'id_user');
        $data_request = $this->ST->get_by_id_single_row('ki_t_request',$id_request,'id_request');
        $data_ppob = $this->ST->get_by_id_single_row('ki_m_ppob',$data_request->id_support,'id_ppob');
        
        
        $data['is_done'] = 1;
        $condition = [ 'id_user' =>  $id_user, 'id_request' =>  $id_request];
        // update lot data in ki_m_user
        $this->ST->update_array('ki_t_request',$data,$condition);
        
        $data2['id_transaction_code'] = 17;
        $data2['id_transaction_group'] = 2;
        $data2['id_user'] = $id_user;
        $data2['description'] = "Pembelian token listrik sebesar ".$data_request->value." point";
        $data2['value'] = $data_request->value;
        $data2['isdk'] = -1;
        $data2['date_transaction'] = date('Y-m-d H:i:s');
        //insert to transaction
        $this->ST->insert('ki_t_transaction',$data2);  
        
        $data3['id_transaction_code'] = 13;
        $data3['id_transaction_group'] = 1;
        $data3['id_user'] = $id_user;
        $data3['description'] = "Keuntungan pembelian token listrik dari ".$data_user->usertologin." (".$data_user->name.")";
        $data3['value'] = $data_ppob->price_markup/$point_to_rp;
        $data3['isdk'] = 1;
        $data3['date_transaction'] = date('Y-m-d H:i:s');
        //insert to transaction
        $this->ST->insert('ki_t_transaction',$data3); 
    }
}
