<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller
{

    public $message;

    function __construct()
    {
        parent::__construct();
        $this->load->model('setting_m', 'ST');
        $this->load->model('user_m', 'UM');
        $this->load->model('admin_m', 'AM');
        $this->load->model('transaction_m', 'TM');
        //redirectToHTTPS();
    }

    public function index()
    {
        $data['segment'] = 'Simulasi |';
        
        $this->load->view('header', $data);
        $this->load->view('general/simulasi', $data);
        $this->load->view('footer');
    }
    
    public function do_bhh() {
        $inputvalue = $this->input->post('pointharian', true);
        $this->AM->doupdate_user_without_transaction($inputvalue);
    }
    
    public function do_aktivasi_saham() 
    {
        /*$activate_data = $this->UM->get_activate_saham();
        $master_point_saham = (int) $this->ST->get_setting_by_alias('point_saham');
        $master_end_percent_point = (float) $this->ST->get_setting_by_alias('end_percent_point_saham');
        $master_activate = $this->ST->get_all_data('ki_m_activate_saham');
        $master_generation = $this->ST->get_all_data('ki_m_generation');
        $now = date('Y-m-d H:i:s');
        // for emergency
        //$now = '2016-07-11 23:55:01';
        foreach($activate_data as $active_data){
            //echo 'masuk foreach';
            $user_data = $this->ST->get_by_id_single_row('ki_m_user',$active_data->id_user,'id_user');
            $gap_days = date_diff(date_create($active_data->date_actived),date_create($now))->format("%a"); 
            if($gap_days >= 4 && $gap_days <= 14){
            
            //echo $gap_days;
                foreach($master_activate as $master){
                    if($gap_days == $master->days_queue){
                        $jml_saham_master = $master->value;
                    }
                }
                // data from setting
                $point_bagi_hasil = (float) $this->ST->get_setting_by_alias('point_bagi_hasil');
                $point_bonus_generasi = (float) $this->ST->get_setting_by_alias('point_bonus_generasi');
                $point_bonus_sponsor = (float) $this->ST->get_setting_by_alias('point_bonus_sponsor');
                $point_amal = (float) $this->ST->get_setting_by_alias('point_amal');
                $point_administrasi = (float) $this->ST->get_setting_by_alias('point_administrasi');
                $point_dana_bersama = (float) $this->ST->get_setting_by_alias('point_dana_bersama');
                
                if($active_data->quantity > $jml_saham_master){
                    $jmlsaham = $jml_saham_master;
                }else{
                    $jmlsaham = $active_data->quantity;
                }
                
                //update ki_m_user
                $data['active_lot'] = $user_data->active_lot + $jmlsaham;
                //$data12['inactive_lot'] = $user_data->inactive_lot - $jmlsaham;
                $condition['id_user'] = $user_data->id_user;
                $this->ST->update_array('ki_m_user',$data,$condition);  
                
                //update ki_t_activate_saham
                $data2['quantity'] = $active_data->quantity - $jmlsaham;
                $condition2['id_activate_saham'] = $active_data->id_activate_saham;
                $this->ST->update_array('ki_t_activate_saham',$data2,$condition2); 
                
                //insert ki_t_activate_bagi_hasil 
                $data3['id_user'] = $user_data->id_user;
                $data3['value'] = $jmlsaham;
                $data3['date_actived'] = $now;
                $this->ST->insert('ki_t_activate_bagi_hasil',$data3);
                
            }
        }*/
    }
    
    
    public function do_aktivasi_bagi_hasil()
    {
        // master data
        /*$master_point_saham = (int) $this->ST->get_setting_by_alias('point_saham');
        $master_end_percent_point = (float) $this->ST->get_setting_by_alias('end_percent_point_saham');
        $master_point_bagi_hasil = (float) $this->ST->get_setting_by_alias('point_bagi_hasil');
        $master_point_kh = (float) $this->ST->get_setting_by_alias('point_keuntungan_harian');
        $master_max_daily_point = (float) $this->ST->get_setting_by_alias('max_daily_point');
        $master_min_sp_point = (float) $this->ST->get_setting_by_alias('min_to_use_stok_pendapatan');
        $code_bhh = $this->ST->get_by_id_single_row('ki_m_transaction_code',5,'id_transaction_code');
        $code_stok_pendapatan = $this->ST->get_by_id_single_row('ki_m_transaction_code',13,'id_transaction_code');
        $now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        // for emergency
        //$now = '2016-07-11 23:55:01';
        //$date_now = date('2016-07-11');
        // get lot yg diaktifasi hari ini
        $posting_data = $this->UM->get_start_activate_saham($date_now);
        $total_lot_active = $this->UM->total_active_lot();
        
        $get_user_has_active_lot = $this->UM->get_user_has_active_lot();
        
        $active_lot_today = 0;
        if (!empty($posting_data)) {
            $active_lot_today = $posting_data;
        }
        $pk = $master_point_bagi_hasil * $active_lot_today;
        // jika ada lot yg diaktifkan hari ini dan ada lot yg aktif
        if ($active_lot_today > 0 && $total_lot_active > 0){
            $pps = $pk/$total_lot_active;
            // jika pendapatan per saham lebih besar dari 0.55
            if($pps >= $master_max_daily_point){
                $total_kh = 0;
                foreach($get_user_has_active_lot as $user){
                    $kh = $master_max_daily_point * $user->active_lot;
                    // get data transaction user
                    $user_saldo_d = (float) $this->TM->get_saldo_d_bagi_hasil_by_user($user->id_user);
                    if(empty($user_saldo_d)) {
                        $user_saldo_d = 0;
                    }
                    $user_max_point = $user->active_lot*$master_point_saham*$master_end_percent_point;
                    // check apakah sudah habis masa kontraknya
                    if($user_max_point > $user_saldo_d){
                        $value_gap = ($user_saldo_d + $kh) - $user_max_point;
                        if($value_gap <= 0 ){
                            // insert into transaction 
                            $data1['id_transaction_group'] = $code_bhh->id_transaction_group;
                            $data1['id_transaction_code'] = $code_bhh->id_transaction_code;
                            $data1['id_user'] = $user->id_user;
                            $data1['description'] = $code_bhh->description;
                            $data1['value'] = $kh;
                            $data1['isdk'] = 1;
                            $data1['date_transaction'] = $now;
                            $this->ST->insert('ki_t_transaction',$data1);
                        } else {
                            // accum melebihi nilai max point
                            $data2['id_transaction_group'] = $code_bhh->id_transaction_group;
                            $data2['id_transaction_code'] = $code_bhh->id_transaction_code;
                            $data2['id_user'] = $user->id_user;
                            $data2['description'] = $code_bhh->description;
                            $data2['value'] = $user_max_point - $user_saldo_d;
                            $data2['isdk'] = 1;
                            $data2['date_transaction'] = $now;
                            $this->ST->insert('ki_t_transaction',$data2);
                            
                            // sisanya masuk ke stok pendapatan jika akumulasi nya sudah melebihi nilai max point 
                            $data3['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
                            $data3['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
                            $data3['id_user'] = $user->id_user;
                            $data3['description'] = 'Sisa Bagi hasil';
                            $data3['value'] = $value_gap;
                            $data3['isdk'] = 1;
                            $data3['date_transaction'] = $now;
                            $this->ST->insert('ki_t_transaction',$data3); 
                        }
                    } else {
                        // jika sudah habis masa kontraknya masuk ke stok pendapatan
                        $data4['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
                        $data4['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
                        $data4['id_user'] = 0;
                        $data4['description'] = 'Bagi Hasil - Masa kontrak sudah habis';
                        $data4['value'] = $kh;
                        $data4['isdk'] = 1;
                        $data4['date_transaction'] = $now;
                        $this->ST->insert('ki_t_transaction',$data4); 
                    }
                $total_kh = $total_kh + $kh;
                }
                
                // simpan sisa ke Stok Pendapatan
                if($pk - $total_kh > 0){ 
                    $data5['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
                    $data5['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
                    $data5['id_user'] = 0;
                    $data5['description'] = 'Sisa Bagi hasil harian';
                    $data5['value'] = $pk - $total_kh;
                    $data5['isdk'] = 1;
                    $data5['date_transaction'] = $now;
                    $this->ST->insert('ki_t_transaction',$data5);
                } 
            } else {
                // jika bagi hasil per lot kurang dari 0.40
                if($pps <= $master_min_sp_point){
                    $total_kh = 0;
                    foreach($get_user_has_active_lot as $user){
                        $kh = ($master_point_kh + $pps) * $user->active_lot;
                        // get data transaction user
                        $user_saldo_d = (float) $this->TM->get_saldo_d_bagi_hasil_by_user($user->id_user);
                        if(empty($user_saldo_d)) {
                            $user_saldo_d = 0;
                        }
                        $user_max_point = $user->active_lot*$master_point_saham*$master_end_percent_point;
                        // check apakah sudah habis masa kontraknya
                        if($user_max_point > $user_saldo_d){
                            $value_gap = ($user_saldo_d + $kh) - $user_max_point;
                            if($value_gap <= 0 ){
                                // insert into transaction 
                                $data6['id_transaction_group'] = $code_bhh->id_transaction_group;
                                $data6['id_transaction_code'] = $code_bhh->id_transaction_code;
                                $data6['id_user'] = $user->id_user;
                                $data6['description'] = $code_bhh->description;
                                $data6['value'] = $kh;
                                $data6['isdk'] = 1;
                                $data6['date_transaction'] = $now;
                                $this->ST->insert('ki_t_transaction',$data6);
                            } else {
                                // accum melebihi nilai max point
                                $data7['id_transaction_group'] = $code_bhh->id_transaction_group;
                                $data7['id_transaction_code'] = $code_bhh->id_transaction_code;
                                $data7['id_user'] = $user->id_user;
                                $data7['description'] = $code_bhh->description;
                                $data7['value'] = $user_max_point - $user_saldo_d;
                                $data7['isdk'] = 1;
                                $data7['date_transaction'] = $now;
                                $this->ST->insert('ki_t_transaction',$data7);
                                
                                // sisanya masuk ke stok pendapatan jika akumulasi nya sudah melebihi nilai max point 
                                $data8['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
                                $data8['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
                                $data8['id_user'] = $user->id_user;
                                $data8['description'] = 'Sisa Bagi hasil';
                                $data8['value'] = $value_gap;
                                $data8['isdk'] = 1;
                                $data8['date_transaction'] = $now;
                                $this->ST->insert('ki_t_transaction',$data8); 
                            }
                        } else {
                            // jika sudah habis masa kontraknya masuk ke stok pendapatan
                            $data9['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
                            $data9['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
                            $data9['id_user'] = 0;
                            $data9['description'] = 'Bagi Hasil - Masa kontrak sudah habis';
                            $data9['value'] = $kh;
                            $data9['isdk'] = 1;
                            $data9['date_transaction'] = $now;
                            $this->ST->insert('ki_t_transaction',$data9); 
                        }
                    $total_kh = $total_kh + $kh;
                    }
                    
                    // simpan sisa ke Stok Pendapatan
                    if($pk - $total_kh > 0){ 
                        $data10['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
                        $data10['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
                        $data10['id_user'] = 0;
                        $data10['description'] = 'Sisa Bagi hasil harian';
                        $data10['value'] = $pk - $total_kh;
                        $data10['isdk'] = 1;
                        $data10['date_transaction'] = $now;
                        $this->ST->insert('ki_t_transaction',$data10);
                    } 
                } else {
                    // jika bagi hasil per lot lebih dari 0.40
                    $total_kh = 0;
                    foreach($get_user_has_active_lot as $user){
                        $kh = $pps * $user->active_lot;
                        // get data transaction user
                        $user_saldo_d = (float) $this->TM->get_saldo_d_bagi_hasil_by_user($user->id_user);
                        if(empty($user_saldo_d)) {
                            $user_saldo_d = 0;
                        }
                        $user_max_point = $user->active_lot*$master_point_saham*$master_end_percent_point;
                        // check apakah sudah habis masa kontraknya
                        if($user_max_point > $user_saldo_d){
                            $value_gap = ($user_saldo_d + $kh) - $user_max_point;
                            if($value_gap <= 0 ){
                                // insert into transaction 
                                $data11['id_transaction_group'] = $code_bhh->id_transaction_group;
                                $data11['id_transaction_code'] = $code_bhh->id_transaction_code;
                                $data11['id_user'] = $user->id_user;
                                $data11['description'] = $code_bhh->description;
                                $data11['value'] = $kh;
                                $data11['isdk'] = 1;
                                $data11['date_transaction'] = $now;
                                $this->ST->insert('ki_t_transaction',$data11);
                            } else {
                                // accum melebihi nilai max point
                                $data12['id_transaction_group'] = $code_bhh->id_transaction_group;
                                $data12['id_transaction_code'] = $code_bhh->id_transaction_code;
                                $data12['id_user'] = $user->id_user;
                                $data12['description'] = $code_bhh->description;
                                $data12['value'] = $user_max_point - $user_saldo_d;
                                $data12['isdk'] = 1;
                                $data12['date_transaction'] = $now;
                                $this->ST->insert('ki_t_transaction',$data12);
                                
                                // sisanya masuk ke stok pendapatan jika akumulasi nya sudah melebihi nilai max point 
                                $data13['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
                                $data13['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
                                $data13['id_user'] = $user->id_user;
                                $data13['description'] = 'Sisa Bagi hasil';
                                $data13['value'] = $value_gap;
                                $data13['isdk'] = 1;
                                $data13['date_transaction'] = $now;
                                $this->ST->insert('ki_t_transaction',$data13); 
                            }
                        } else {
                            // jika sudah habis masa kontraknya masuk ke stok pendapatan
                            $data14['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
                            $data14['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
                            $data14['id_user'] = 0;
                            $data14['description'] = 'Bagi Hasil dari '. $user->usertologin .' (Masa kontrak sudah habis)';
                            $data14['value'] = $kh;
                            $data14['isdk'] = 1;
                            $data14['date_transaction'] = $now;
                            $this->ST->insert('ki_t_transaction',$data14); 
                        }
                    $total_kh = $total_kh + $kh;
                    }
                    
                    // simpan sisa ke Stok Pendapatan
                    if($pk - $total_kh > 0){ 
                        $data15['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
                        $data15['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
                        $data15['id_user'] = 0;
                        $data15['description'] = 'Sisa Bagi hasil harian';
                        $data15['value'] = $pk - $total_kh;
                        $data15['isdk'] = 1;
                        $data15['date_transaction'] = $now;
                        $this->ST->insert('ki_t_transaction',$data15);
                    } 
                }
            }
        } else if ($active_lot_today == 0 && $total_lot_active > 0){
            // jika tidak ada lot yg diaktifkan
            $total_kh = 0;
            foreach($get_user_has_active_lot as $user){
                $kh =  $master_point_kh * $user->active_lot;
                // get data transaction user
                $user_saldo_d = (float) $this->TM->get_saldo_d_bagi_hasil_by_user($user->id_user);
                if(empty($user_saldo_d)) {
                    $user_saldo_d = 0;
                }
                $user_max_point = $user->active_lot*$master_point_saham*$master_end_percent_point;
                // check apakah sudah habis masa kontraknya
                if($user_max_point > $user_saldo_d){
                    $value_gap = ($user_saldo_d + $kh) - $user_max_point;
                    if($value_gap <= 0 ){
                        // insert into transaction 
                        $data16['id_transaction_group'] = $code_bhh->id_transaction_group;
                        $data16['id_transaction_code'] = $code_bhh->id_transaction_code;
                        $data16['id_user'] = $user->id_user;
                        $data16['description'] = $code_bhh->description;
                        $data16['value'] = $kh;
                        $data16['isdk'] = 1;
                        $data16['date_transaction'] = $now;
                        $this->ST->insert('ki_t_transaction',$data16);
                    } else {
                        // accum melebihi nilai max point
                        $data17['id_transaction_group'] = $code_bhh->id_transaction_group;
                        $data17['id_transaction_code'] = $code_bhh->id_transaction_code;
                        $data17['id_user'] = $user->id_user;
                        $data17['description'] = $code_bhh->description;
                        $data17['value'] = $user_max_point - $user_saldo_d;
                        $data17['isdk'] = 1;
                        $data17['date_transaction'] = $now;
                        $this->ST->insert('ki_t_transaction',$data17);
                        
                        // sisanya masuk ke stok pendapatan jika akumulasi nya sudah melebihi nilai max point 
                        $data18['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
                        $data18['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
                        $data18['id_user'] = $user->id_user;
                        $data18['description'] = 'Sisa Bagi hasil';
                        $data18['value'] = $value_gap;
                        $data18['isdk'] = 1;
                        $data18['date_transaction'] = $now;
                        $this->ST->insert('ki_t_transaction',$data18); 
                    }
                } else {
                    // jika sudah habis masa kontraknya masuk ke stok pendapatan
                    $data19['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
                    $data19['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
                    $data19['id_user'] = 0;
                    $data19['description'] = 'Bagi Hasil dari '. $user->usertologin .' (Masa kontrak sudah habis)';
                    $data19['value'] = $kh;
                    $data19['isdk'] = 1;
                    $data19['date_transaction'] = $now;
                    $this->ST->insert('ki_t_transaction',$data19); 
                }
            $total_kh = $total_kh + $kh;
            }
            
            if($total_kh > 0) {
                // Stok Pendapatan dikurangi
                $data20['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
                $data20['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
                $data20['id_user'] = 0;
                $data20['description'] = 'Bantuan bagi hasil harian';
                $data20['value'] = $total_kh;
                $data20['isdk'] = -1;
                $data20['date_transaction'] = $now;
                $this->ST->insert('ki_t_transaction',$data20);
            } 
        } else {
            // Stok Pendapatan ditambah
            $data21['id_transaction_group'] = $code_stok_pendapatan->id_transaction_group;
            $data21['id_transaction_code'] = $code_stok_pendapatan->id_transaction_code;
            $data21['id_user'] = 0;
            $data21['description'] = 'Bagi hasil yang tidak dibagikan';
            $data21['value'] = $pk;
            $data21['isdk'] = 1;
            $data21['date_transaction'] = $now;
            $this->ST->insert('ki_t_transaction',$data21);
        } */
    }
    
    public function backup_database()
    {
        exec('/usr/bin/mysqldump -u u219400032_kopei -p4kuk4y4r4y4 u219400032_kopei > /home/u219400032/public_html/backup_db/'.date('YmdHi').'.sql');
    }
    
    public function testing_cron()
    {
        //raja_sms('085710042685','raja sms is online now');
        
        
    }
    
}
