<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller
{

    public $message;
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('setting_m', 'ST');
        $this->load->model('user_m', 'UM');
        $this->load->model('authenticate_m', 'AM');
        redirectToHTTPS();        
        die('<h1 style="text-align:center;margin-top:150px;">-------WE ARE UNDER MAINTENANCE-------</h1><br /><h3 style="text-align:center;">Please wait until we finish this for you</h3><h3 style="text-align:center;color:lightblue;">kopei-int.com</h3><h4 style="text-align:center;">-Komunitas Pejuang Ekonomi Internasional-</h4>');
    }
    
    public function index()
    {   
        $data['segment'] = 'Home |';
        //die('<h1 style="text-align:center;margin-top:150px;">-------WE ARE UNDER MAINTENANCE-------</h1><br /><h3 style="text-align:center;">Please wait until we finish this for you</h3><h3 style="text-align:center;color:lightblue;">kopei-int.com</h3><h4 style="text-align:center;">-Komunitas Pejuang Ekonomi Internasional-</h4>');
                
        $this->load->view('header', $data);
        $this->load->view('general/menu', $data);
        $this->load->view('general/home', $data);
        $this->load->view('footer');
    }
    
    public function login_page()
    {
        $data['segment'] = 'Login |';
        
        $this->load->view('header', $data);
        $this->load->view('general/login');
        $this->load->view('footer');
    }
    
    public function register()
    {
        $data['segment'] = 'Register |';
        
        $data['id_sponsor'] = $this->input->get('id_sponsor');
        
        $this->load->view('header', $data);
        $this->load->view('general/menu', $data);
        $this->load->view('general/register');
        $this->load->view('footer');
    }
    
    public function check_email()
    {
        $email = $this->input->post('email', TRUE);
        $isAvailable = $this->UM->is_email_available($email);

        echo json_encode(array(
            'valid' => $isAvailable,
            'email' => $email
        ));
    }
    
    public function check_handphone()
    {
        $handphone = $this->input->post('handphone', TRUE);
        $isAvailable = $this->UM->is_handphone_available($handphone);

        echo json_encode(array(
            'valid' => $isAvailable,
            'handphone' => $handphone
        ));
    }
    
	public function check_email_update()
    {
		$sess = $this->session->userdata('datauser');
        $id_user = $sess['id_user'];
        $email = $this->input->post('email', TRUE);
		$c_id_user = $this->input->post('id_user', TRUE);
		if ($id_user==$c_id_user) {
			$isAvailable = true;
		} else {
			$isAvailable = $this->UM->is_email_available($emails);
		}
        

        echo json_encode(array(
            'valid' => $isAvailable,
            'email' => $email
        ));
    }
	
    public function check_rekening()
    {
        $rek = $this->input->post('norekening', TRUE);
        $isAvailable = $this->UM->is_rekening_available($rek);

        echo json_encode(array(
            'valid' => $isAvailable,
            'norekening' => $rek
        ));
    }
    
    public function check_sponsor()
    {
        $sponsor = strtoupper($this->input->post('user_sponsor', TRUE));
        $seller_code = $this->ST->get_setting_by_alias('code_register_seller');
        if($sponsor == $seller_code){
            $isExist = TRUE;
        } else {
            $isExist = $this->UM->is_sponsor_exist($sponsor);
        }

        echo json_encode(array(
            'valid' => $isExist,
            'sponsor' => $sponsor
        ));
    }
    
    public function send_sms_verification(){
        $phone = $this->input->post('handphone');
        $verification_code = $this->input->post('code');
        
        $message = 'Kode verifikasi pendaftaran anda : '.$verification_code;
        raja_sms($phone,$message);
        
        //echo json_encode($verification_code);
        return '';
    }
    
    public function marketing()
    {   
        $data['segment'] = 'Marketing |';
                
        $this->load->view('header', $data);
        $this->load->view('general/menu', $data);
        $this->load->view('general/marketing', $data);
        $this->load->view('footer');
    }
    
    public function legalitas()
    {   
        $data['segment'] = 'Legalitas |';
                
        $this->load->view('header', $data);
        $this->load->view('general/menu', $data);
        $this->load->view('general/legalitas', $data);
        $this->load->view('footer');
    }
    
    public function daftar_leader()
    {   
        $data['segment'] = 'Leader |';
        
        $data['leader'] = $this->ST->get_by_id_multiple_rows('ki_m_user',1,'role');
                
        $this->load->view('header', $data);
        $this->load->view('general/menu', $data);
        $this->load->view('general/daftar_leader', $data);
        $this->load->view('footer');
    }
    
    public function contact_us()
    {   
        $data['segment'] = 'Kontak Kami |';
                
        $this->load->view('header', $data);
        $this->load->view('general/menu', $data);
        $this->load->view('general/contact_us', $data);
        $this->load->view('footer');
    }
    
    public function gallery()
    {   
        $data['segment'] = 'Gallery |';
                
        $this->load->view('header', $data);
        $this->load->view('general/menu', $data);
        $this->load->view('general/gallery', $data);
        $this->load->view('footer');
    }
    
    public function roundcube_email()
    {
        header('Location: http://webmail1.idhostinger.com/roundcube/');
    }
}