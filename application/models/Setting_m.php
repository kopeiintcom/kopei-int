<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Setting_m extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function get_all_setting(){
        return $this->db->get('ki_m_setting');
    }

    public function get_setting_by_alias($alias){
    	$this->db->select('content');
        $this->db->where('alias', $alias);
        $q = $this->db->get('ki_m_setting');
        return $q->row()->content;
    }

    public function get_by_id_single_row($table,$id,$id_name){
        $query = $this->db->get_where($table, array($id_name => $id));
        return $query->row();
    }

    public function get_by_id_multiple_rows($table,$id,$id_name){
        $query = $this->db->get_where($table, array($id_name => $id));
        return $query->result();
    }

    public function get_where_in_multiple_rows($table,$id,$id_name){
        $this->db->select("*");
        $this->db->where_in($id_name,$id);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_where_not_in_multiple_rows($table,$id,$id_name,$ignore,$ignore_name){
        $this->db->select("*");
        $this->db->where($id_name,$id);
        $this->db->where_not_in($ignore_name,$ignore);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_total_row($table,$id,$id_name){
        $query = $this->db->get_where($table, array($id_name => $id));
        return $query->num_rows();
    }

    public function get_total_all_data($table){
        $query = $this->db->get($table); 
        return $query->num_rows();
    }

    public function get_all_data($table){
        $query = $this->db->get($table); 
        return $query->result();
    }

    public function get_all_data_order_by_limit($table,$order_by,$ordered,$limit){
        $this->db->order_by($order_by, $ordered); 
        $this->db->limit($limit);
        $query = $this->db->get($table); 
        return $query->result();
    }
    
    public function get_join($table1,$table2,$pivot_name,$where_name,$where_id,$order_name='',$ordered='ASC'){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$pivot_name.' = '.$table2.'.'.$pivot_name);
        $this->db->where($table1.'.'.$where_name,$where_id);
        if($order_name != '')
            $this->db->order_by($table2.'.'.$order_name,$ordered);
        $query = $this->db->get(); 

        return $query->result();
    }

    public function get_join_where_in($table1,$table2,$pivot_name,$where_name,$where_id,$order_name='',$ordered='ASC'){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$pivot_name.' = '.$table2.'.'.$pivot_name);
        $this->db->where_in($table1.'.'.$where_name,$where_id);
        if($order_name != '')
            $this->db->order_by($table2.'.'.$order_name,$ordered);
        $query = $this->db->get(); 

        return $query->result();
    }

    public function get_left_join($table1,$table2,$pivot_name,$list_id,$id_name,$branch_id,$branch_name){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$pivot_name.' = '.$table2.'.'.$pivot_name, 'left');
        $this->db->where($table1.'.'.$branch_name,$branch_id);
        $this->db->where_in($table1.'.'.$id_name,$list_id);
        $query = $this->db->get(); 

        return $query->result();
    }

    public function get_where_in($table1,$table2,$pivot_name,$list_id,$id_name,$branch_id,$branch_name){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$pivot_name.' = '.$table2.'.'.$pivot_name, 'left');
        $this->db->where($table1.'.'.$branch_name,$branch_id);
        $this->db->where_in($table1.'.'.$id_name,$list_id);
        $query = $this->db->get(); 

        return $query->result();
    }

    public function insert($table,$data){
        $this->db->insert($table, $data); 
        return $this->db->insert_id();
    }

    public function insert_batch($table,$data){
        $this->db->insert_batch($table, $data);
        return TRUE;
    }

    public function update_array($table,$data,$array){
        $this->db->where($array);
        $this->db->update($table, $data);
        return TRUE;
    }

    public function update_batch($table,$data,$update_name){
        $this->db->update_batch($table, $data, $update_name); 
        return TRUE;
    }

    public function delete_by_id($table,$id,$id_name){
        $this->db->delete($table, array($id_name => $id)); 
        return TRUE;
    }

    public function get_result_by_array($table,$array){
        $query = $this->db->get_where($table, $array);
        return $query->result();
    }

    public function get_single_data_by_array($table,$array){
        $query = $this->db->get_where($table, $array);
        return $query->row();
    }

    public function get_search_by_name_single_row($table,$id,$id_name){
        $this->db->select('*');
        $this->db->from($table);
        $query = $this->db->like($id, $id_name);
        return $query->row();
    }

    public function get_search_by_name_multiple_rows($table,$id,$id_name){
        $query = "SELECT * FROM $table WHERE $id_name LIKE '%$id%'";

        $result = $this->db->query($query);
        return $result->result();
    }

    public function get_search_by_two_name_multiple_rows($table,$id,$id_name,$id_2,$id_name_2){
        $query = "SELECT * FROM $table WHERE $id_name LIKE '%$id%' AND $id_name_2 LIKE '%$id_2%'";

        $result = $this->db->query($query);
        return $result->result();
    }

    

}