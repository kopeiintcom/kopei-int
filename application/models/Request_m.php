<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Request_m extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    
    public function get_process_point_by_user($id_user,$request_type) {
        $query = "select sum(value) as saldo from ki_t_request where is_done = 0 and id_user = ".$id_user; //request_type = ".$request_type." and 

        $result = $this->db->query($query);
        return $result->row()->saldo;
    }
    
    public function get_data_request_point($request)
    {
        $sql = "select a.*,b.usertologin,b.name, b.bank_name, b.bank_account_number, b.handphone from ki_t_request a left join  ki_m_user b on b.id_user=a.id_user
                where request_type = ".$request." order by date_transaction desc";
        
        $q = $this->db->query($sql);
        return $q->result();
    }
    
    public function get_data_request_activate_account()
    {
        $sql = "select a.*,b.usertologin,b.name, b.email, b.handphone from ki_t_request a left join  ki_m_user b on b.id_user=a.id_user
                where request_type = 2 and is_done = 0 order by date_transaction desc";
        
        $q = $this->db->query($sql);
        return $q->result();
    }

    public function get_user_data_point($id_user) 
    {
        //$sql = "select usertologin from ki_m_user where id_user in (select id_user from ki_t_request where request_type = 1)" ;
		$sql = "select a.*,b.usertologin, b.bank_name, b.bank_account_number from ki_t_request a left join  ki_m_user b on b.id_user=a.id_user
                where request_type = 1 and b.id_user =".$id_user." order by a.date_transaction desc" ;
        $q = $this->db->query($sql);
        //var_dump( $q->result());die();
        return $q->result();
    }  
    
    public function get_user_data_ppob_pulsa($id_user) 
    {
        //$sql = "select usertologin from ki_m_user where id_user in (select id_user from ki_t_request where request_type = 1)" ;
		$sql = "select a.*,b.usertologin, b.bank_name, b.bank_account_number from ki_t_request a left join  ki_m_user b on b.id_user=a.id_user
                where request_type = 2 and b.id_user =".$id_user." order by a.date_transaction desc" ;
        $q = $this->db->query($sql);
        //var_dump( $q->result());die();
        return $q->result();
    }
    
    public function get_user_data_ppob_listrik($id_user) 
    {
        //$sql = "select usertologin from ki_m_user where id_user in (select id_user from ki_t_request where request_type = 1)" ;
		$sql = "select a.*,b.usertologin, b.bank_name, b.bank_account_number from ki_t_request a left join  ki_m_user b on b.id_user=a.id_user
                where request_type = 3 and b.id_user =".$id_user." order by a.date_transaction desc" ;
        $q = $this->db->query($sql);
        //var_dump( $q->result());die();
        return $q->result();
    }
}
