<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_m extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    public function is_new_member($id_user)
    {
        $this->db->where('DATEDIFF(now(), date_created) >= 2 and id_user = '.$id_user);
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function is_email_available($email)
    {
        $this->db->where('email', $email);
        $this->db->where('is_delete', 0);
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function is_handphone_available($handphone)
    {
        $this->db->where('handphone', $handphone);
        $this->db->where('is_delete', 0);
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function is_rekening_available($rek)
    {
        $this->db->where('bank_account_number', $rek);
        $this->db->where('is_delete', 0);
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function is_sponsor_exist($usertologin)
    {
        $this->db->where('usertologin', $usertologin);
        $this->db->where('is_active', 1);
        $this->db->where('is_delete', 0);
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return false;
        } else {
            return true;
        }
    }
    
    public function signup($data)
    {
        $this->db->insert('ki_m_user', $data);
        return $this->db->insert_id();
    }
    
    public function get_data_amal_transaction($id_user)
    {
        $sql = "select * from ki_t_transaction where 
                id_user = ".$id_user." and id_transaction_code = 3 order by date_transaction asc";
        
        $q = $this->db->query($sql);
        return $q->result();
    }
    
    public function get_activate_saham()
    {
        $sql = "select * from ki_t_activate_saham where quantity > 0";
        
        $q = $this->db->query($sql);
        return $q->result();
    }
    
    public function get_start_activate_saham_by_user($id_user)
    {
        $sql = "select * from ki_t_activate_saham where DATE(date_actived)=CURDATE() and id_user = ".$id_user;
        
        $q = $this->db->query($sql);
        return $q->row();
    }
    
    public function get_start_activate_saham($date)
    {
        $sql = "select sum(quantity) as value from ki_t_activate_saham where DATE(date_actived)='".$date."'";
        
        $q = $this->db->query($sql);
        return $q->row()->value;
    }
    
    public function get_posting_saham()
    {
        $sql = "select * from ki_t_activate_bagi_hasil where DATE(date_actived)=CURDATE() ";
        
        $q = $this->db->query($sql);
        return $q->result();
    }
    
    public function total_active_lot() {
        $query = "select sum(active_lot) as total from ki_m_user";

        $result = $this->db->query($query);
        return $result->row()->total;
    }
    
    public function get_user_has_active_lot() {
        $query = "select * from ki_m_user where active_lot > 0";

        $result = $this->db->query($query);
        return $result->result();
    }
    
    public function is_username_exist($usertologin, $isAdmin)
    {
        if (!$isAdmin) {
          $this->db->where('(role = 1 or role = 0) and usertologin ="'.$usertologin.'"');
        } else {
          $this->db->where('role = 2 and usertologin ="'.$usertologin.'"');
        }
        
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return false;
        } else {
            return true;
        }
    }
    
    public function is_lot_active_queue($id_user) 
    {
        $query = "select active_lot+(select sum(quantity) from ki_t_activate_saham where id_user=".$id_user.") as lot 
                from ki_m_user where id_user =".$id_user;

        $result = $this->db->query($query);
        if($result->row()->lot > 0){
            return true;
        } else {
            return false;
        }
    }
    
    public function is_receiver_fee($id_user)
    {
        $this->db->where('id_user', $id_user);
        if ($this->db->get('ki_m_user')->num_rows() == 0) {
            return false;
        } else {
            return true;
        }
    }
    
    public function get_ppob_pulsa($operator) {
        $query = "select * from ki_m_ppob where name_provider='".$operator."' order by code asc";

        $result = $this->db->query($query);
        return $result->result();
    }
    
    public function get_ppob_listrik() {
        $query = "select * from ki_m_ppob where type=2 order by price asc";

        $result = $this->db->query($query);
        return $result->result();
    }
    
    public function get_active_user_by_sponsor($id_sponsor) {
        $query = "select * from ki_m_user where id_sponsor=".$id_sponsor." and is_delete = 0";

        $result = $this->db->query($query);
        return $result->result();
    }
}