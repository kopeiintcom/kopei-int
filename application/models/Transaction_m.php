<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Transaction_m extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    public function get_dana_bersama() {
        $query = "select sum(value*isdk) as dana_bersama from ki_t_transaction where id_transaction_code = 1";

        $result = $this->db->query($query);
        return $result->row()->dana_bersama;
    }
    
    public function get_transaction_dana_bersama() {
        $query = "select * from ki_t_transaction where id_transaction_code = 1 order by date_transaction asc";

        $result = $this->db->query($query);
        return $result->result();
    }
    
    public function get_all_amal() {
        $query = "select sum(value*isdk) as amal from ki_t_transaction where id_transaction_code = 3 order by date_transaction asc";

        $result = $this->db->query($query);
        return $result->row()->amal;
    }
    
    public function get_transaction_amal() {
        $query = "select * from ki_t_transaction where id_transaction_code = 3 order by date_transaction asc";

        $result = $this->db->query($query);
        return $result->result();
    }
    
	public function get_stock_pendapatan() {
        $query = "select sum(value*isdk) as stock_pendapatan from ki_t_transaction where id_transaction_code = 13";

        $result = $this->db->query($query);
        return $result->row()->stock_pendapatan;
    }
    
    public function get_transaction_stock_pendapatan() {
        $query = "select * from ki_t_transaction where id_transaction_code = 13 order by date_transaction asc";

        $result = $this->db->query($query);
        return $result->result();
    }
	
	public function get_administrasi() {
        $query = "select sum(value*isdk) as administrasi from ki_t_transaction where id_transaction_code = 2";

        $result = $this->db->query($query);
        return $result->row()->administrasi;
    }
    
    public function get_transaction_administrasi() {
        $query = "select * from ki_t_transaction where id_transaction_code = 2 order by date_transaction asc";

        $result = $this->db->query($query);
        return $result->result();
    }
    
    public function get_total_pendapatan_by_user($id_user) {
        $query = "select sum(value) as total from ki_t_transaction where id_transaction_group = 2 and date_format(date_transaction, '%Y-%m-%d') = CURDATE() and id_user=".$id_user;

        $result = $this->db->query($query);
        return $result->row()->total;
    }
	
    public function get_saldo_debet_by_user($id_user) {
        $query = "select sum(value) as saldo_debet from ki_t_transaction where id_transaction_group = 2 and isdk = 1 and id_user = ".$id_user;

        $result = $this->db->query($query);
        return $result->row()->saldo_debet;
    }
    
    public function get_debet_transaction_by_user($id_user) {
        $query = "select * from ki_t_transaction where id_transaction_group = 2 and isdk = 1 and id_user = ".$id_user." order by date_transaction asc";

        $result = $this->db->query($query);
        return $result->result();
    }
    
    public function get_saldo_kredit_by_user($id_user) {
        $query = "select sum(value) as saldo_kredit from ki_t_transaction where id_transaction_group = 2 and isdk = -1 and id_user = ".$id_user;

        $result = $this->db->query($query);
        return $result->row()->saldo_debet;
    }
    
    public function get_kredit_transaction_by_user($id_user) {
        $query = "select * from ki_t_transaction where id_transaction_group = 2 and isdk = -1 and id_user = ".$id_user." order by date_transaction asc";

        $result = $this->db->query($query);
        return $result->result();
    }
    
    public function get_saldo_dk_by_user($id_user) {
        $query = "select sum(value*isdk) as saldo from ki_t_transaction where id_transaction_group = 2 and id_user = ".$id_user;

        $result = $this->db->query($query);
        return $result->row()->saldo;
    }
    
    public function get_dk_transaction_by_user($id_user) {
        $query = "select * from ki_t_transaction where id_transaction_group = 2 and id_user = ".$id_user." order by date_transaction asc";

        $result = $this->db->query($query);
        return $result->result();
    }
    
    public function get_total_penerima() {
        $query = "SELECT sum(active_lot) as total_penerimaan FROM `ki_m_user`";

        $result = $this->db->query($query);
        return $result->row()->total_penerimaan;
    }
	
	public function get_total_full_profit() {
        $query = "SELECT count(*) as total_full_profit FROM `ki_m_user` WHERE `id_user` IN (SELECT id_user FROM `ki_t_activate_saham` WHERE quantity=0)";

        $result = $this->db->query($query);
        return $result->row()->total_full_profit;
    }
	
	public function get_total_antrian() {
        $query = "select sum(quantity) as total_antrian from ki_t_activate_saham";

        $result = $this->db->query($query);
        return $result->row()->total_antrian;
    }
	
	public function get_total_antrian_by_user($id_user) {
        $query = "select sum(quantity) as total_antrian from ki_t_activate_saham where id_user=".$id_user;

        $result = $this->db->query($query);
        return $result->row()->total_antrian;
    }
	
	public function get_total_lot() {
        $query = "select ((select sum(active_lot+ inactive_lot) from ki_m_user)+ (select sum(quantity) from ki_t_activate_saham)) as total_lot";

        $result = $this->db->query($query);
        return $result->row()->total_lot;
    }
    
    public function get_total_lot_by_user($id_user) {
        $query = "select sum(active_lot+ inactive_lot+IFNULL(
        (select sum(quantity) from ki_t_activate_saham where id_user=".$id_user."), 0)) as total_lot 
        from ki_m_user where id_user=".$id_user;

        $result = $this->db->query($query);
        return $result->row()->total_lot;
    }
    
	public function get_total_member() {
        $query = "select count( DISTINCT(id_user) ) as total_member FROM ki_t_activate_saham";

        $result = $this->db->query($query);
        return $result->row()->total_member;
    }
	
	public function get_total_free_member() {
        $query = "select count( DISTINCT(id_user) ) as total_free_member FROM `ki_m_user` WHERE id_user NOT IN (SELECT id_user FROM `ki_t_activate_saham`) and role=0" ;

        $result = $this->db->query($query);
        return $result->row()->total_free_member;
    }
	
	public function get_pendapatan_keseluruhan($id_user) {
        $query = "SELECT a.active_lot*b.content as calc FROM ki_m_user a
				join ki_m_setting b on (alias = 'point_bagi_hasil') where id_user= ".$id_user;

        $result = $this->db->query($query);
        return $result->row()->calc;
    }
	public function get_pendapatan_persaham($id_user) {
        $query = "SELECT a.active_lot*b.content as calc FROM ki_m_user a
				join ki_m_setting b on (alias = 'point_bagi_hasil') where id_user= ".$id_user;
		
		$query2 = "Select sum(active_lot) as sum_active_lot from ki_m_user";
		
        $result = $this->db->query($query);
		$result2 = $this->db->query($query2);
		
		$total = "Select ".$result->row()->calc." * ".$result2->row()->sum_active_lot." as total";
		$result_total = $this->db->query($total);
        return $result_total->row()->total;
    }
	
	public function get_lot_active_user($id_user) {
        $query = "SELECT sum(active_lot) as active_lot FROM ki_m_user
				 where id_user= ".$id_user;

        $result = $this->db->query($query);
        return $result->row()->active_lot;
    }
	
	public function get_lot_inactive_user($id_user) {
        $query = "SELECT sum(inactive_lot) as inactive_lot FROM ki_m_user
				 where id_user= ".$id_user;

        $result = $this->db->query($query);
        return $result->row()->inactive_lot;
    }
	
	public function get_antri_lot_user($id_user) {
        $query = "SELECT sum(quantity) as kuantitas FROM ki_t_activate_saham
				 where id_user = ".$id_user;

        $result = $this->db->query($query);
        return $result->row()->kuantitas;
    }
	
	public function get_profit_yesterday($id_user) {
		$query = "SELECT sum(value*isdk)  as value FROM `ki_t_transaction`  WHERE  
		id_transaction_code= 5 and date_format(date_transaction, '%Y-%m-%d') = subdate(CURDATE() ,1) 
		and id_user=".$id_user;
		$result = $this->db->query($query);
		return $result->row()->value;
	}
	
	public function get_current_slot() {
		$query = "SELECT sum(quantity) as value FROM `ki_t_activate_saham` WHERE date_format(date_actived, '%Y-%m-%d') = CURDATE()";
		$result = $this->db->query($query);
		return $result->row()->value;
	}
	
	public function get_yesterday_slot() {
		//$query = "SELECT sum(value) as slot_yesterday FROM `ki_t_activate_bagi_hasil` WHERE date_format(date_actived, '%Y-%m-%d') = CURDATE()";
		$query = "SELECT sum(quantity) as value FROM `ki_t_activate_saham` WHERE date_format(date_actived, '%Y-%m-%d') = subdate(CURDATE(),1)";
		$result = $this->db->query($query);
		return $result->row()->value;
	}
	
	public function get_current_user() {
		$query = "select count(*) as value from ki_m_user where date_format(date_created, '%Y-%m-%d') = CURDATE()";
		$result = $this->db->query($query);
		return $result->row()->value;
	}
    
    public function get_saldo_d_bagi_hasil_by_user($id_user) {
        $query = "select sum(value) as saldo_debet from ki_t_transaction where id_transaction_code = 5 and isdk = 1 and id_user = ".$id_user;

        $result = $this->db->query($query);
		if($result->row()->saldo_debet == null) {
			return 0;
		} else {
			return $result->row()->saldo_debet;
		}
    }
	
	public function get_penambahan_posisi() {
		$query = "SELECT b.name, a.date_actived, b.city from ki_t_activate_saham a left join ki_m_user b on (a.id_user = b.id_user) order by date_actived desc limit 10";
		
		$result = $this->db->query($query);
		return $result->result();
	}
	
	public function get_new_member() {
		$query = "select * from ki_m_user order by date_created desc limit 10";
		
		$result = $this->db->query($query);
		return $result->result();
	}
	
	public function get_top_sponsor() {
		$query = "select a.id_sponsor,b.name,b.usertologin,a.total from 
                (select id_sponsor,count(id_user) as total from ki_m_user 
                where id_sponsor <> 0 and  id_sponsor <> 17 group by id_sponsor) a 
                left join ki_m_user b on (a.id_sponsor = b.id_user) 
                where b.role = 0 || b.role=1
                order by total desc limit 10";
		
		$result = $this->db->query($query);
		return $result->result();
	}
	
	public function get_mutasi_lot($id_user) {
		$query = "SELECT * FROM `ki_t_transaction` WHERE (id_transaction_code=7 or id_transaction_code=6) and id_user= ".$id_user."  ORDER BY date_transaction ASC";
		
		$result = $this->db->query($query);
		return $result->result();
	}
	
	public function get_mutasi_lot_admin() {
		$query = "SELECT * FROM `ki_t_transaction` WHERE id_transaction_code=6 ORDER BY date_transaction ASC";
		
		$result = $this->db->query($query);
		return $result->result();
	}
	
	public function get_recursive($id_user) {
		$query = "SELECT id_user FROM `ki_m_user` WHERE id_sponsor=".$id_user ." and id_user!=".$id_user;
		
		$result = $this->db->query($query);
		return $result->result_array();
	}
	
	public function get_count_recursive($id_user) {
		$query = "select count(*) as value from ki_m_user where id_sponsor=".$id_user."  and id_user=".$id_user;
		$result = $this->db->query($query);
		return $result->row()->value;
	}
	
	public function get_parent($parent)
    {
        $query='SELECT id_sponsor FROM ki_m_user '.'WHERE id_user="'.$parent.'"';
		$result = $this->db->query($query);
		return $result->result_array();
    }
	
	public function get_child($parent){
		$query='SELECT id_user FROM ki_m_user '.'WHERE id_sponsor="'.$parent.'"';
		$result = $this->db->query($query);
		return $result->result_array();
	}
    
    public function summary_total_all_transaction_by_day()
    {
        $query = "select count(id_transaction) as jumlah, date_format(date_transaction,'%d-%m-%y') as tanggal from ki_t_transaction 
                group by date_format(date_transaction,'%y%m%d')";
		
		$result = $this->db->query($query);
		return $result->result();
    }
    
    public function summary_total_transaction_by_day_code($id_transaction_code)
    {
        $query = "select count(id_transaction) as jumlah, date_format(date_transaction,'%d-%m-%y') as tanggal from ki_t_transaction
                where id_transaction_code = ".$id_transaction_code."
                group by date_format(date_transaction,'%y%m%d')";
		
		$result = $this->db->query($query);
		return $result->result();
    }
    
    public function summary_bagi_hasil_per_user()
    {
        $query = "SELECT a.id_user,a.usertologin,a.active_lot, (
                SELECT SUM(value) AS saldo_debet
                FROM ki_t_transaction
                WHERE id_transaction_code = 5 AND isdk = 1 AND id_user = a.id_user) as saldo_bagi_hasil
                FROM ki_m_user a where a.active_lot > 0";
        
        $result = $this->db->query($query);
		return $result->result();
    }
}
